//
//  dpImportWindowController.h
//  congregate
//
//  Created by Mike Flores on 1/9/13.
//  Copyright (c) 2013 digiputty, llc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "dpAppDelegate.h"

@interface dpImportWindowController : NSWindowController

@property (weak) IBOutlet NSString *familyFileName;
@property (weak) IBOutlet NSString *personFileName;
@property (weak) IBOutlet NSString *familyImportResults;
@property (weak) IBOutlet NSString *personImportResults;
@property (weak) IBOutlet NSString *progressMessage;
@property (weak) IBOutlet NSNumber *progressIndicator;

@property (strong) IBOutlet dpAppDelegate *delegate;

- (IBAction)selectFamilyImportFile:(id)sender;
- (IBAction)selectPersonImportFile:(id)sender;
- (IBAction)import:(id)sender;

@end
