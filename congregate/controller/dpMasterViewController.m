//
//  dpMasterViewController.m
//  congregate
//
//  Created by Mike Flores on 1/7/13.
//  Copyright (c) 2013 digiputty, llc. All rights reserved.
//

#import "dpMasterViewController.h"
#import "dpDonationsViewController.h"

@interface dpMasterViewController ()

@end

@implementation dpMasterViewController

@synthesize subView = _subView;
@synthesize subViewController = _subViewController;
@synthesize subViewButtons = _subViewButtons;
@synthesize selectedObject = _selectedObject;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (IBAction)newItem:(id)sender
{
    if ([_subViewController respondsToSelector:@selector(newItem:)])
    {
        [_subViewController newItem:sender];
    }
}

- (IBAction)delete:(id)sender
{
    if ([_subViewController respondsToSelector:@selector(delete:)])
    {
        [_subViewController delete:sender];
    }
}

- (IBAction)changeSubView:(id)sender
{
    NSButton *button = sender;
    NSString *buttonTitle = [[button selectedCell] alternateTitle];
    
    
    //NSLog(selectedItem);
    [self changeSubViewController:buttonTitle];
    
    // make sure all buttons are unselected
    if (_subViewButtons) {
        for (NSButton *btn in _subViewButtons)
        {
            [btn setState:NSOffState];
        }
    }
    
    // select the appropriate button
    [button setState:NSOnState];
}

- (void)changeSubViewController:(NSString *)viewName
{
    // remove the old view
    [self removeSubView];
    
   // add the new view
    NSString *nibName = [NSString stringWithFormat:@"%s%@%s", "dp", [viewName stringByReplacingOccurrencesOfString:@" " withString:@""], "View"];
    _subViewController = [(DPViewController *)[NSClassFromString([NSString stringWithFormat:@"%@Controller", nibName]) alloc] initWithNibName:nibName bundle:nil];
    NSArray *subViews = [NSArray arrayWithObject:[_subViewController view]];
    [_subView setSubviews:subViews];
    //[_subView addSubview:[_subViewController view]];
    [_subViewController addObserver:self forKeyPath:@"selectedObject" options:NSKeyValueObservingOptionNew context:nil];
    [_subViewController addObserver:self forKeyPath:@"arrangedObjects" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)removeSubView
{
    [self setPrimaryArrayController:nil];
    [_subViewController removeObserver:self forKeyPath:@"selectedObject"];
    [_subViewController removeObserver:self forKeyPath:@"arrangedObjects"];
    [[_subViewController view]removeFromSuperview];
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"selectedObject"]) {
        [self willChangeValueForKey:@"selectedObject"];
        [self setSelectedObject:[change objectForKey:NSKeyValueChangeNewKey]];
        [self didChangeValueForKey:@"selectedObject"];
    }
    if ([keyPath isEqualToString:@"arrangedObjects"]) {
        [self willChangeValueForKey:@"arrangedObjects"];
        [self setArrangedObjects:[[_subViewController primaryArrayController] arrangedObjects]];
        [self didChangeValueForKey:@"arrangedObjects"];
    }
}

+ (BOOL)automaticallyNotifiesObserversForKey:(NSString *)theKey {
    
    BOOL automatic = NO;
    if ([theKey isEqualToString:@"selectedObject"]) {
        automatic = NO;
    }
    else {
        automatic = [super automaticallyNotifiesObserversForKey:theKey];
    }
    return automatic;
}

- (void)loadView
{
    [self viewWillLoad];
    [super loadView];
    [self viewDidLoad];
}

- (void)viewWillLoad
{
    // this should be overridden by subclasses
}

- (void)viewDidLoad
{
    if (_dashboardView) {
        [_subView addSubview:_dashboardView];
    }
}

- (void)dealloc
{
    //if ([self primaryArrayController])
    //{
        [_subViewController removeObserver:self forKeyPath:@"selectedObject"];
        [_subViewController removeObserver:self forKeyPath:@"arrangedObjects"];
    //}
}


@end
