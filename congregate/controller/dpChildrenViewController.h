//
//  dpChildrenViewController.h
//  congregate
//
//  Created by Mike Flores on 12/7/12.
//  Copyright (c) 2012 digiputty, llc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "DPViewController.h"
#import "Person.h"
#import "PhoneNumber.h"
#import "MailingAddress.h"
#import "EmailAddress.h"
#import "SocialAddress.h"
#import "SignificantDate.h"

@interface dpChildrenViewController : DPViewController
@property (weak) IBOutlet NSButton *genderButton;

@property (strong) IBOutlet NSArrayController *phoneArrayController;
@property (strong) IBOutlet NSArrayController *addressArrayController;
@property (strong) IBOutlet NSArrayController *emailArrayController;
@property (strong) IBOutlet NSArrayController *socialArrayController;
@property (strong) IBOutlet NSArrayController *familyMembersArrayController;
@property (strong) IBOutlet NSArrayController *datesArrayController;

@property (strong) IBOutlet NSPopover *phonePopover;
@property (strong) IBOutlet NSPopover *addressPopover;
@property (strong) IBOutlet NSPopover *socialPopover;
@property (strong) IBOutlet NSPopover *emailPopover;
@property (strong) IBOutlet NSPopover *personPopover;
@property (strong) IBOutlet NSPopover *datesPopover;

@property (weak) IBOutlet NSCollectionView *socialCollectionView;
@property (weak) IBOutlet NSCollectionView *datesCollectionView;
@property (weak) IBOutlet NSCollectionView *phoneCollectionView;
@property (weak) IBOutlet NSCollectionView *addressCollectionView;
@property (weak) IBOutlet NSCollectionView *emailCollectionView;
@property (weak) IBOutlet NSCollectionView *familyCollectionView;
@property (weak) IBOutlet NSTableView *personTableView;

@property (weak) IBOutlet NSTabView *detailTabView;
@property (weak) IBOutlet NSButton *tab1Button;
@property (weak) IBOutlet NSButton *tab2Button;
@property (weak) IBOutlet NSButton *tab3Button;
@property (weak) IBOutlet NSButton *tab4Button;

@property (strong) NSDate *today;
@property (strong) IBOutlet NSView *childLabelView;
@property (strong) IBOutlet NSView *claimTagView;

@property (weak) IBOutlet NSTextField *gradeAdjustField;

- (IBAction)changeDetailTab:(id)sender;
- (IBAction)printNameBadge:(id)sender;
- (IBAction)changeGradeAdjust:(id)sender;

- (IBAction)addPhoneNumber:(id)sender;
- (IBAction)editPhone:(id)sender;
- (IBAction)deletePhone:(id)sender;
- (IBAction)finishPhoneEdit:(id)sender;
- (IBAction)cancelPhoneEdit:(id)sender;
- (void)selectPhone:(PhoneNumber *)phone;

- (IBAction)addAddress:(id)sender;
- (IBAction)editAddress:(id)sender;
- (IBAction)deleteAddress:(id)sender;
- (IBAction)finishAddressEdit:(id)sender;
- (IBAction)cancelAddressEdit:(id)sender;
- (void)selectAddress:(MailingAddress *)address;

- (IBAction)addEmail:(id)sender;
- (IBAction)editEmail:(id)sender;
- (IBAction)deleteEmail:(id)sender;
- (IBAction)finishEmailEdit:(id)sender;
- (IBAction)cancelEmailEdit:(id)sender;
- (void)selectEmail:(EmailAddress *)email;

- (IBAction)addSocialAddress:(id)sender;
- (IBAction)editSocialAddress:(id)sender;
- (IBAction)deleteSocialAddress:(id)sender;
- (IBAction)finishSocialEdit:(id)sender;
- (IBAction)cancelSocialEdit:(id)sender;
- (void)selectSocialAddress:(SocialAddress *)address;

- (IBAction)addDate:(id)sender;
- (IBAction)editDate:(id)sender;
- (IBAction)deleteDate:(id)sender;
- (IBAction)finishDateEdit:(id)sender;
- (IBAction)cancelDateEdit:(id)sender;
- (void)selectDate:(SignificantDate *)eventDate;

- (IBAction)addPerson:(id)sender;
- (IBAction)deletePerson:(id)sender;
- (IBAction)finishPersonEdit:(id)sender;
- (IBAction)cancelPersonEdit:(id)sender;
- (IBAction)newItem:(id)sender;

@end
