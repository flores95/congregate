//
//  dpPeopleViewController.h
//  congregate
//
//  Created by Mike Flores on 12/12/12.
//  Copyright (c) 2012 digiputty, llc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "dpMasterViewController.h"

@interface dpPeopleViewController : dpMasterViewController

@property (assign) int countOfPeople;
@property (assign) int countOfAdults;
@property (assign) int countOfChildren;
@property (assign) int countOfActive;
@property (assign) int countOfAttending;
@property (assign) int countOfSearching;
@property (assign) int countOfVisiting;
@property (assign) int countOfGiving;
@property (assign) int countOfParticipating;
@property (weak) IBOutlet NSButton *usersButton;
@property (weak) IBOutlet NSButton *familiesButton;
@property (weak) IBOutlet NSButton *childrenButton;
@property (weak) IBOutlet NSButton *peopleButton;
@property (weak) IBOutlet NSButton *adultButton;

@end
