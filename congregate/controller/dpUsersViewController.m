//
//  dpUsersViewController.m
//  congregate
//
//  Created by Mike Flores on 1/10/13.
//  Copyright (c) 2013 digiputty, llc. All rights reserved.
//

#import "dpUsersViewController.h"
#import "DPUser.h"

@interface dpUsersViewController ()

@end

@implementation dpUsersViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // get things sorted
    NSSortDescriptor *personSort = [NSSortDescriptor sortDescriptorWithKey:@"displayName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    [[self personArrayController] setSortDescriptors:[NSArray arrayWithObject:personSort]];
    NSSortDescriptor *userSort = [NSSortDescriptor sortDescriptorWithKey:@"username" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    [[self primaryArrayController] setSortDescriptors:[NSArray arrayWithObject:userSort]];

    // handle double click edit
    // deal with double clicks
    [_userTableView setTarget:self];
    [_userTableView setDoubleAction:@selector(editUser:)];

}

- (void)newItem:(id)sender
{
    [self addUser:sender];
}

- (IBAction)addUser:(id)sender
{
    DPUser *newUser = [DPUser createInContext:[[NSApp delegate] managedObjectContext]];
    [[self primaryArrayController] addObject:newUser];
    [[self userPopover] setAppearance:NSPopoverAppearanceHUD];
    [[self userPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMinYEdge];
}

- (void)editUser:(id)sender
{
    [[self userPopover] setAppearance:NSPopoverAppearanceHUD];
    [[self userPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxXEdge];
}

- (IBAction)deleteUser:(id)sender
{
    [[self primaryArrayController] remove:sender];
}

- (IBAction)finishUserEdit:(id)sender
{
    DPUser *currentItem = [[[self primaryArrayController] arrangedObjects] objectAtIndex:[[self primaryArrayController] selectionIndex]];
    NSError *error = nil;
    if ([currentItem isInserted] && ![[_passwordEditField stringValue] isEqualToString:[_confirmPasswordEditField stringValue]])
    {
        NSDictionary *errorDict = [NSDictionary dictionaryWithObjectsAndKeys:@"* Passwords do not match", NSLocalizedDescriptionKey, nil];
        error = [NSError errorWithDomain:@"CONGREGATE" code:100 userInfo:errorDict];
        [self showAlert:@"PROBLEM SAVING USER" WithError:error];
        return;
    }
    else if ([_confirmPasswordEditField stringValue])
    {
        if (![[_passwordEditField stringValue] isEqualToString:[_confirmPasswordEditField stringValue]])
        {
            NSDictionary *errorDict = [NSDictionary dictionaryWithObjectsAndKeys:@"* Passwords do not match", NSLocalizedDescriptionKey, nil];
            error = [NSError errorWithDomain:@"CONGREGATE" code:100 userInfo:errorDict];
            [self showAlert:@"PROBLEM SAVING USER" WithError:error];
            return;
        }
    }
    
    if ([currentItem validateEntity:&error])
    {
        [_userPopover close];
    } else {
        [self showAlert:@"PROBLEM SAVING USER" WithError:error];
    }
}

- (IBAction)cancelUserEdit:(id)sender
{
    DPUser *currentItem = [[[self primaryArrayController] arrangedObjects] objectAtIndex:[[self primaryArrayController] selectionIndex]];
    if ([currentItem isInserted])
    {
        [[self primaryArrayController] remove:sender];
    }
    [_userPopover close];
}

- (void)showAlert:(NSString *)title WithError:(NSError *)error
{
    NSAlert *alert = [NSAlert alertWithMessageText:title defaultButton:@"OK" alternateButton:@"Cancel" otherButton:nil informativeTextWithFormat:@"%@", [error localizedDescription]];
    [alert beginSheetModalForWindow:[[self view] window] modalDelegate:self  didEndSelector:@selector(alertDidEnd:returnCode:contextInfo:) contextInfo:nil];
}

- (void) alertDidEnd:(NSAlert *)alert returnCode:(int)returnCode contextInfo:(void *)contextInfo
{
    // don't need to do anything for now
}

@end
