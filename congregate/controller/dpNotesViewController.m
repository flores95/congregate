//
//  dpNotesViewController.m
//  congregate
//
//  Created by Mike Flores on 1/14/13.
//  Copyright (c) 2013 digiputty, llc. All rights reserved.
//

#import "dpNotesViewController.h"

@interface dpNotesViewController ()

@end

@implementation dpNotesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

@end
