//
//  DPViewController.m
//  congregate
//
//  Created by Mike Flores on 1/17/13.
//  Copyright (c) 2013 digiputty, llc. All rights reserved.
//

#import "DPViewController.h"

@interface DPViewController ()

@end

@implementation DPViewController

@synthesize selectedObject = _selectedObject;
@synthesize primaryArrayController = _primaryArrayController;

- (void)awakeFromNib
{
    [super awakeFromNib];
    [[self primaryArrayController] addObserver:self forKeyPath:@"selection" options:NSKeyValueObservingOptionNew context:nil];
    [[self primaryArrayController] addObserver:self forKeyPath:@"arrangedObjects" options:NSKeyValueObservingOptionNew context:nil];
}


- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    if ([keyPath isEqualToString:@"selection"] && [[[self primaryArrayController] arrangedObjects] count] > 0) {
        if ([[self primaryArrayController] selectedObjects] && [[[self primaryArrayController] selectedObjects] count] > 0)
        {
            [self setSelectedObject:[[[self primaryArrayController] arrangedObjects] objectAtIndex:[[self primaryArrayController] selectionIndex]]];
        }
    }
    if ([keyPath isEqualToString:@"arrangedObjects"]) {
        [self willChangeValueForKey:@"arrangedObjects"];
        [self setArrangedObjects:[[self primaryArrayController] arrangedObjects]];
        [self didChangeValueForKey:@"arrangedObjects"];
    }
}

-(IBAction)newItem:(id)sender
{
    // implement this in the subclasses
}

-(IBAction)delete:(id)sender
{
    // implement this in the subclasses
}

- (void) dealloc
{
    [[self primaryArrayController] removeObserver:self forKeyPath:@"selection"];
    [[self primaryArrayController] removeObserver:self forKeyPath:@"arrangedObjects"];
}

- (void)loadView
{
    [self viewWillLoad];
    [super loadView];
    [self viewDidLoad];
}

- (void)viewWillLoad
{
}

- (void)viewDidLoad
{
}



@end
