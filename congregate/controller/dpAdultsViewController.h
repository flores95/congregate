//
//  dpAdultsViewController.h
//  congregate
//
//  Created by Mike Flores on 12/7/12.
//  Copyright (c) 2012 digiputty, llc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "DPViewController.h"
#import "Person.h"
#import "PhoneNumber.h"
#import "MailingAddress.h"
#import "EmailAddress.h"
#import "SocialAddress.h"
#import "SignificantDate.h"

@interface dpAdultsViewController : DPViewController

@property (strong) IBOutlet NSArrayController *phoneArrayController;
@property (strong) IBOutlet NSArrayController *addressArrayController;
@property (strong) IBOutlet NSArrayController *emailArrayController;
@property (strong) IBOutlet NSArrayController *socialArrayController;
@property (weak) IBOutlet NSTextField *firstEditField;
@property (weak) IBOutlet NSButton *genderButton;
@property (strong) IBOutlet NSPopover *phonePopover;
@property (weak) IBOutlet NSCollectionView *phoneCollectionView;
@property (strong) IBOutlet NSPopover *addressPopover;
@property (weak) IBOutlet NSCollectionView *addressCollectionView;
@property (strong) IBOutlet NSPopover *socialPopover;
@property (weak) IBOutlet NSCollectionView *socialCollectionView;
@property (strong) IBOutlet NSPopover *emailPopover;
@property (weak) IBOutlet NSCollectionView *emailCollectionView;
@property (weak) IBOutlet NSTableView *personTableView;
@property (strong) IBOutlet NSPopover *personPopover;
@property (strong) IBOutlet NSPopover *datesPopover;
@property (weak) IBOutlet NSCollectionView *datesCollectionView;
@property (strong) IBOutlet NSArrayController *datesArrayController;
@property (weak) IBOutlet NSCollectionView *familyCollectionView;
@property (strong) IBOutlet NSArrayController *familyMembersArrayController;
@property (strong) IBOutlet NSPopover *deletePopover;
@property (weak) IBOutlet NSButton *deletePhoneButton;

@property (weak) IBOutlet NSTabView *detailTabView;
@property (weak) IBOutlet NSButton *tab1Button;
@property (weak) IBOutlet NSButton *tab2Button;
@property (weak) IBOutlet NSButton *tab3Button;
@property (weak) IBOutlet NSButton *tab4Button;

@property (strong) IBOutlet NSView *primaryEmailExportView;
@property (strong) IBOutlet NSPopover *exportPopover;
@property (strong) IBOutlet NSPopover *searchPopover;
@property (weak) IBOutlet NSPredicateEditor *searchPredicateEditor;

- (IBAction)changeDetailTab:(id)sender;
- (IBAction)showExport:(id)sender;
- (IBAction)showSearchPopover:(id)sender;
- (IBAction)performSearch:(id)sender;

- (IBAction)addPhoneNumber:(id)sender;
- (IBAction)editPhone:(id)sender;
- (IBAction)deletePhone:(id)sender;
- (IBAction)finishPhoneEdit:(id)sender;
- (IBAction)cancelPhoneEdit:(id)sender;
- (void)selectPhone:(PhoneNumber *)phone;

- (IBAction)addAddress:(id)sender;
- (IBAction)editAddress:(id)sender;
- (IBAction)deleteAddress:(id)sender;
- (IBAction)finishAddressEdit:(id)sender;
- (IBAction)cancelAddressEdit:(id)sender;
- (void)selectAddress:(MailingAddress *)address;

- (IBAction)addEmail:(id)sender;
- (IBAction)editEmail:(id)sender;
- (IBAction)deleteEmail:(id)sender;
- (IBAction)finishEmailEdit:(id)sender;
- (IBAction)cancelEmailEdit:(id)sender;
- (void)selectEmail:(EmailAddress *)email;

- (IBAction)addSocialAddress:(id)sender;
- (IBAction)editSocialAddress:(id)sender;
- (IBAction)deleteSocialAddress:(id)sender;
- (IBAction)finishSocialEdit:(id)sender;
- (IBAction)cancelSocialEdit:(id)sender;
- (void)selectSocialAddress:(SocialAddress *)address;

- (IBAction)addDate:(id)sender;
- (IBAction)editDate:(id)sender;
- (IBAction)deleteDate:(id)sender;
- (IBAction)finishDateEdit:(id)sender;
- (IBAction)cancelDateEdit:(id)sender;
- (void)selectDate:(SignificantDate *)eventDate;

- (IBAction)addPerson:(id)sender;
- (IBAction)deletePerson:(id)sender;
- (IBAction)finishPersonEdit:(id)sender;
- (IBAction)cancelPersonEdit:(id)sender;
- (IBAction)newItem:(id)sender;

- (IBAction)exportPrimaryEmail:(id)sender;
- (IBAction)exportPrimaryContactInfo:(id)sender;

@end
