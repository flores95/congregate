//
//  dpCompaniesViewController.m
//  congregate
//
//  Created by Mike Flores on 12/27/12.
//  Copyright (c) 2012 digiputty, llc. All rights reserved.
//

#import "dpCompaniesViewController.h"

@interface dpCompaniesViewController ()

@end

@implementation dpCompaniesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

@end
