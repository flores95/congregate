//
//  dpDonorsViewController.m
//  congregate
//
//  Created by Mike Flores on 12/27/12.
//  Copyright (c) 2012 digiputty, llc. All rights reserved.
//

#import "dpDonorsViewController.h"
#import "GivingBatch.h"
#import "Donation.h"
#import "Donor.h"
#import "Person.h"
#import "MailingAddress.h"
#import "Business.h"
#import "Fund.h"
#import "GivingBatch.h"
#import "dpAppDelegate.h"

@interface dpDonorsViewController ()

@end

@implementation dpDonorsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)viewDidLoad
{
    // sort the arrays
    NSSortDescriptor *donorSort = [NSSortDescriptor sortDescriptorWithKey:@"displayName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    [self.primaryArrayController setSortDescriptors:[NSArray arrayWithObject:donorSort]];
    NSSortDescriptor *donationSort = [NSSortDescriptor sortDescriptorWithKey:@"batch.givingDate" ascending:YES];
    [_donationsArrayController setSortDescriptors:[NSArray arrayWithObject:donationSort]];
    
    // filter out donor's that have no donations
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"donations.@count > 0"];
    [self.primaryArrayController setFilterPredicate:pred];
    
    // set the printing defaults
    NSPrintInfo *printInfo = [NSPrintInfo sharedPrintInfo];
    [printInfo setOrientation:NSPortraitOrientation];
    [printInfo setHorizontallyCentered:YES];
    [printInfo setLeftMargin:18];
    [printInfo setRightMargin:18];
    [printInfo setTopMargin:18];
    [printInfo setBottomMargin:18];
}

- (IBAction)exportMenu:(id)sender {
    NSRect frame = [(NSButton *)sender frame];
    NSPoint menuOrigin = [[(NSButton *)sender superview] convertPoint:NSMakePoint(frame.origin.x, frame.origin.y)
                                                               toView:nil];
    
    NSEvent *event =  [NSEvent mouseEventWithType:NSLeftMouseDown
                                         location:menuOrigin
                                    modifierFlags:0
                                        timestamp:0
                                     windowNumber:[[(NSButton *)sender window] windowNumber]
                                          context:[[(NSButton *)sender window] graphicsContext]
                                      eventNumber:0
                                       clickCount:1
                                         pressure:1];
    
    [NSMenu popUpContextMenu:_exportMenu withEvent:event forView:(NSButton *)sender];
}

- (IBAction)showItemExportMenu:(id)sender {
    NSRect frame = [(NSButton *)sender frame];
    NSPoint menuOrigin = [[(NSButton *)sender superview] convertPoint:NSMakePoint(frame.origin.x, frame.origin.y)
                                                               toView:nil];
    
    NSEvent *event =  [NSEvent mouseEventWithType:NSLeftMouseDown
                                         location:menuOrigin
                                    modifierFlags:0
                                        timestamp:0
                                     windowNumber:[[(NSButton *)sender window] windowNumber]
                                          context:[[(NSButton *)sender window] graphicsContext]
                                      eventNumber:0
                                       clickCount:1
                                         pressure:1];
    
    [NSMenu popUpContextMenu:_itemExportMenu withEvent:event forView:(NSButton *)sender];
}

- (IBAction)printDonationReport:(id)sender
{
}

- (IBAction)createYTDDonationReports:(id)sender {
    // lets do this in html
    dpAppDelegate *appDelegate = [NSApp delegate];
    NSURL *fileUrl = [[appDelegate applicationFilesDirectory] URLByAppendingPathComponent:@"Temp_Print.html"];
    
    
    NSMutableString *htmlString = [[NSMutableString alloc] init];
    [htmlString appendString:[self _printStyles]];
    
    for (Donor *currentDonor in [self.primaryArrayController arrangedObjects])
    {
        [htmlString appendFormat:@"<div style=\"page-break-after: always;\">"];
        [htmlString appendString:[self _givingStatementHeader:currentDonor]];
        [htmlString appendString:[self _givingStatementTotals:currentDonor]];
        [htmlString appendString:[self _givingStatementFundSummary:currentDonor]];
        [htmlString appendString:[self _givingStatementFooter:currentDonor]];
        [htmlString appendString:[self _givingStatementDetail:currentDonor]];
        [htmlString appendString:@"</font></div>"];
    }
    
    [[htmlString dataUsingEncoding:NSUTF8StringEncoding] writeToURL:fileUrl atomically:YES];
    
    [[_reportView mainFrame] loadRequest:[NSURLRequest requestWithURL:fileUrl]];
    [_printPreviewWindow makeKeyAndOrderFront:self];
    
}

- (IBAction)createDonationReport:(id)sender {
    // lets do this in html
    dpAppDelegate *appDelegate = [NSApp delegate];
    NSURL *fileUrl = [[appDelegate applicationFilesDirectory] URLByAppendingPathComponent:@"Temp_Print.html"];
    NSMutableString *htmlString = [[NSMutableString alloc] init];
    [htmlString appendString:[self _printStyles]];
    Donor *currentDonor = (Donor *)[self selectedObject];
    
    [htmlString appendFormat:@"<div style=\"page-break-after: always;\">"];
    [htmlString appendString:[self _givingStatementHeader:currentDonor]];
    [htmlString appendString:[self _givingStatementTotals:currentDonor]];
    [htmlString appendString:[self _givingStatementFundSummary:currentDonor]];
    [htmlString appendString:[self _givingStatementFooter:currentDonor]];
    [htmlString appendString:[self _givingStatementDetail:currentDonor]];
    [htmlString appendString:@"</div>"];
    
    [[htmlString dataUsingEncoding:NSUTF8StringEncoding] writeToURL:fileUrl atomically:YES];
    
    [[_reportView mainFrame] loadRequest:[NSURLRequest requestWithURL:fileUrl]];
    [_printPreviewWindow makeKeyAndOrderFront:self];
}

- (NSString *)_printStyles
{
    NSString *htmlString = @"<style>";
    htmlString = [htmlString stringByAppendingString:@"@media print {"];
    //htmlString = [htmlString stringByAppendingString:@"html body {width: 100%; height: 100%;}"];
    //htmlString = [htmlString stringByAppendingString:@"div#page_footer {position: fixed; bottom: 0; margin-bottom: 10px;}"];
    htmlString = [htmlString stringByAppendingString:@"}"];
    htmlString = [htmlString stringByAppendingString:@"</style>"];
    
    return htmlString;
}

- (NSString *)_givingStatementHeader:(Donor *)donor
{
    NSMutableString *header = [[NSMutableString alloc] init];
    
    //TODO pull church name, address and details from a database
    [header appendString:@"<div style=\"padding-bottom: 10px; padding-top: 30px;\"><table width=100%><tr>"];
    [header appendString:@"<td style=\"width: 50%;\">"];
    [header appendString:@"<div style=\"padding-bottom: 90px; font:8pt verdana,sans-serif;\">"];
    [header appendString:@"<b>Northwest Bible Church</b><br/>"];
    [header appendString:@"889 W Chapala Dr<br/>"];
    [header appendString:@"Tucson, AZ 85704<br/>"];
    [header appendString:@"520-544-7775<br/>"];
    [header appendString:@"</div>"];
    [header appendString:@"<div>"];
    [header appendFormat:@"<div style=\" font:8pt verdana,sans-serif;\"><b>%@</b><br/>", [donor displayName]];
    if ([donor isKindOfClass:[Person class]])
    {
        if ([(Person *)donor primaryAddress])
        {
            [header appendFormat:@"%@<br/>", [[[(Person *)donor primaryAddress] fullAddress] stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"]];
        } else if ([(Person *)donor mailingAddresses]) {
            for (MailingAddress *addr in [(Person *)donor mailingAddresses])
            {
                if ([[addr label] isEqualToString:@"Home"])
                {
                    [header appendFormat:@"%@<br/>", [[addr fullAddress] stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"]];
                }
            }
        }
    } else {
        if ([(Business *)donor address])
        {
            [header appendFormat:@"%@<br/>", [(Business *)donor address]];
        }
    }
    [header appendString:@"</div>"];
    [header appendString:@"</div>"];
    [header appendString:@"</td>"];
    
    
    [header appendString:@"<td style=\"width: 50%; vertical-align: top; font-size:8pt; font-family: Verdana;\">"];
    [header appendString:@"<b><font size=3>Giving Statement</font></b><br/>"];
    [header appendString:@"January 1, 2013 to December 31, 2013<br/>"];
    [header appendString:@"<p><b>Tax ID:</b> 94-2756941(US)</p>"];
    [header appendString:@"</td>"];
    [header appendString:@"</tr></table></div>"];
    
    return header;
}

- (NSString *)_givingStatementFundSummary:(Donor *)donor
{
    NSMutableString *htmlString = [[NSMutableString alloc] init];
    
    [htmlString appendString:@"<div style=\"padding-top: 15px;\"><table width=300px style=\"font-size: 8pt; font-family: Verdana;\"><tr>"];
    [htmlString appendString:@"<tr><td width=200px align=left style=\"border-bottom: 1px solid #000;\"><b>Fund Summary</b></td><td width=100px align=right style=\"border-bottom: 1px solid #000;\"><b>Total</b></td></tr>"];
    for (Fund *currentFund in [_fundArrayContoller arrangedObjects])
    {
        double fundTotal = 0;
        for (Donation *curDonation in [donor donations])
        {
            if ([[currentFund name] isEqualToString:[[curDonation fund] name]])
            {
                fundTotal += [[curDonation amount] doubleValue];
            }
        }
        if (fundTotal > 0)
        {
            [htmlString appendFormat:@"<tr><td width=200px align=left style=\"padding-left: 10px;\">%@</td><td width=100px style=\"text-align: right;\">%.2f</td></tr>", [currentFund name], fundTotal];
        }
    }
    [htmlString appendFormat:@"<tr><td width=200px align=left style=\"border-top: 1px solid #000;\"><b>Total Fund Contributions</b></td><td width=100px align=right style=\"border-top: 1px solid #000;\"><b>%.2f</b></td></tr>", [self _totalDonations:donor]];
    [htmlString appendString:@"</table></div>"];
    
    return htmlString;
}

- (NSString *)_givingStatementTotals:(Donor *)donor
{
    NSMutableString *htmlString = [[NSMutableString alloc] init];
    
    [htmlString appendString:@"<div style=\"border: 2px solid black; margin-top: 35px;\"><table width=100% style=\"font-size: 8pt; font-family: Verdana; font-style: bold;\"><tr>"];
    [htmlString appendFormat:@"<td>Tax Deductible Total: %.2f</td>", [self _totalDeductibleDonations:donor]];
    [htmlString appendFormat:@"<td>*Non-Tax Deductible total: %.2f</td>", [self _totalNonDeductibleDonations:donor]];
    [htmlString appendFormat:@"<td>Total Contributions: %.2f</td>", [self _totalDonations:donor]];
    [htmlString appendString:@"</tr></table></div>"];
    
   
    return htmlString;
}

- (NSString *)_givingStatementDetail:(Donor *)donor
{
    NSMutableString *htmlString = [[NSMutableString alloc] init];
    
    //TODO pull church name, address and details from a database
    [htmlString appendString:@"<div style=\"margin-top: 5px;\"><b><font size=3>Giving Details</font></b>"];
    [htmlString appendString:@"<table width=100% style=\"font-family: Verdana; font-size:5pt;\"><tr>"];
    [htmlString appendString:@"<td style=\"border-bottom: 1px solid black;\"><b>Date</b></td>"];
    [htmlString appendString:@"<td style=\"border-bottom: 1px solid black;\"><b>Ref</b></td>"];
    [htmlString appendString:@"<td style=\"border-bottom: 1px solid black;\"><b>Fund</b></td>"];
    [htmlString appendString:@"<td style=\"border-bottom: 1px solid black;\"><b>Amount</b></td>"];
    [htmlString appendString:@"<td>&nbsp;</td>"];
    [htmlString appendString:@"<td style=\"border-bottom: 1px solid black;\"><b>Date</b></td>"];
    [htmlString appendString:@"<td style=\"border-bottom: 1px solid black;\"><b>Ref</b></td>"];
    [htmlString appendString:@"<td style=\"border-bottom: 1px solid black;\"><b>Fund</b></td>"];
    [htmlString appendString:@"<td style=\"border-bottom: 1px solid black;\"><b>Amount</b></td>"];
    [htmlString appendString:@"<td>&nbsp;</td>"];
    [htmlString appendString:@"<td style=\"border-bottom: 1px solid black;\"><b>Date</b></td>"];
    [htmlString appendString:@"<td style=\"border-bottom: 1px solid black;\"><b>Ref</b></td>"];
    [htmlString appendString:@"<td style=\"border-bottom: 1px solid black;\"><b>Fund</b></td>"];
    [htmlString appendString:@"<td style=\"border-bottom: 1px solid black;\"><b>Amount</b></td>"];
    [htmlString appendString:@"</tr>"];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yy"];

    int donationCount = 0;
    [htmlString appendString:@"<tr>"];
    NSSortDescriptor *donationSort = [NSSortDescriptor sortDescriptorWithKey:@"batch.givingDate" ascending:YES];
    [_donationsArrayController setSortDescriptors:[NSArray arrayWithObject:donationSort]];
    NSArray *sortedDonations = [[donor donations] sortedArrayUsingDescriptors:[NSArray arrayWithObjects:donationSort, nil]];
    
    for (Donation *currentDonation in sortedDonations)
    {
        donationCount++;
        [htmlString appendFormat:@"<td align=center>%@</td>", [dateFormat stringFromDate:[[currentDonation batch] givingDate]]];
        
        if ([currentDonation checkNumber])
        {
            [htmlString appendFormat:@"<td align=left>%@</td>", [currentDonation checkNumber]];
        } else {
            [htmlString appendString:@"<td align=center>cash</td>"];
        }
        
        [htmlString appendFormat:@"<td>%@</td>", [[currentDonation fund] name]];
        
        if ([currentDonation amount] && ![[currentDonation amount] isEqualToNumber:[NSDecimalNumber decimalNumberWithString:@"0.0"]])
        {
            [htmlString appendFormat:@"<td align=right style=\"padding-right: 3px;\">%.2f</td>", [[currentDonation amount] doubleValue]];
        } else if ([currentDonation nonDeductAmount] && ![[currentDonation nonDeductAmount] isEqualToNumber:[NSDecimalNumber decimalNumberWithString:@"0.0"]])
        {
            [htmlString appendFormat:@"<td align=right style=\"padding-right: 3px;\">%.2f*</td>", [[currentDonation nonDeductAmount] doubleValue]];
        } else {
            [htmlString appendString:@"<td align=center style=\"padding-right: 3px;\"></td>"];
        }
        
        if (remainder(donationCount,3) == 0)
        {
            [htmlString appendString:@"</tr><tr>"];
        } else {
            //space cell
            [htmlString appendString:@"<td></td>"];
        }
    }
    [htmlString appendString:@"</tr>"];
    [htmlString appendString:@"</table></div>"];
    
    return htmlString;
}

- (NSString *)_givingStatementFooter:(Donor *)donor
{
    NSMutableString *htmlString = [[NSMutableString alloc] init];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    [htmlString appendString:@"<div id=\"page_footer\" style=\"margin-top: 20px; font-family: Verdana; font-size: 8pt; border-top: 2px solid black\">"];
    [htmlString appendString:@"The donor has not received any goods, services or benefits in connection with any contribution. The only value the donor received consisted of intangible religious benefits.<br />"];
    //[htmlString appendFormat:@"Printed On: %@", [dateFormat stringFromDate:[NSDate date]]];
    [htmlString appendString:@"<div style=\"padding-top: 80px; padding-bottom: 20px; font-face: Verdana; font-size: 9pt;\">"];
    [htmlString appendString:@"Ronald W. Brown </br> Treasurer"];
    [htmlString appendString:@"</div>"];
    [htmlString appendString:@"</div>"];
    
    return htmlString;
}

- (double)_totalDonations:(Donor *)donor
{
    double total = 0;
    for (Donation *curDonation in [donor donations])
    {
        total = total + [[curDonation amount] doubleValue];
        if ([curDonation nonDeductAmount])
        {
            total = total + [[curDonation nonDeductAmount] doubleValue];
        }
    }
    return total;
}

- (double)_totalDeductibleDonations:(Donor *)donor
{
    double total = 0;
    for (Donation *curDonation in [donor donations])
    {
        total = total + [[curDonation amount] doubleValue];
    }
    return total;
}

- (double)_totalNonDeductibleDonations:(Donor *)donor
{
    double total = 0;
    for (Donation *curDonation in [donor donations])
    {
        if ([curDonation nonDeductAmount])
        {
            total = total + [[curDonation nonDeductAmount] doubleValue];
        }
    }
    return total;
}

@end
