//
//  dpPreferencesWindowController.m
//  congregate
//
//  Created by Mike Flores on 2/25/13.
//  Copyright (c) 2013 digiputty, llc. All rights reserved.
//

#import "dpPreferencesWindowController.h"

@interface dpPreferencesWindowController ()

@end

@implementation dpPreferencesWindowController

- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    
    NSArray *printerNames = [NSPrinter printerNames];
    NSMutableArray *lblPrinters = [[NSMutableArray alloc] initWithCapacity:[printerNames count]];
    for (NSString *printerName in printerNames)
    {
        NSPrinter *printer = [NSPrinter printerWithName:printerName];
        NSLog(@"Printer (%@) Type (%@)", printerName, [printer type]);
        if ([[printer type] isEqualToString:@"DYMO LabelWriter 400"] || [[printer type] isEqualToString:@"DYMO LabelWriter 450"])
        {
            [lblPrinters addObject:printer];
        }
    }
    [self setLabelPrinters:lblPrinters];
}

@end
