//
//  dpAttendanceViewController.m
//  congregate
//
//  Created by Mike Flores on 2/16/13.
//  Copyright (c) 2013 digiputty, llc. All rights reserved.
//

#import "dpAttendanceViewController.h"

@interface dpAttendanceViewController ()

@end

@implementation dpAttendanceViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

@end
