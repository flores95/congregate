//
//  dpFamiliesViewController.h
//  congregate
//
//  Created by Mike Flores on 1/7/13.
//  Copyright (c) 2013 digiputty, llc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "DPViewController.h"
#import <WebKit/WebKit.h>

@interface dpFamiliesViewController : DPViewController

@property (strong) IBOutlet NSArrayController *personArrayController;
@property (strong) IBOutlet NSArrayController *familyMemberArrayController;
@property (strong) IBOutlet NSArrayController *fundArrayController;

@property (weak) IBOutlet NSTableView *familyTableView;
@property (weak) IBOutlet NSTableView *familyMemberTableView;
@property (weak) IBOutlet NSTabView *detailTabView;

@property (strong) IBOutlet NSPopover *familyPopover;
@property (strong) IBOutlet NSPopover *memberPopover;
@property (strong) IBOutlet NSPopover *personPopover;
@property (strong) IBOutlet NSPopover *existingPersonPopover;
@property (weak) IBOutlet NSTextField *existingPersonField;
@property (strong) IBOutlet NSPopover *exportPopover;
@property (strong) IBOutlet NSPopover *searchPopover;
@property (strong) IBOutlet NSPopover *addressPopover;
@property (strong) IBOutlet NSPopover *phonePopover;
@property (strong) IBOutlet NSPopover *emailPopover;
@property (strong) IBOutlet NSPopover *selectHeadPopover;
@property (strong) IBOutlet NSPopover *selectAddressPopover;
@property (strong) IBOutlet NSPopover *selectPhonePopover;
@property (strong) IBOutlet NSPopover *selectEmailPopover;

@property (weak) IBOutlet NSButton *editAddressButton;
@property (weak) IBOutlet NSButton *editPhoneButton;
@property (weak) IBOutlet NSButton *editEmailButton;

@property (strong) IBOutlet NSWindow *printPreviewWindow;
@property (weak) IBOutlet WebView *reportView;
@property (strong) IBOutlet NSMenu *itemExportMenu;

@property (weak) IBOutlet NSPredicateEditor *searchPredicateEditor;

@property (weak) IBOutlet NSCollectionView *memberCollectionView;

- (IBAction)addFamily:(id)sender;
- (IBAction)deleteFamily:(id)sender;
- (IBAction)finishFamilyEdit:(id)sender;
- (IBAction)cancelFamilyEdit:(id)sender;
- (IBAction)newItem:(id)sender;

- (IBAction)editMember:(id)sender;
- (IBAction)addNewMember:(id)sender;
- (IBAction)addExistingMember:(id)sender;
- (IBAction)removeMember:(id)sender;
- (IBAction)finishMemberEdit:(id)sender;
- (IBAction)cancelMemberEdit:(id)sender;

- (IBAction)selectPerson:(id)sender;
- (IBAction)cancelAddExisting:(id)sender;
- (IBAction)finishAddExisting:(id)sender;

- (IBAction)selectHead:(id)sender;
- (IBAction)selectAddress:(id)sender;
- (IBAction)selectPhone:(id)sender;
- (IBAction)selectEmail:(id)sender;

- (IBAction)editPhone:(id)sender;
- (IBAction)finishPhoneEdit:(id)sender;
- (IBAction)updateMemberPhone:(id)sender;

- (IBAction)editAddress:(id)sender;
- (IBAction)finishAddressEdit:(id)sender;
- (IBAction)updateMemberAddress:(id)sender;

- (IBAction)editEmail:(id)sender;
- (IBAction)finishEmailEdit:(id)sender;
- (IBAction)updateMemberEmail:(id)sender;

- (IBAction)showItemExportMenu:(id)sender;
- (IBAction)printYTDDonationReport:(id)sender;
- (IBAction)createYTDDonationReport:(id)sender;

- (IBAction)showSearchPopover:(id)sender;
- (IBAction)performSearch:(id)sender;
- (IBAction)showExport:(id)sender;
- (IBAction)exportFamilyHeadContactInfo:(id)sender;

@end
