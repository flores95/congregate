//
//  dpGroupsViewController.m
//  congregate
//
//  Created by Mike Flores on 12/5/12.
//  Copyright (c) 2012 digiputty, llc. All rights reserved.
//

#import "dpGroupsViewController.h"

@interface dpGroupsViewController ()

@end

@implementation dpGroupsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // add subView buttons
    NSArray *buttons = [NSArray arrayWithObjects:[self groupButton], [self tagsButton], [self attendanceButton], nil];
    self.subViewButtons = buttons;
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
}



@end
