//
//  dpFundsViewController.m
//  congregate
//
//  Created by Mike Flores on 12/18/12.
//  Copyright (c) 2012 digiputty, llc. All rights reserved.
//

#import "dpFundsViewController.h"
#import "Fund.h"

@interface dpFundsViewController ()

@end

@implementation dpFundsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    // setup array sorts
    NSSortDescriptor *fundSort = [NSSortDescriptor sortDescriptorWithKey:@"internalName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    [[self primaryArrayController] setSortDescriptors:[NSArray arrayWithObject:fundSort]];
}

- (IBAction)addFund:(id)sender
{
    Fund *newFund = [Fund createInContext:[[NSApp delegate] managedObjectContext]];
    [[self primaryArrayController] addObject:newFund];
    [_firstEditField becomeFirstResponder];
}

- (IBAction)newItem:(id)sender
{
    [self addFund:sender];
}


@end
