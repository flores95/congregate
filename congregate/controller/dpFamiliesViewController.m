//
//  dpFamiliesViewController.m
//  congregate
//
//  Created by Mike Flores on 1/7/13.
//  Copyright (c) 2013 digiputty, llc. All rights reserved.
//

#import "dpFamiliesViewController.h"
#import "Family.h"
#import "FamilyMember.h"
#import "Person.h"
#import "SignificantDate.h"
#import "GivingBatch.h"
#import "Donation.h"
#import "Donor.h"
#import "MailingAddress.h"
#import "Business.h"
#import "Fund.h"
#import "GivingBatch.h"
#import "dpAppDelegate.h"
#import "CHCSVParser.h"

@interface dpFamiliesViewController ()

@end

@implementation dpFamiliesViewController

NSString *selectHeadFor = @""; // used for editing family address/email/phone

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)viewDidLoad
{
    // get things sorted
    NSSortDescriptor *personSort = [NSSortDescriptor sortDescriptorWithKey:@"displayName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    [[self personArrayController] setSortDescriptors:[NSArray arrayWithObject:personSort]];
    NSSortDescriptor *familySort = [NSSortDescriptor sortDescriptorWithKey:@"lastName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    [[self primaryArrayController] setSortDescriptors:[NSArray arrayWithObject:familySort]];
    
    // deal with double clicks
    [_familyTableView setTarget:self];
    [_familyTableView setDoubleAction:@selector(editFamily:)];
    [_familyMemberTableView setTarget:self];
    [_familyMemberTableView setDoubleAction:@selector(editMember:)];

    // cleanup collection view appearance
    NSArray *viewBGColors = [NSArray arrayWithObjects:[NSColor colorWithDeviceWhite:1.0 alpha:0], nil];
    [_memberCollectionView setBackgroundColors:viewBGColors];

    
    // setup the predicate(search) editor
    [self setupPredicateEditor];
    
    // setup printing defaults
    NSPrintInfo *printInfo = [NSPrintInfo sharedPrintInfo];
    [printInfo setOrientation:NSPortraitOrientation];
    [printInfo setVerticalPagination: NSAutoPagination];
    [printInfo setHorizontallyCentered:YES];
    [printInfo setLeftMargin:18];
    [printInfo setRightMargin:18];
    [printInfo setTopMargin:18];
    [printInfo setBottomMargin:18];

}

- (void)setupPredicateEditor
{
    NSArray *stringPaths = [NSArray arrayWithObjects:
                            [NSExpression expressionForKeyPath:@"name"],
                            [NSExpression expressionForKeyPath:@"lastName"],
                            nil];
    NSArray *stringOps = [NSArray arrayWithObjects:[NSNumber numberWithInteger:NSEqualToPredicateOperatorType],
                          [NSNumber numberWithInteger:NSNotEqualToPredicateOperatorType],
                          [NSNumber numberWithInteger:NSBeginsWithPredicateOperatorType],
                          [NSNumber numberWithInteger:NSEndsWithPredicateOperatorType],
                          [NSNumber numberWithInteger:NSContainsPredicateOperatorType],
                          nil];
    
    NSPredicateEditorRowTemplate *stringTemplate = [[NSPredicateEditorRowTemplate alloc] initWithLeftExpressions:stringPaths
                                                                                    rightExpressionAttributeType:NSStringAttributeType
                                                                                                        modifier:NSDirectPredicateModifier
                                                                                                       operators:stringOps
                                                                                                         options:(NSCaseInsensitivePredicateOption | NSDiacriticInsensitivePredicateOption)];
    
    NSArray *boolPaths = [NSArray arrayWithObjects:
                          [NSExpression expressionForKeyPath:@"active"],
                          nil];
    NSArray *boolOps = [NSArray arrayWithObjects:
                        [NSNumber numberWithInteger:NSEqualToPredicateOperatorType],
                        [NSNumber numberWithInteger:NSNotEqualToPredicateOperatorType],
                        nil];
    
    NSPredicateEditorRowTemplate *boolTemplate = [[NSPredicateEditorRowTemplate alloc] initWithLeftExpressions:boolPaths
                                                                                  rightExpressionAttributeType:NSBooleanAttributeType
                                                                                                      modifier:NSDirectPredicateModifier
                                                                                                     operators:boolOps
                                                                                                       options:(NSCaseInsensitivePredicateOption | NSDiacriticInsensitivePredicateOption)];
    
    NSArray *childStringPaths = [NSArray arrayWithObjects:
                                 [NSExpression expressionForKeyPath:@"tags.name"],
                                 [NSExpression expressionForKeyPath:@"notes.noteText"],
                                nil];
    
    NSPredicateEditorRowTemplate *childStringTemplate = [[NSPredicateEditorRowTemplate alloc] initWithLeftExpressions:childStringPaths
                                                                                         rightExpressionAttributeType:NSStringAttributeType
                                                                                                             modifier:NSAnyPredicateModifier
                                                                                                            operators:stringOps
                                                                                                              options:(NSCaseInsensitivePredicateOption | NSDiacriticInsensitivePredicateOption)];
    
    
    NSArray *compoundTypes = [NSArray arrayWithObjects:[NSNumber numberWithInteger:NSNotPredicateType],
                              [NSNumber numberWithInteger:NSAndPredicateType],
                              [NSNumber numberWithInteger:NSOrPredicateType],
                              nil];
    NSPredicateEditorRowTemplate *compound = [[NSPredicateEditorRowTemplate alloc] initWithCompoundTypes:compoundTypes];
    
    NSArray *rowTemplates = [NSArray arrayWithObjects:stringTemplate, boolTemplate, childStringTemplate, compound, nil];
    
    [_searchPredicateEditor setRowTemplates:rowTemplates];
}

#pragma mark - Family Edit Routines
- (void)editFamily:(id)sender
{
    [[self familyPopover] setAppearance:NSPopoverAppearanceHUD];
    [[self familyPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxXEdge];
}

- (IBAction)addFamily:(id)sender
{
    Family *newFamily = [Family createInContext:[[NSApp delegate] managedObjectContext]];
    [newFamily setActiveValue:YES];
    
    [[self primaryArrayController] addObject:newFamily];
    
    [[self familyPopover] setAppearance:NSPopoverAppearanceHUD];
    [[self familyPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxYEdge];
}

- (IBAction)deleteFamily:(id)sender
{
    //TODO - WARNING Popup and remove people first
    [[self primaryArrayController] remove:sender];
    [_familyPopover close];
}

- (IBAction)finishFamilyEdit:(id)sender
{
    Person *currentItem = [[[self primaryArrayController] arrangedObjects] objectAtIndex:[[self primaryArrayController] selectionIndex]];
    NSError *error = nil;
    if ([currentItem validateEntity:&error])
    {
        [[self familyPopover] close];
    } else {
        [self showAlert:@"PROBLEM SAVING FAMILY" WithError:error];
    }
}

- (IBAction)cancelFamilyEdit:(id)sender
{
    Person *currentItem = [[[self primaryArrayController] arrangedObjects] objectAtIndex:[[self primaryArrayController] selectionIndex]];
    if ([currentItem isInserted])
    {
        [[self primaryArrayController] remove:sender];
    }
    [[self familyPopover] close];
}

-(IBAction)newItem:(id)sender
{
    [self addFamily:sender];
}

#pragma mark - Member Edit Routines

- (IBAction)addNewMember:(id)sender
{
    Person *newPerson = [Person createInContext:[[NSApp delegate] managedObjectContext]];
    
    // make a significant date for creation of new person
    SignificantDate *regDate = [SignificantDate createInContext:[[NSApp delegate] managedObjectContext]];
    [regDate setEventDate:[NSDate date]];
    [regDate setEventTitle:@"Registered"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *user = [defaults valueForKey:kDPAppUserKey];
    [regDate setEventDescription:[NSString stringWithFormat:@"Registered by %@", user]];
    [newPerson addSignificantDatesObject:regDate];
    
    // make a significant date for adding of new person to family
    SignificantDate *famDate = [SignificantDate createInContext:[[NSApp delegate] managedObjectContext]];
    [famDate setEventDate:[NSDate date]];
    [famDate setEventTitle:@"Added to Family"];
    Family *curFamily = [[[self primaryArrayController] arrangedObjects] objectAtIndex:[[self primaryArrayController] selectionIndex]];
    [famDate setEventDescription:[NSString stringWithFormat:@"Added to %@ family", [curFamily name]]];
    [newPerson addSignificantDatesObject:famDate];
    
    // create new FamilyMember and connect new person
    FamilyMember *newMember = [FamilyMember createInContext:[[NSApp delegate] managedObjectContext]];
    [newMember setPersonDetail:newPerson];
    
    // add the member to the current familiy
    [[self familyMemberArrayController] insertObject:newMember atArrangedObjectIndex:0];

    
    [[self memberPopover] setAppearance:NSPopoverAppearanceHUD];
    [[self memberPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxYEdge];
}

- (IBAction)removeMember:(id)sender
{
    //NSAlert
    [[self familyMemberArrayController] remove:sender];
    [_memberPopover close];
}

- (void)editMember:(id)sender
{
    [[self memberPopover] setAppearance:NSPopoverAppearanceHUD];
    [[self memberPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxXEdge];
}

- (IBAction)finishMemberEdit:(id)sender
{
    Person *currentItem = [[[self familyMemberArrayController] arrangedObjects] objectAtIndex:[[self familyMemberArrayController] selectionIndex]];
    NSError *error = nil;
    if ([currentItem validateEntity:&error])
    {
        [[self memberPopover] close];
    } else {
        [self showAlert:@"PROBLEM SAVING PERSON" WithError:error];
    }
}

- (IBAction)cancelMemberEdit:(id)sender
{
    FamilyMember *curMember = [[[self familyMemberArrayController] arrangedObjects] objectAtIndex:[[self familyMemberArrayController] selectionIndex]];
    Person *curPerson = [curMember personDetail];
    if ([curPerson isInserted])
    {
        NSError *error = nil;
        [curPerson deleteEntity:&error];
        [[self familyMemberArrayController] remove:sender];
    }
    [[self memberPopover] close];
}

#pragma mark - Select Head & Primaries
- (void)selectHead:(id)sender
{
    Family *curFamily = [[[self primaryArrayController] arrangedObjects] objectAtIndex:[[self primaryArrayController] selectionIndex]];
    
    if ([selectHeadFor isEqualToString:@"ADDRESS"])
    {
        if ([curFamily headOfFamily])
        {
            selectHeadFor = @"";
            [[self selectHeadPopover] close];
            [self editAddress:_editAddressButton];
        }
    }

    if ([selectHeadFor isEqualToString:@"PHONE"])
    {
        if ([curFamily headOfFamily])
        {
            selectHeadFor = @"";
            [[self selectHeadPopover] close];
            [self editPhone:_editPhoneButton];
        }
    }

    if ([selectHeadFor isEqualToString:@"EMAIL"])
    {
        if ([curFamily headOfFamily])
        {
            selectHeadFor = @"";
            [[self selectHeadPopover] close];
            [self editEmail:_editEmailButton];
        }
        
    }
}

- (IBAction)selectAddress:(id)sender
{
    Family *curFamily = [[[self primaryArrayController] arrangedObjects] objectAtIndex:[[self primaryArrayController] selectionIndex]];
    if ([[curFamily headOfFamily] primaryAddress])
    {
        [[self selectAddressPopover] close];
    }
}

- (IBAction)selectPhone:(id)sender
{
    Family *curFamily = [[[self primaryArrayController] arrangedObjects] objectAtIndex:[[self primaryArrayController] selectionIndex]];
    if ([[curFamily headOfFamily] primaryPhone])
    {
        [[self selectPhonePopover] close];
    }
}

- (IBAction)selectEmail:(id)sender
{
    Family *curFamily = [[[self primaryArrayController] arrangedObjects] objectAtIndex:[[self primaryArrayController] selectionIndex]];
    if ([[curFamily headOfFamily] primaryEmail])
    {
        [[self selectEmailPopover] close];
    }
}


#pragma mark - Phone Number Actions

- (IBAction)editPhone:(id)sender
{
    Family *curFamily = [[[self primaryArrayController] arrangedObjects] objectAtIndex:[[self primaryArrayController] selectionIndex]];
    if ([curFamily headOfFamily])
    {
        if (![[curFamily headOfFamily] primaryPhone]) {
            if ([[[curFamily headOfFamily] phoneNumbers] count] > 0)
            {
                [[self selectPhonePopover] setAppearance:NSPopoverAppearanceHUD];
                [[self selectPhonePopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxXEdge];
            } else {
                // if no phones ask if you want to add one
                NSAlert *alert = [NSAlert alertWithMessageText: @"No Phone Number"
                                                 defaultButton:@"YES"
                                               alternateButton:@"NO"
                                                   otherButton:nil
                                     informativeTextWithFormat:@"There are no phone numbers for %@. Would you like to create a new one?", [[curFamily headOfFamily] name]];
                NSInteger button = [alert runModal];
                if (button == NSAlertDefaultReturn) {
                    PhoneNumber *newPhone = [PhoneNumber createInContext:[[NSApp delegate] managedObjectContext]];
                    [newPhone setPublishValue:YES];
                    [newPhone setPrimaryValue:YES];
                    [newPhone setLabel:@"Home"];
                    [[curFamily headOfFamily] addPhoneNumbersObject:newPhone];
                    [[self phonePopover] setAppearance:NSPopoverAppearanceHUD];
                    [[self phonePopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxXEdge];
                }
            }
        } else {
            [[self phonePopover] setAppearance:NSPopoverAppearanceHUD];
            [[self phonePopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxXEdge];            
        } 
    } else {
        selectHeadFor = @"PHONE";
        [[self selectHeadPopover] setAppearance:NSPopoverAppearanceHUD];
        [[self selectHeadPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxXEdge];
    }

}

- (IBAction)finishPhoneEdit:(id)sender
{
    Family *curFamily = [[[self primaryArrayController] arrangedObjects] objectAtIndex:[[self primaryArrayController] selectionIndex]];
    PhoneNumber *currentItem = [curFamily.headOfFamily primaryPhone];
    NSError *error = nil;
    if ([currentItem validateEntity:&error])
    {
        [_phonePopover close];
    } else {
        [self showAlert:@"PROBLEM SAVING PHONE" WithError:error];
    }
}

- (IBAction)updateMemberPhone:(id)sender
{
    Family *curFamily = [[[self primaryArrayController] arrangedObjects] objectAtIndex:[[self primaryArrayController] selectionIndex]];
    if ([curFamily headOfFamily] && [curFamily.headOfFamily primaryPhone])
    {
        for(FamilyMember *member in [curFamily members])
        {
            if (![member headValue])
            {
                Person *curPerson = [member personDetail];
                // first remove any address with the same label
                NSString *label = [curFamily.headOfFamily.primaryPhone label];
                for (PhoneNumber *addr in [curPerson phoneNumbers])
                {
                    if ([[addr label] isEqualToString:label])
                    {
                        NSError *error = nil;
                        [addr deleteEntity:&error];
                    }
                }
                
                // add the new address
                [curPerson addPhoneNumbersObject:[curFamily.headOfFamily.primaryPhone duplicateEntity]];
            }
        }
    }
}

#pragma mark - Address Actions

- (IBAction)editAddress:(id)sender
{
    Family *curFamily = [[[self primaryArrayController] arrangedObjects] objectAtIndex:[[self primaryArrayController] selectionIndex]];
    if ([curFamily headOfFamily])
    {
        if (![[curFamily headOfFamily] primaryAddress]) {
            if ([[[curFamily headOfFamily] mailingAddresses] count] > 0) {
                [[self selectAddressPopover] setAppearance:NSPopoverAppearanceHUD];
                [[self selectAddressPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxXEdge];
               
            } else {
                // if no addresses ask if you want to add one
                NSAlert *alert = [NSAlert alertWithMessageText: @"No Address"
                                                 defaultButton:@"YES"
                                               alternateButton:@"NO"
                                                   otherButton:nil
                                     informativeTextWithFormat:@"There are no addresses for %@. Would you like to create a new one?", [[curFamily headOfFamily] name]];
                NSInteger button = [alert runModal];
                if (button == NSAlertDefaultReturn) {
                    MailingAddress *newAddr = [MailingAddress createInContext:[[NSApp delegate] managedObjectContext]];
                    [newAddr setPublishValue:YES];
                    [newAddr setPrimaryValue:YES];
                    [newAddr setLabel:@"Home"];
                    [[curFamily headOfFamily] addMailingAddressesObject:newAddr];
                    [[self addressPopover] setAppearance:NSPopoverAppearanceHUD];
                    [[self addressPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxXEdge];
                }
            }
        } else {
            [[self addressPopover] setAppearance:NSPopoverAppearanceHUD];
            [[self addressPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxXEdge];
        }
    } else {
        selectHeadFor = @"ADDRESS";
        [[self selectHeadPopover] setAppearance:NSPopoverAppearanceHUD];
        [[self selectHeadPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxXEdge];
    }
}

- (IBAction)finishAddressEdit:(id)sender
{
    Family *curFamily = [[[self primaryArrayController] arrangedObjects] objectAtIndex:[[self primaryArrayController] selectionIndex]];
    MailingAddress *currentItem = [curFamily.headOfFamily primaryAddress];
    NSError *error = nil;
    if ([currentItem validateEntity:&error])
    {
        [_addressPopover close];
    } else {
        [self showAlert:@"PROBLEM SAVING ADDRESS" WithError:error];
    }
}

- (IBAction)updateMemberAddress:(id)sender {
    Family *curFamily = [[[self primaryArrayController] arrangedObjects] objectAtIndex:[[self primaryArrayController] selectionIndex]];
    if ([curFamily headOfFamily] && [curFamily.headOfFamily primaryAddress])
    {
        for(FamilyMember *member in [curFamily members])
        {
            if (![member headValue])
            {
                Person *curPerson = [member personDetail];
                // first remove any address with the same label
                NSString *label = [curFamily.headOfFamily.primaryAddress label];
                for (MailingAddress *addr in [curPerson mailingAddresses])
                {
                    if ([[addr label] isEqualToString:label])
                    {
                        NSError *error = nil;
                        [addr deleteEntity:&error];
                    }
                }
                
                // add the new address
                [curPerson addMailingAddressesObject:[curFamily.headOfFamily.primaryAddress duplicateEntity]];
            }
        }
    }
}

#pragma mark - Email Actions

- (IBAction)editEmail:(id)sender
{
    Family *curFamily = [[[self primaryArrayController] arrangedObjects] objectAtIndex:[[self primaryArrayController] selectionIndex]];
    if ([curFamily headOfFamily])
    {
        if (![[curFamily headOfFamily] primaryEmail]) {
            if ([[[curFamily headOfFamily] emailAddresses] count] > 0) {
                [[self selectEmailPopover] setAppearance:NSPopoverAppearanceHUD];
                [[self selectEmailPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxXEdge];
            } else {
                // if no emails ask if you want to add one
                NSAlert *alert = [NSAlert alertWithMessageText: @"No Email Address"
                                                 defaultButton:@"YES"
                                               alternateButton:@"NO"
                                                   otherButton:nil
                                     informativeTextWithFormat:@"There are no email addresses for %@. Would you like to create a new one?", [[curFamily headOfFamily] name]];
                NSInteger button = [alert runModal];
                if (button == NSAlertDefaultReturn) {
                    EmailAddress *newAddr = [EmailAddress createInContext:[[NSApp delegate] managedObjectContext]];
                    [newAddr setPublishValue:YES];
                    [newAddr setPrimaryValue:YES];
                    [newAddr setLabel:@"Home"];
                    [[curFamily headOfFamily] addEmailAddressesObject:newAddr];
                    [[self emailPopover] setAppearance:NSPopoverAppearanceHUD];
                    [[self emailPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxXEdge];
                }
            }
        } else {
            [[self emailPopover] setAppearance:NSPopoverAppearanceHUD];
            [[self emailPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxXEdge];
        }
    } else {
        selectHeadFor = @"EMAIL";
        [[self selectHeadPopover] setAppearance:NSPopoverAppearanceHUD];
        [[self selectHeadPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxXEdge];
    }
}

- (IBAction)finishEmailEdit:(id)sender
{
    Family *curFamily = [[[self primaryArrayController] arrangedObjects] objectAtIndex:[[self primaryArrayController] selectionIndex]];
    EmailAddress *currentItem = [curFamily.headOfFamily primaryEmail];
    NSError *error = nil;
    if ([currentItem validateEntity:&error])
    {
        [_emailPopover close];
    } else {
        [self showAlert:@"PROBLEM SAVING EMAIL" WithError:error];
    }
}

- (IBAction)updateMemberEmail:(id)sender
{
    Family *curFamily = [[[self primaryArrayController] arrangedObjects] objectAtIndex:[[self primaryArrayController] selectionIndex]];
    if ([curFamily headOfFamily] && [curFamily.headOfFamily primaryEmail])
    {
        for(FamilyMember *member in [curFamily members])
        {
            if (![member headValue])
            {
                Person *curPerson = [member personDetail];
                // first remove any address with the same label
                NSString *label = [curFamily.headOfFamily.primaryEmail label];
                for (EmailAddress *addr in [curPerson emailAddresses])
                {
                    if ([[addr label] isEqualToString:label])
                    {
                        NSError *error = nil;
                        [addr deleteEntity:&error];
                    }
                }
                
                // add the new address
                [curPerson addEmailAddressesObject:[curFamily.headOfFamily.primaryEmail duplicateEntity]];
            }
        }
    }
}

#pragma mark - Search Routines

- (IBAction)showSearchPopover:(id)sender
{
    if ([[self searchPopover] isShown])
    {
        [[self searchPopover] close];
    } else {
        [[self searchPopover] setAppearance:NSPopoverAppearanceHUD];
        [[self searchPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxXEdge];
    }
}

- (IBAction)performSearch:(id)sender
{
    NSPredicate *pred = [[self searchPredicateEditor] predicate];
    [[self primaryArrayController] setFilterPredicate:pred];
    [[self searchPopover] close];
}

#pragma mark - Donation Reports


- (IBAction)printYTDDonationReport:(id)sender
{
    NSPrintInfo *printInfo = [NSPrintInfo sharedPrintInfo];
    [printInfo setOrientation:NSPortraitOrientation];
    [printInfo setVerticalPagination: NSAutoPagination];
    [printInfo setHorizontallyCentered:YES];
    [printInfo setLeftMargin:0];
    [printInfo setRightMargin:0];
    [printInfo setTopMargin:0];
    [printInfo setBottomMargin:0];
    [[NSPrintOperation printOperationWithView:_reportView] runOperation];
    [_printPreviewWindow close];
}

- (IBAction)createYTDDonationReport:(id)sender {
    Family *curFamily = [[[self primaryArrayController] arrangedObjects] objectAtIndex:[[self primaryArrayController] selectionIndex]];
    // lets do this in html
    dpAppDelegate *appDelegate = [NSApp delegate];
    NSURL *fileUrl = [[appDelegate applicationFilesDirectory] URLByAppendingPathComponent:@"Temp_Print.html"];
    
    NSMutableString *htmlString = [[NSMutableString alloc] init];
    [htmlString appendString:[self _printStyles]];
    
    [htmlString appendFormat:@"<div style=\"page-break-after: always;\">"];
    [htmlString appendString:[self _givingStatementHeader:curFamily]];
    [htmlString appendString:[self _givingStatementTotals:curFamily]];
    [htmlString appendString:[self _givingStatementFundSummary:curFamily]];
    [htmlString appendString:[self _givingStatementFooter:curFamily]];
    for (FamilyMember *member in [self.familyMemberArrayController arrangedObjects])
    {
        if ([member personDetail] && [[member.personDetail donations] count] > 0)
        {
            [htmlString appendString:[self _givingStatementDetail:[member personDetail]]];
        }
    }
    [htmlString appendString:@"</div>"];
    
    [[htmlString dataUsingEncoding:NSUTF8StringEncoding] writeToURL:fileUrl atomically:YES];
    
    [[_reportView mainFrame] loadRequest:[NSURLRequest requestWithURL:fileUrl]];
    [_printPreviewWindow makeKeyAndOrderFront:self];
}

- (NSString *)_printStyles
{
    NSString *htmlString = @"<style>";
    htmlString = [htmlString stringByAppendingString:@"@media print {"];
    //htmlString = [htmlString stringByAppendingString:@"html body {width: 100%; height: 100%;}"];
    //htmlString = [htmlString stringByAppendingString:@"div#page_footer {position: fixed; bottom: 0; margin-bottom: 10px;}"];
    htmlString = [htmlString stringByAppendingString:@"}"];
    htmlString = [htmlString stringByAppendingString:@"</style>"];
    
    return htmlString;
}

- (NSString *)_givingStatementHeader:(Family *)family
{
    NSMutableString *header = [[NSMutableString alloc] init];
    
    //TODO pull church name, address and details from a database
    [header appendString:@"<div style=\"padding-bottom: 10px; padding-top: 30px;\"><table width=100%><tr>"];
    [header appendString:@"<td style=\"width: 50%;\">"];
    [header appendString:@"<div style=\"padding-bottom: 90px; font:8pt verdana,sans-serif;\">"];
    [header appendString:@"<b>Northwest Bible Church</b><br/>"];
    [header appendString:@"889 W Chapala Dr<br/>"];
    [header appendString:@"Tucson, AZ 85704<br/>"];
    [header appendString:@"520-544-7775<br/>"];
    [header appendString:@"</div>"];
    [header appendString:@"<div>"];
    [header appendFormat:@"<div style=\" font:8pt verdana,sans-serif;\"><b>%@</b><br/>", [family name]];
    BOOL addressFound = NO;
    if ([family headOfFamily])
    {
        if ([family.headOfFamily primaryAddress])
        {
            [header appendFormat:@"%@<br/>", [[[family.headOfFamily primaryAddress] fullAddress] stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"]];
            addressFound = YES;
        } else {
            for (MailingAddress *addr in [family.headOfFamily mailingAddresses])
            {
                if (!addressFound && [[addr label] isEqualToString:@"Home"])
                {
                    [header appendFormat:@"%@<br/>", [[addr fullAddress] stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"]];
                    addressFound = YES;
                }
            }
       }
    }
    
    if (!addressFound)
    {
        for (FamilyMember *member in [family members])
        {
            if ([member personDetail])
            {
                for (MailingAddress *addr in [member.personDetail mailingAddresses])
                {
                    if (!addressFound && [[addr label] isEqualToString:@"Home"])
                    {
                        [header appendFormat:@"%@<br/>", [[addr fullAddress] stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"]];
                        addressFound = YES;
                    }
                }
            }
        }
    }
    
    [header appendString:@"</div>"];
    [header appendString:@"</div>"];
    [header appendString:@"</td>"];
    
    
    [header appendString:@"<td style=\"width: 50%; vertical-align: top; font-size:8pt; font-family: Verdana;\">"];
    [header appendString:@"<b><font size=3>Giving Statement</font></b><br/>"];
    [header appendString:@"January 1, 2013 to December 31, 2013<br/>"];
    [header appendString:@"<p><b>Tax ID:</b> 94-2756941(US)</p>"];
    [header appendString:@"</td>"];
    [header appendString:@"</tr></table></div>"];
    
    return header;
}

- (NSString *)_givingStatementFundSummary:(Family *)family
{
    NSMutableString *htmlString = [[NSMutableString alloc] init];
    
    [htmlString appendString:@"<div style=\"padding-top: 15px;\"><table width=300px style=\"font-size: 8pt; font-family: Verdana;\"><tr>"];
    [htmlString appendString:@"<tr><td width=200px align=left style=\"border-bottom: 1px solid #000;\"><b>Fund Summary</b></td><td width=100px align=right style=\"border-bottom: 1px solid #000;\"><b>Total</b></td></tr>"];
    double totalDonations = 0;
    for (Fund *currentFund in [[self fundArrayController] arrangedObjects])
    {
        double fundTotal = 0;
        for (FamilyMember *member in [family members])
        {
            for (Donation *curDonation in [member.personDetail donations])
            {
                if ([[currentFund name] isEqualToString:[[curDonation fund] name]])
                {
                    fundTotal += [[curDonation amount] doubleValue];
                }
            }
        }
        if (fundTotal > 0)
        {
            [htmlString appendFormat:@"<tr><td width=200px align=left style=\"padding-left: 10px;\">%@</td><td width=100px style=\"text-align: right;\">%.2f</td></tr>", [currentFund name], fundTotal];
        }
        totalDonations += fundTotal;
    }
    [htmlString appendFormat:@"<tr><td width=200px align=left style=\"border-top: 1px solid #000;\"><b>Total Fund Contributions</b></td><td width=100px align=right style=\"border-top: 1px solid #000;\"><b>%.2f</b></td></tr>", totalDonations];
    [htmlString appendString:@"</table></div>"];
    
    return htmlString;
}

- (NSString *)_givingStatementTotals:(Family *)family
{
    NSMutableString *htmlString = [[NSMutableString alloc] init];
    
    double totalDeduct = 0;
    double totalNonDeduct = 0;
    double totalDonations = 0;
    
    for (FamilyMember *member in [family members])
    {
        totalDeduct += [self _totalDeductibleDonations:[member personDetail]];
        totalNonDeduct += [self _totalNonDeductibleDonations:[member personDetail]];
        totalDonations += [self _totalDonations:[member personDetail]];
    }
    [htmlString appendString:@"<div style=\"border: 2px solid black; margin-top: 35px;\"><table width=100% style=\"font-size: 8pt; font-family: Verdana; font-style: bold;\"><tr>"];
    [htmlString appendFormat:@"<td>Tax Deductible Total: %.2f</td>", totalDeduct];
    [htmlString appendFormat:@"<td>*Non-Tax Deductible total: %.2f</td>", totalNonDeduct];
    [htmlString appendFormat:@"<td>Total Contributions: %.2f</td>", totalDonations];
    [htmlString appendString:@"</tr></table></div>"];
    
    
    return htmlString;
}

- (NSString *)_givingStatementDetail:(Donor *)donor
{
    NSMutableString *htmlString = [[NSMutableString alloc] init];
    
    //TODO pull church name, address and details from a database
    [htmlString appendString:@"<div style=\"margin-top: 5px;\"><b><font size=3>Giving Details</font></b>"];
    [htmlString appendString:@"<table width=100% style=\"font-family: Verdana; font-size:5pt;\"><tr>"];
    [htmlString appendString:@"<td style=\"border-bottom: 1px solid black;\"><b>Date</b></td>"];
    [htmlString appendString:@"<td style=\"border-bottom: 1px solid black;\"><b>Ref</b></td>"];
    [htmlString appendString:@"<td style=\"border-bottom: 1px solid black;\"><b>Fund</b></td>"];
    [htmlString appendString:@"<td style=\"border-bottom: 1px solid black;\"><b>Amount</b></td>"];
    [htmlString appendString:@"<td>&nbsp;</td>"];
    [htmlString appendString:@"<td style=\"border-bottom: 1px solid black;\"><b>Date</b></td>"];
    [htmlString appendString:@"<td style=\"border-bottom: 1px solid black;\"><b>Ref</b></td>"];
    [htmlString appendString:@"<td style=\"border-bottom: 1px solid black;\"><b>Fund</b></td>"];
    [htmlString appendString:@"<td style=\"border-bottom: 1px solid black;\"><b>Amount</b></td>"];
    [htmlString appendString:@"<td>&nbsp;</td>"];
    [htmlString appendString:@"<td style=\"border-bottom: 1px solid black;\"><b>Date</b></td>"];
    [htmlString appendString:@"<td style=\"border-bottom: 1px solid black;\"><b>Ref</b></td>"];
    [htmlString appendString:@"<td style=\"border-bottom: 1px solid black;\"><b>Fund</b></td>"];
    [htmlString appendString:@"<td style=\"border-bottom: 1px solid black;\"><b>Amount</b></td>"];
    [htmlString appendString:@"</tr>"];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yy"];
    
    int donationCount = 0;
    [htmlString appendString:@"<tr>"];
    NSSortDescriptor *donationSort = [NSSortDescriptor sortDescriptorWithKey:@"batch.givingDate" ascending:YES];
    NSArray *sortedDonations = [[donor donations] sortedArrayUsingDescriptors:[NSArray arrayWithObjects:donationSort, nil]];
    
    for (Donation *currentDonation in sortedDonations)
    {
        donationCount++;
        [htmlString appendFormat:@"<td align=center>%@</td>", [dateFormat stringFromDate:[[currentDonation batch] givingDate]]];
        
        if ([currentDonation checkNumber])
        {
            [htmlString appendFormat:@"<td align=left>%@</td>", [currentDonation checkNumber]];
        } else {
            [htmlString appendString:@"<td align=center>cash</td>"];
        }
        
        [htmlString appendFormat:@"<td>%@</td>", [[currentDonation fund] name]];
        
        if ([currentDonation amount] && ![[currentDonation amount] isEqualToNumber:[NSDecimalNumber decimalNumberWithString:@"0.0"]])
        {
            [htmlString appendFormat:@"<td align=right style=\"padding-right: 3px;\">%.2f</td>", [[currentDonation amount] doubleValue]];
        } else if ([currentDonation nonDeductAmount] && ![[currentDonation nonDeductAmount] isEqualToNumber:[NSDecimalNumber decimalNumberWithString:@"0.0"]])
        {
            [htmlString appendFormat:@"<td align=right style=\"padding-right: 3px;\">%.2f*</td>", [[currentDonation nonDeductAmount] doubleValue]];
        } else {
            [htmlString appendString:@"<td align=center style=\"padding-right: 3px;\"></td>"];
        }
        
        if (remainder(donationCount,3) == 0)
        {
            [htmlString appendString:@"</tr><tr>"];
        } else {
            //space cell
            [htmlString appendString:@"<td></td>"];
        }
    }
    [htmlString appendString:@"</tr>"];
    [htmlString appendString:@"</table></div>"];
    
    return htmlString;
}

- (NSString *)_givingStatementFooter:(Family *)family
{
    NSMutableString *htmlString = [[NSMutableString alloc] init];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    [htmlString appendString:@"<div id=\"page_footer\" style=\"margin-top: 20px; font-family: Verdana; font-size: 8pt; border-top: 2px solid black\">"];
    [htmlString appendString:@"The donor has not received any goods, services or benefits in connection with any contribution. The only value the donor received consisted of intangible religious benefits.<br />"];
    //[htmlString appendFormat:@"Printed On: %@", [dateFormat stringFromDate:[NSDate date]]];
    [htmlString appendString:@"<div style=\"padding-top: 80px; padding-bottom: 20px; font-face: Verdana; font-size: 9pt;\">"];
    [htmlString appendString:@"Ronald W. Brown </br> Treasurer"];
    [htmlString appendString:@"</div>"];
    [htmlString appendString:@"</div>"];
    
    return htmlString;
}

- (double)_totalDonations:(Donor *)donor
{
    double total = 0;
    for (Donation *curDonation in [donor donations])
    {
        total = total + [[curDonation amount] doubleValue];
        if ([curDonation nonDeductAmount])
        {
            total = total + [[curDonation nonDeductAmount] doubleValue];
        }
    }
    return total;
}

- (double)_totalDeductibleDonations:(Donor *)donor
{
    double total = 0;
    for (Donation *curDonation in [donor donations])
    {
        total = total + [[curDonation amount] doubleValue];
    }
    return total;
}

- (double)_totalNonDeductibleDonations:(Donor *)donor
{
    double total = 0;
    for (Donation *curDonation in [donor donations])
    {
        if ([curDonation nonDeductAmount])
        {
            total = total + [[curDonation nonDeductAmount] doubleValue];
        }
    }
    return total;
}


#pragma mark - Export Routines

- (IBAction)showItemExportMenu:(id)sender
{
    NSRect frame = [(NSButton *)sender frame];
    NSPoint menuOrigin = [[(NSButton *)sender superview] convertPoint:NSMakePoint(frame.origin.x, frame.origin.y)
                                                               toView:nil];
    
    NSEvent *event =  [NSEvent mouseEventWithType:NSLeftMouseDown
                                         location:menuOrigin
                                    modifierFlags:0
                                        timestamp:0
                                     windowNumber:[[(NSButton *)sender window] windowNumber]
                                          context:[[(NSButton *)sender window] graphicsContext]
                                      eventNumber:0
                                       clickCount:1
                                         pressure:1];
    
    [NSMenu popUpContextMenu:_itemExportMenu withEvent:event forView:(NSButton *)sender];
}

-(IBAction)showExport:(id)sender
{
    if ([[self exportPopover] isShown])
    {
        [[self exportPopover] close];
    } else {
        [[self exportPopover] setAppearance:NSPopoverAppearanceHUD];
        [[self exportPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxXEdge];
    }
}

- (IBAction)exportFamilyHeadContactInfo:(id)sender
{
    NSURL *fileURL = [self _selectExportFile];
    
    if (fileURL)
    {
        NSOutputStream *output = [NSOutputStream outputStreamWithURL:fileURL append:NO];
        CHCSVWriter *writer = [[CHCSVWriter alloc] initWithOutputStream:output encoding:NSUTF8StringEncoding delimiter:','];
        [writer writeLineOfFields:@[@"First Name", @"Last Name", @"Adult Names", @"Children Names", @"Member Names",
                                    @"Phone Number", @"Email", @"Address 1", @"Address 2", @"City", @"State", @"Zip"]];
      
        //loop through current arranged people and create csv file
        for (Family *curFamily in [[self primaryArrayController] arrangedObjects])
        {
            Person *curPerson = [curFamily headOfFamily];
            NSString *lname = [curFamily lastName];
            NSString *fname = [curFamily name];
            NSString *adultNames = @"";
            if ([curFamily adults])
            {
                int adultCount = 0;
                NSSortDescriptor *adultSort = [NSSortDescriptor sortDescriptorWithKey:@"gender" ascending:NO selector:@selector(compare:)];
                NSArray *sortedAdults = [[curFamily adults] sortedArrayUsingDescriptors:[NSArray arrayWithObject:adultSort]];
                for (Person *curAdult in sortedAdults)
                {
                    adultCount++;
                    adultNames = [adultNames stringByAppendingFormat:@"%@", [curAdult nickName]];
                    if (adultCount != [[curFamily adults] count]) {
                        adultNames = [adultNames stringByAppendingString:@" & "];
                    }
                }
            }
            NSString *childrenNames = @"";
            if ([curFamily children])
            {
                int childCount = 0;
                NSSortDescriptor *childSort = [NSSortDescriptor sortDescriptorWithKey:@"birthday" ascending:YES selector:@selector(compare:)];
                NSArray *sortedChildren = [[curFamily children] sortedArrayUsingDescriptors:[NSArray arrayWithObject:childSort]];
                for (Person *curChild in sortedChildren)
                {
                    childCount++;
                    childrenNames = [childrenNames stringByAppendingFormat:@"%@", [curChild nickName]];
                    if (childCount != [[curFamily children] count]) {
                        childrenNames = [childrenNames stringByAppendingString:@", "];
                    }
                }
            }
            NSString *memberNames = [curFamily memberNames];
            NSString *line1 = @"";
            NSString *line2 = @"";
            NSString *city = @"";
            NSString *state = @"";
            NSString *postalCode = @"";
            if ([curPerson primaryAddress])
            {
                line1 = [curPerson.primaryAddress addressLine1];
                line2 = [curPerson.primaryAddress addressLine2];
                city = [curPerson.primaryAddress city];
                state = [curPerson.primaryAddress state];
                postalCode = [curPerson.primaryAddress postalCode];
            }
            NSString *phone = @"";
            if ([curPerson primaryPhone])
            {
                phone = [curPerson.primaryPhone number];
            }
            NSString *email = @"";
            if ([curPerson primaryEmail])
            {
                email = [curPerson.primaryEmail address];
            }
            
            if (!line1) { line1 = @""; }
            if (!line2) { line2 = @""; }
            if (!city) { city = @""; }
            if (!state) { state = @""; }
            if (!postalCode) { postalCode = @""; }
            if (!phone) { phone = @""; }
            if (!email) { email = @""; }
            
            
            [writer writeLineOfFields:@[fname, lname, adultNames, childrenNames, memberNames, phone, email, line1, line2, city, state, postalCode]];
        }
        
        [writer closeStream];
    }
    
    
    //close the export popover
    [_exportPopover close];
    NSAlert *alert = [NSAlert alertWithMessageText: @"Finished"
                                     defaultButton:@"OK"
                                   alternateButton:nil
                                       otherButton:nil
                         informativeTextWithFormat:@"Export file created."];
    [alert runModal];

}

#pragma mark - Add Existing Member Routines

- (void)addExistingMember:(id)sender
{
    // create new FamilyMember and connect new person
    FamilyMember *newMember = [FamilyMember createInContext:[[NSApp delegate] managedObjectContext]];
    
    // add the member to the current familiy
    [[self familyMemberArrayController] insertObject:newMember atArrangedObjectIndex:0];
    
    [[self existingPersonPopover] setAppearance:NSPopoverAppearanceHUD];
    [[self existingPersonPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxYEdge];
}

- (IBAction)selectPerson:(id)sender
{
    Person *selectedPerson = [[_personArrayController arrangedObjects] objectAtIndex:[_personArrayController selectionIndex]];

    // get FamilyMember and connect new person
    FamilyMember *curMember = [[[self familyMemberArrayController] arrangedObjects] objectAtIndex:[[self familyMemberArrayController] selectionIndex]];
    [curMember setPersonDetail:selectedPerson];

    [_personPopover close];
    [_existingPersonField setStringValue:[selectedPerson displayName]];
}

- (IBAction)cancelAddExisting:(id)sender {
    FamilyMember *curMember = [[[self familyMemberArrayController] arrangedObjects] objectAtIndex:[[self familyMemberArrayController] selectionIndex]];
    Person *curPerson = [curMember personDetail];
    if ([curPerson isInserted])
    {
        NSError *error = nil;
        [curPerson deleteEntity:&error];
        [[self familyMemberArrayController] remove:sender];
    }

    [[self existingPersonPopover] close];
}

- (IBAction)finishAddExisting:(id)sender {
    // get FamilyMember and connect new person
    FamilyMember *curMember = [[[self familyMemberArrayController] arrangedObjects] objectAtIndex:[[self familyMemberArrayController] selectionIndex]];

    if ([curMember personDetail]) {
        Person *selectedPerson = [curMember personDetail];
        // make a significant date for adding of new person to family
        SignificantDate *famDate = [SignificantDate createInContext:[[NSApp delegate] managedObjectContext]];
        [famDate setEventDate:[NSDate date]];
        [famDate setEventTitle:@"Added to Family"];
        Family *curFamily = [[[self primaryArrayController] arrangedObjects] objectAtIndex:[[self primaryArrayController] selectionIndex]];
        [famDate setEventDescription:[NSString stringWithFormat:@"Added to %@ family", [curFamily name]]];
        [selectedPerson addSignificantDatesObject:famDate];
    }
    
    [_existingPersonPopover close];
}

- (void)controlTextDidBeginEditing:(NSNotification *)obj
{
    if ([obj object] == _existingPersonField)
    {
        if (!_personPopover.shown)
        {
            [[self personPopover] showRelativeToRect:[_existingPersonField bounds] ofView:_existingPersonField preferredEdge:NSMaxXEdge];
            [_existingPersonField becomeFirstResponder];
        }
    }
}

- (void)controlTextDidChange:(NSNotification *)obj
{
    if ([obj object] == _existingPersonField)
    {
        NSPredicate *personFilterPred = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@", [_existingPersonField stringValue]];
        [_personArrayController setFilterPredicate:personFilterPred];
        [_personArrayController rearrangeObjects];
        if (!_personPopover.shown)
        {
            [[self personPopover] showRelativeToRect:[_existingPersonField bounds] ofView:_existingPersonField preferredEdge:NSMaxXEdge];
            [_existingPersonField becomeFirstResponder];
        }
    }
}

- (void)controlTextDidEndEditing:(NSNotification *)obj
{
    if ([obj object] == _existingPersonField && ![self isTextFieldInFocus:_existingPersonField])
    {
        if (_personPopover.shown)
        {
            [_personPopover close];
        }
        // now make sure a legit donor is set
    }
}

#pragma mark - UI Utilities

- (BOOL)isTextFieldInFocus:(NSTextField *)textField
{
	BOOL inFocus = NO;
	
	inFocus = ([[[textField window] firstResponder] isKindOfClass:[NSTextView class]]
			   && [[textField window] fieldEditor:NO forObject:nil]!=nil
			   && [textField isEqualTo:(id)[(NSTextView *)[[textField window] firstResponder]delegate]]);
	
	return inFocus;
}


#pragma mark - Select File
- (NSURL *) _selectExportFile
{
    // Create the File Open Dialog class.
    NSSavePanel *saveDlg = [NSSavePanel savePanel];
    
    // Enable the selection of files in the dialog.
    [saveDlg setAllowedFileTypes:@[@"csv", @"txt"]];
    
    // Display the dialog. If the OK button was pressed,
    // process the files.
    if ( [saveDlg runModal] == NSOKButton )
    {
        return [saveDlg URL];
    }
    return nil;
}

#pragma mark - Alerts

- (void)showAlert:(NSString *)title WithError:(NSError *)error
{
    NSAlert *alert = [NSAlert alertWithMessageText:title defaultButton:@"OK" alternateButton:@"Cancel" otherButton:nil informativeTextWithFormat:@"%@", [error localizedDescription]];
    [alert beginSheetModalForWindow:[[self view] window] modalDelegate:self  didEndSelector:@selector(alertDidEnd:returnCode:contextInfo:) contextInfo:nil];
}

- (void) alertDidEnd:(NSAlert *)alert returnCode:(int)returnCode contextInfo:(void *)contextInfo
{
    // don't need to do anything for now
}



@end
