//
//  dpBatchViewController.h
//  congregate
//
//  Created by Mike Flores on 12/18/12.
//  Copyright (c) 2012 digiputty, llc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "DPViewController.h"

@interface dpBatchViewController : DPViewController {}
@property (strong) IBOutlet NSArrayController *donationsArrayController;
@property (strong) IBOutlet NSArrayController *donorArrayConroller;
@property (weak) IBOutlet NSPopUpButton *firstEditField;
@property (strong) IBOutlet NSPopover *donationPopover;
@property (strong) IBOutlet NSPopover *donorPopover;
@property (weak) IBOutlet NSTextField *donorField;
@property (weak) IBOutlet NSTableView *donationsTableView;
@property (weak) IBOutlet NSTableView *batchTableView;
@property (strong) IBOutlet NSPopover *batchPopover;

@property (strong) IBOutlet NSView *batchReportView;
@property (weak) IBOutlet NSCollectionView *batchReportCollectionView;
@property (strong) IBOutlet NSView *reconciliationReportView;
@property NSMutableDictionary *batchFundTotals;

- (IBAction)addBatch:(id)sender;
- (IBAction)finishBatchEdit:(id)sender;
- (IBAction)cancelBatchEdit:(id)sender;
- (IBAction)deleteBatch:(id)sender;
- (IBAction)donationFundSelected:(id)sender;
- (IBAction)addNewDonor:(id)sender;
- (IBAction)printBatchReport:(id)sender;
- (IBAction)printReconciliationReport:(id)sender;
- (IBAction)addDonation:(id)sender;
- (IBAction)selectDonor:(id)sender;
- (IBAction)finishDonationEdit:(id)sender;
- (IBAction)cancelDonationEdit:(id)sender;
- (IBAction)deleteDonation:(id)sender;

@end
