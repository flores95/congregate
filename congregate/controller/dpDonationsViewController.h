//
//  dpDonationsViewController.h
//  congregate
//
//  Created by Mike Flores on 12/18/12.
//  Copyright (c) 2012 digiputty, llc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "DPViewController.h"

@interface dpDonationsViewController : DPViewController {}
@property (strong) IBOutlet NSArrayController *donorArrayConroller;
@property (strong) IBOutlet NSPopover *donationPopover;
@property (strong) IBOutlet NSPopover *donorPopover;
@property (weak) IBOutlet NSTextField *donorField;
@property (weak) IBOutlet NSTableView *donationsTableView;

- (IBAction)donationFundSelected:(id)sender;
- (IBAction)addNewDonor:(id)sender;
- (IBAction)addDonation:(id)sender;
- (IBAction)selectDonor:(id)sender; 
- (IBAction)finishDonationEdit:(id)sender;
- (IBAction)cancelDonationEdit:(id)sender;
- (IBAction)deleteDonation:(id)sender;
- (IBAction)delete:(id)sender;

@end
