//
//  dpDonorsViewController.h
//  congregate
//
//  Created by Mike Flores on 12/27/12.
//  Copyright (c) 2012 digiputty, llc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "DPViewController.h"
#import <WebKit/WebKit.h>

@interface dpDonorsViewController : DPViewController {}

@property (weak) IBOutlet WebView *reportView;
@property (strong) IBOutlet NSWindow *printPreviewWindow;
@property (strong) IBOutlet NSArrayController *donationsArrayController;
@property (strong) IBOutlet NSArrayController *fundArrayContoller;
@property (strong) IBOutlet NSMenu *exportMenu;
@property (strong) IBOutlet NSMenu *itemExportMenu;


- (IBAction)exportMenu:(id)sender;
- (IBAction)showItemExportMenu:(id)sender;
- (IBAction)printDonationReport:(id)sender;
- (IBAction)createYTDDonationReports:(id)sender;
- (IBAction)createDonationReport:(id)sender;

@end
