//
//  dpPreferencesWindowController.h
//  congregate
//
//  Created by Mike Flores on 2/25/13.
//  Copyright (c) 2013 digiputty, llc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface dpPreferencesWindowController : NSWindowController

@property (weak) IBOutlet NSTextField *alertNumberField;
@property (strong) NSMutableArray *labelPrinters;

@end
