//
//  dpUsersViewController.h
//  congregate
//
//  Created by Mike Flores on 1/10/13.
//  Copyright (c) 2013 digiputty, llc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "DPViewController.h"

@interface dpUsersViewController : DPViewController

@property (strong) IBOutlet NSArrayController *personArrayController;
@property (weak) IBOutlet NSTableView *userTableView;
@property (strong) IBOutlet NSPopover *userPopover;
@property (weak) IBOutlet NSSecureTextField *passwordEditField;
@property (weak) IBOutlet NSSecureTextField *confirmPasswordEditField;

- (IBAction)addUser:(id)sender;
- (IBAction)deleteUser:(id)sender;
- (IBAction)finishUserEdit:(id)sender;
- (IBAction)cancelUserEdit:(id)sender;

@end
