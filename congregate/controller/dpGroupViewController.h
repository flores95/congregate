//
//  dpGroupViewController.h
//  congregate
//
//  Created by Mike Flores on 12/5/12.
//  Copyright (c) 2012 digiputty, llc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "DPViewController.h"

@interface dpGroupViewController : DPViewController

@property (weak) IBOutlet NSTextField *firstEditField;
@property (weak) IBOutlet NSTextField *memberNameField;
@property (strong) IBOutlet NSPopover *memberPopover;
@property (strong) IBOutlet NSPopover *personPopover;
@property (weak) IBOutlet NSTextField *personNameField;

@property (strong) IBOutlet NSArrayController *peopleArrayController;
@property (strong) IBOutlet NSArrayController *membersArrayController;

@property (strong) IBOutlet NSPopover *groupPopover;

- (IBAction)newItem:(id)sender;
- (IBAction)addGroup:(id)sender;
- (IBAction)finishGroupEdit:(id)sender;
- (IBAction)cancelGroupEdit:(id)sender;

- (IBAction)addMember:(id)sender;
- (IBAction)finishMemberEdit:(id)sender;
- (IBAction)cancelMemberEdit:(id)sender;
- (IBAction)removeMember:(id)sender;


@end
