//
//  dpGivingViewController.m
//  congregate
//
//  Created by Mike Flores on 12/13/12.
//  Copyright (c) 2012 digiputty, llc. All rights reserved.
//

#import "dpGivingViewController.h"

@interface dpGivingViewController ()

@end

@implementation dpGivingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    // setup some appearance stuff
    //[_navBarView setBackground:[NSColor colorWithDeviceRed:160.0/255.0 green:200.0/255.0 blue:50/255.0 alpha:0.8]];
    //[_navBarView needsDisplay];
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
}

- (void)viewDidLoad
{
    [self setSubViewButtons:[NSArray arrayWithObjects: _batchButton,
                                                       _donationsButton,
                                                       _fundsButton,
                                                       _donorsButton,
                                                       _companiesButton,
                                                       nil]];
    [_donorsButton performClick:nil];

}

@end
