//
//  dpDonationsViewController.m
//  congregate
//
//  Created by Mike Flores on 12/18/12.
//  Copyright (c) 2012 digiputty, llc. All rights reserved.
//

#import "dpDonationsViewController.h"
#import "GivingBatch.h"
#import "Donation.h"
#import "Donor.h"
#import "Fund.h"
#import "GivingBatch.h"
#import "dpAppDelegate.h"
#import <WebKit/WebKit.h>

@interface dpDonationsViewController ()

@end

@implementation dpDonationsViewController

#pragma - Init and Setup

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];

    [_donationsTableView setTarget:self];
    [_donationsTableView setDoubleAction:@selector(editDonation:)];
    // sort the arrays
    NSSortDescriptor *donorSort = [NSSortDescriptor sortDescriptorWithKey:@"displayName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    [_donorArrayConroller setSortDescriptors:[NSArray arrayWithObject:donorSort]];
}

#pragma - IBActions

- (IBAction)newItem:(id)sender
{
}

- (IBAction)donationFundSelected:(id)sender
{
    //NSComboBoxCell *fundComboBox = sender;
    //NSLog(@"something was selected");
}

- (IBAction)addNewDonor:(id)sender
{
    //NSComboBoxCell *fundComboBox = sender;
    NSLog(@"add new donor was selected");
}

#pragma mark - Donations

- (IBAction)addDonation:(id)sender
{
    if ([self selectedObject])
    {
        Donation *newDonation = [Donation createInContext:[[NSApp delegate] managedObjectContext]];
        //TODO hack here for default fund
        Fund *defaultFund = [Fund findFirstByAttribute:@"internalName" WithValue:@"1000" InContext:[[NSApp delegate] managedObjectContext]];
        [newDonation setFund:defaultFund];
        
        // select the new donation
        [[self primaryArrayController] insertObject:newDonation atArrangedObjectIndex:0];
        
        // show a popover to edit this new donation
        [_donorField setStringValue:@""];
        [[self donationPopover] setAppearance:NSPopoverAppearanceHUD];
        [[self donationPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMinYEdge];        
    }    
}

- (IBAction)finishDonationEdit:(id)sender
{
    Donation *currentDonation = [[[self primaryArrayController] arrangedObjects] objectAtIndex:[[self primaryArrayController] selectionIndex]];
    NSError *error = nil;
    if ([currentDonation validateEntity:&error])
    {
        [_donorPopover close];
        [_donationPopover close];
    } else {
        NSAlert *alert = [NSAlert alertWithMessageText:@"Problem with donation information:" defaultButton:@"OK" alternateButton:@"Cancel" otherButton:nil informativeTextWithFormat:@"%@", [error localizedDescription]];
        [alert beginSheetModalForWindow:[[self view] window] modalDelegate:self  didEndSelector:@selector(alertDidEnd:returnCode:contextInfo:) contextInfo:nil];
    }
}

- (void) alertDidEnd:(NSAlert *)alert returnCode:(int)returnCode contextInfo:(void *)contextInfo
{
    // don't need to do anything for now
}

- (IBAction)cancelDonationEdit:(id)sender
{
    Donation *currentDonation = [[[self primaryArrayController] arrangedObjects] objectAtIndex:[[self primaryArrayController] selectionIndex]];
    if ([currentDonation isInserted])
    {
        NSError *error = nil;
        [currentDonation deleteEntity:&error];
    }
    [_donorPopover close];
    [_donationPopover close];
}

- (IBAction)deleteDonation:(id)sender
{
    Donation *currentDonation = [[[self primaryArrayController] arrangedObjects] objectAtIndex:[[self primaryArrayController] selectionIndex]];
    NSError *error = nil;
    [currentDonation deleteEntity:&error];
    [_donorPopover close];
    [_donationPopover close];
}

// First Responder for Main Menu and Keyboard Shortcut
- (IBAction)delete:(id)sender
{
    for (id obj in [[self primaryArrayController] selectedObjects])
    {
        if ([obj isKindOfClass:[Donation class]])
        {
            NSError *error = nil;
            [(Donation *)obj deleteEntity:&error];
        }
    }
}

- (void)editDonation:(id)sender
{
    if ([_donationsTableView numberOfSelectedRows] == 1) {
        Donation *currentDonation = [[[self primaryArrayController] arrangedObjects] objectAtIndex:[[self primaryArrayController] selectionIndex]];
        if ([currentDonation donorName])
        {
            [_donorField setStringValue:[currentDonation donorName]];
        }
        [[self donationPopover] setAppearance:NSPopoverAppearanceHUD];
        [[self donationPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMinYEdge];
    }
}

#pragma mark - Donors

- (IBAction)selectDonor:(id)sender
{
    Donor *selectedDonor = [[_donorArrayConroller arrangedObjects] objectAtIndex:[_donorArrayConroller selectionIndex]];
    Donation *currentDonation = [[[self primaryArrayController] arrangedObjects] objectAtIndex:[[self primaryArrayController] selectionIndex]];
    [currentDonation setDonor:selectedDonor];
    [_donorPopover close];
    [_donorField setStringValue:[selectedDonor displayName]];
    [[self primaryArrayController] rearrangeObjects];
}

- (void)controlTextDidBeginEditing:(NSNotification *)obj
{
    if ([obj object] == _donorField)
    {
        if (!_donorPopover.shown)
        {
            //[[self donorPopover] setAppearance:NSPopoverAppearanceHUD];
            [[self donorPopover] showRelativeToRect:[_donorField bounds] ofView:_donorField preferredEdge:NSMaxXEdge];
            [_donorField becomeFirstResponder];
        }
    }    
}

- (void)controlTextDidChange:(NSNotification *)obj
{
    if ([obj object] == _donorField)
    {
        NSPredicate *donorFilterPred = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@", [_donorField stringValue]];
        [_donorArrayConroller setFilterPredicate:donorFilterPred];
        [_donorArrayConroller rearrangeObjects];
        if (!_donorPopover.shown)
        {
            //[[self donorPopover] setAppearance:NSPopoverAppearanceHUD];
            [[self donorPopover] showRelativeToRect:[_donorField bounds] ofView:_donorField preferredEdge:NSMaxXEdge];
            [_donorField becomeFirstResponder];
        }        
    }
}

- (void)controlTextDidEndEditing:(NSNotification *)obj
{
    if ([obj object] == _donorField && ![self isTextFieldInFocus:_donorField])
    {
        if (_donorPopover.shown)
        {
            [_donorPopover close];
        }
        // now make sure a legit donor is set
    }
}

#pragma mark - UI Utilities

- (BOOL)isTextFieldInFocus:(NSTextField *)textField
{
	BOOL inFocus = NO;
	
	inFocus = ([[[textField window] firstResponder] isKindOfClass:[NSTextView class]]
			   && [[textField window] fieldEditor:NO forObject:nil]!=nil
			   && [textField isEqualTo:(id)[(NSTextView *)[[textField window] firstResponder]delegate]]);
	
	return inFocus;
}

@end
