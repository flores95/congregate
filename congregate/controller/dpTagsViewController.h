//
//  dpTagsViewController.h
//  congregate
//
//  Created by Mike Flores on 1/7/13.
//  Copyright (c) 2013 digiputty, llc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "DPViewController.h"

@interface dpTagsViewController : DPViewController
@property (strong) IBOutlet NSArrayController *itemsArrayController;
- (IBAction)removeTag:(id)sender;

@end
