//
//  dpBatchViewController.m
//  congregate
//
//  Created by Mike Flores on 12/18/12.
//  Copyright (c) 2012 digiputty, llc. All rights reserved.
//

#import "dpBatchViewController.h"
#import "GivingBatch.h"
#import "Donation.h"
#import "Donor.h"
#import "Fund.h"
#import "GivingBatch.h"
#import "dpAppDelegate.h"
#import <WebKit/WebKit.h>

@interface dpBatchViewController ()

@end

@implementation dpBatchViewController

#pragma - Init and Setup

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [_batchTableView setTarget:self];
    [_batchTableView setDoubleAction:@selector(editBatch:)];

    [_donationsTableView setTarget:self];
    [_donationsTableView setDoubleAction:@selector(editDonation:)];
    // sort the arrays
    NSSortDescriptor *donorSort = [NSSortDescriptor sortDescriptorWithKey:@"displayName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    [_donorArrayConroller setSortDescriptors:[NSArray arrayWithObject:donorSort]];
    NSSortDescriptor *donationSort = [NSSortDescriptor sortDescriptorWithKey:@"donorName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    [_donationsArrayController setSortDescriptors:[NSArray arrayWithObject:donationSort]];
}

#pragma - IBActions

- (IBAction)newItem:(id)sender
{
    [super newItem:sender];
    [[self primaryArrayController] add:sender];
}

- (IBAction)donationFundSelected:(id)sender
{
    //NSComboBoxCell *fundComboBox = sender;
    //NSLog(@"something was selected");
}

- (IBAction)addNewDonor:(id)sender
{
    //NSComboBoxCell *fundComboBox = sender;
    NSLog(@"add new donor was selected");
}

#pragma mark - Batch Edit

- (IBAction)addBatch:(id)sender
{
    GivingBatch *newBatch = [GivingBatch createInContext:[[NSApp delegate] managedObjectContext]];
    [newBatch setOpenValue:YES];
    
    // select the new donation
    [[self primaryArrayController] insertObject:newBatch atArrangedObjectIndex:0];
    
    // show a popover to edit this new batch
    [[self batchPopover] setAppearance:NSPopoverAppearanceHUD];
    [[self batchPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxYEdge];
}

- (void)editBatch:(id)sender
{
    [[self batchPopover] setAppearance:NSPopoverAppearanceHUD];
    [[self batchPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxXEdge];
}

- (IBAction)finishBatchEdit:(id)sender
{
    GivingBatch *currentBatch = (GivingBatch *)[self selectedObject];
    NSError *error = nil;
    if ([currentBatch validateEntity:&error])
    {
        [_batchPopover close];
    } else {
        NSAlert *alert = [NSAlert alertWithMessageText:@"Problem with batch information:" defaultButton:@"OK" alternateButton:@"Cancel" otherButton:nil informativeTextWithFormat:@"%@", [error localizedDescription]];
        [alert beginSheetModalForWindow:[[self view] window] modalDelegate:self  didEndSelector:@selector(alertDidEnd:returnCode:contextInfo:) contextInfo:nil];
    }
}

- (IBAction)cancelBatchEdit:(id)sender
{
    GivingBatch *currentBatch = (GivingBatch *)[self selectedObject];
    if ([currentBatch isInserted])
    {
        [[self primaryArrayController] remove:sender];
    }
    [_batchPopover close];
}

- (IBAction)deleteBatch:(id)sender
{
    GivingBatch *currentBatch = (GivingBatch *)[self selectedObject];
    if (currentBatch) {
        NSError *error = nil;
        if (![currentBatch deleteEntity:&error])
        {
            NSLog(@"XXXXXX ERROR DELETING BATCH: %@", [error localizedDescription]);
        }
    }
    [_batchPopover close];
}

#pragma mark - Printing

- (IBAction)printBatchReport:(id)sender
{
    // lets do this in html
    WebView *webView = [[WebView alloc] init];
    dpAppDelegate *appDelegate = [NSApp delegate];
    NSURL *fileUrl = [[appDelegate applicationFilesDirectory] URLByAppendingPathComponent:@"Temp_Print.html"];
    
        
    NSMutableString *htmlString = [[NSMutableString alloc] init];
    GivingBatch *currentBatch = (GivingBatch *)[self selectedObject];
    
    [htmlString appendFormat:@"<h2>Batch Report - %@</h2>", [currentBatch name]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    
    [htmlString appendString:@"<table width=100%><tr>"];
    [htmlString appendFormat:@"<td>Date: %@</td>", [dateFormat stringFromDate:[currentBatch givingDate]]];
    [htmlString appendFormat:@"<td>Count: %lu</td><td>Total: %f</td>", (unsigned long)[[currentBatch donations] count], [self _batchTotal:currentBatch]];
    [htmlString appendString:@"</tr></table><hr/><br/>"];
    
    
    [htmlString appendString:@"<table width=100% style=\"font-size: 10pt;\"><tr style=\"border-bottom: 1px solid #000;\">"];
    [htmlString appendString:@"<td align=left>Donor</td><td align=left>Fund</td><td align=right>Amount</td><td align=right>Non-Deduct</td><td align=center>Type</td><td align=center>Split</td><td align=right>Check #</td></tr>"];
    for (Donation *currentDonation in [_donationsArrayController arrangedObjects])
    {
        [htmlString appendString:@"<tr>"];
        [htmlString appendFormat:@"<td><font size=-2 face=verdana>%@</font></td>", [currentDonation donorName]];
        [htmlString appendFormat:@"<td><font size=-2 face=verdana>%@</font></td>", [[currentDonation fund] name]];
        if ([currentDonation amount] && ![[currentDonation amount] isEqualToNumber:[NSDecimalNumber decimalNumberWithString:@"0.0"]])
        {
            [htmlString appendFormat:@"<td align=right><font size=-2 face=verdana>%@</font></td>", [currentDonation amount]];
        } else {
            [htmlString appendString:@"<td align=center><font size=-2 face=verdana></font></td>"];
        }
        if ([currentDonation nonDeductAmount] && ![[currentDonation nonDeductAmount] isEqualToNumber:[NSDecimalNumber decimalNumberWithString:@"0.0"]])
        {
            [htmlString appendFormat:@"<td align=right><font size=-2 face=verdana>%@</font></td>", [currentDonation nonDeductAmount]];
        } else {
            [htmlString appendString:@"<td align=center><font size=-2 face=verdana></font></td>"];
        }
        [htmlString appendFormat:@"<td><font size=-2 face=verdana>%@</font></td>", [currentDonation type]];
        if ([currentDonation splitValue])
        {
            [htmlString appendString:@"<td align=center><font size=-2 face=verdana>X</font></td>"];
        } else {
            [htmlString appendString:@"<td align=center><font size=-2 face=verdana></font></td>"];
        }
        if ([currentDonation checkNumber])
        {
            [htmlString appendFormat:@"<td align=right><font size=-2 face=verdana>%@</font></td>", [currentDonation checkNumber]];
        } else {
            [htmlString appendString:@"<td align=center><font size=-2 face=verdana></font></td>"];
        }
        [htmlString appendString:@"</tr>"];
    }
    [htmlString appendString:@"</table>"];
    
    [[htmlString dataUsingEncoding:NSUTF8StringEncoding] writeToURL:fileUrl atomically:YES];
    
    [[webView mainFrame] loadRequest:[NSURLRequest requestWithURL:fileUrl]];
    [webView setFrame:NSMakeRect(0, 0, 552, 20*[[_donationsArrayController arrangedObjects] count])];

    NSPrintInfo *printInfo = [NSPrintInfo sharedPrintInfo];
    [printInfo setOrientation:NSPortraitOrientation];
    [printInfo setVerticalPagination: NSAutoPagination];
    [printInfo setHorizontallyCentered:YES];
    [printInfo setLeftMargin:0];
    [printInfo setRightMargin:0];
    [printInfo setTopMargin:0];
    [printInfo setBottomMargin:0];
    [[NSPrintOperation printOperationWithView:webView] runOperation];
}

- (IBAction)printReconciliationReport:(id)sender
{
    [self calculateFundTotals];
    NSPrintInfo *printInfo = [NSPrintInfo sharedPrintInfo];
    [printInfo setOrientation:NSPortraitOrientation];
    [printInfo setLeftMargin:0];
    [printInfo setRightMargin:0];
    [printInfo setTopMargin:0];
    [printInfo setBottomMargin:0];
    [[NSPrintOperation printOperationWithView:_reconciliationReportView] runOperation];
}

- (double)_batchTotal:(GivingBatch *)batch
{
    double total = 0;
    for (Donation *curDonation in [batch donations])
    {
        total = total + [[curDonation amount] doubleValue];
        if ([curDonation nonDeductAmount])
        {
            total = total + [[curDonation nonDeductAmount] doubleValue];
        }
    }
    return total;
}

- (void)calculateFundTotals
{
    // clear the old data
    NSMutableDictionary *totals = [[NSMutableDictionary alloc] init];
    
    // get the new data
    if ([self selectedObject])
    {
        GivingBatch *batch = (GivingBatch *)[self selectedObject];
        NSSet *funds = [[batch donations] valueForKeyPath:@"@distinctUnionOfObjects.fund"];
        for (Fund *batchFund in funds)
        {
            NSPredicate *filterPred = [NSPredicate predicateWithFormat:@"fund.sourceObjectId == %@", [batchFund sourceObjectId]];
            NSSet *fundDonations = [[batch donations] filteredSetUsingPredicate:filterPred];
            NSDecimalNumber *fundTotal = [fundDonations valueForKeyPath:@"@sum.amount"];
            [totals setObject:fundTotal forKey:[batchFund name]];
        }
    }
    [self setBatchFundTotals:totals];
}

#pragma mark - Donations

- (IBAction)addDonation:(id)sender
{
    if ([self selectedObject])
    {
        Donation *newDonation = [Donation createInContext:[[NSApp delegate] managedObjectContext]];
        //TODO hack here for default fund
        Fund *defaultFund = [Fund findFirstByAttribute:@"internalName" WithValue:@"1000" InContext:[[NSApp delegate] managedObjectContext]];
        [newDonation setFund:defaultFund];
        
        // select the new donation
        [_donationsArrayController insertObject:newDonation atArrangedObjectIndex:0];
        
        // show a popover to edit this new donation
        [_donorField setStringValue:@""];
        [[self donationPopover] setAppearance:NSPopoverAppearanceHUD];
        [[self donationPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMinYEdge];        
    }    
}

- (IBAction)finishDonationEdit:(id)sender
{
    Donation *currentDonation = [[_donationsArrayController arrangedObjects] objectAtIndex:[_donationsArrayController selectionIndex]];
    NSError *error = nil;
    if ([currentDonation validateEntity:&error])
    {
        [_donorPopover close];
        [_donationPopover close];
    } else {
        NSAlert *alert = [NSAlert alertWithMessageText:@"Problem with donation information:" defaultButton:@"OK" alternateButton:@"Cancel" otherButton:nil informativeTextWithFormat:@"%@", [error localizedDescription]];
        [alert beginSheetModalForWindow:[[self view] window] modalDelegate:self  didEndSelector:@selector(alertDidEnd:returnCode:contextInfo:) contextInfo:nil];
    }
}

- (void) alertDidEnd:(NSAlert *)alert returnCode:(int)returnCode contextInfo:(void *)contextInfo
{
    // don't need to do anything for now
}

- (IBAction)cancelDonationEdit:(id)sender
{
    Donation *currentDonation = [[_donationsArrayController arrangedObjects] objectAtIndex:[_donationsArrayController selectionIndex]];
    if ([currentDonation isInserted])
    {
        NSError *error = nil;
        [currentDonation deleteEntity:&error];
    }
    [_donorPopover close];
    [_donationPopover close];
}

- (IBAction)deleteDonation:(id)sender
{
    Donation *currentDonation = [[_donationsArrayController arrangedObjects] objectAtIndex:[_donationsArrayController selectionIndex]];
    NSError *error = nil;
    [currentDonation deleteEntity:&error];
    [_donorPopover close];
    [_donationPopover close];
}

- (void)editDonation:(id)sender
{
    if ([_donationsTableView numberOfSelectedRows] == 1) {
        Donation *currentDonation = [[_donationsArrayController arrangedObjects] objectAtIndex:[_donationsArrayController selectionIndex]];
        [_donorField setStringValue:[currentDonation donorName]];
        [[self donationPopover] setAppearance:NSPopoverAppearanceHUD];
        [[self donationPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMinYEdge];
    }
}

#pragma mark - Donors

- (IBAction)selectDonor:(id)sender
{
    Donor *selectedDonor = [[_donorArrayConroller arrangedObjects] objectAtIndex:[_donorArrayConroller selectionIndex]];
    Donation *currentDonation = [[_donationsArrayController arrangedObjects] objectAtIndex:[_donationsArrayController selectionIndex]];
    [currentDonation setDonor:selectedDonor];
    [_donorPopover close];
    [_donorField setStringValue:[selectedDonor displayName]];
    [_donationsArrayController rearrangeObjects];
}

- (void)controlTextDidBeginEditing:(NSNotification *)obj
{
    if ([obj object] == _donorField)
    {
        if (!_donorPopover.shown)
        {
            //[[self donorPopover] setAppearance:NSPopoverAppearanceHUD];
            [[self donorPopover] showRelativeToRect:[_donorField bounds] ofView:_donorField preferredEdge:NSMaxXEdge];
            [_donorField becomeFirstResponder];
        }
    }    
}

- (void)controlTextDidChange:(NSNotification *)obj
{
    if ([obj object] == _donorField)
    {
        NSPredicate *donorFilterPred = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@", [_donorField stringValue]];
        [_donorArrayConroller setFilterPredicate:donorFilterPred];
        [_donorArrayConroller rearrangeObjects];
        if (!_donorPopover.shown)
        {
            //[[self donorPopover] setAppearance:NSPopoverAppearanceHUD];
            [[self donorPopover] showRelativeToRect:[_donorField bounds] ofView:_donorField preferredEdge:NSMaxXEdge];
            [_donorField becomeFirstResponder];
        }        
    }
}

- (void)controlTextDidEndEditing:(NSNotification *)obj
{
    if ([obj object] == _donorField && ![self isTextFieldInFocus:_donorField])
    {
        if (_donorPopover.shown)
        {
            [_donorPopover close];
        }
        // now make sure a legit donor is set
    }
}

#pragma mark - UI Utilities

- (BOOL)isTextFieldInFocus:(NSTextField *)textField
{
	BOOL inFocus = NO;
	
	inFocus = ([[[textField window] firstResponder] isKindOfClass:[NSTextView class]]
			   && [[textField window] fieldEditor:NO forObject:nil]!=nil
			   && [textField isEqualTo:(id)[(NSTextView *)[[textField window] firstResponder]delegate]]);
	
	return inFocus;
}

@end
