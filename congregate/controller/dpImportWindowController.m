//
//  dpImportWindowController.m
//  congregate
//
//  Created by Mike Flores on 1/9/13.
//  Copyright (c) 2013 digiputty, llc. All rights reserved.
//

#import "dpImportWindowController.h"
#import "CHCSVParser.h"
#import "Person.h"
#import "Family.h"
#import "FamilyMember.h"
#import "EmailAddress.h"
#import "PhoneNumber.h"
#import "MailingAddress.h"
#import "SocialAddress.h"
#import "Note.h"
#import "Tag.h"
#import "ItemCollection.h"
#import "Collectible.h"
#import "DPUser.h"

@interface dpImportWindowController ()

@end

@implementation dpImportWindowController

@synthesize personFileName, familyFileName;
@synthesize familyImportResults, personImportResults;
@synthesize progressIndicator, progressMessage;

- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}

- (IBAction)selectFamilyImportFile:(id)sender
{
    [self setFamilyFileName:[[self selectFile] path]];
}

- (IBAction)selectPersonImportFile:(id)sender
{
    [self setPersonFileName:[[self selectFile] path]];
    [[NSApp delegate] setPersonFileName:personFileName];
}

- (void)import:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"StartImport"
                                                        object:self];
}

- (NSURL *)selectFile
{
    NSURL *selectedFile = nil;
    
    // Create the File Open Dialog class.
    NSOpenPanel* openDlg = [NSOpenPanel openPanel];
    
    // Enable the selection of files in the dialog.
    [openDlg setCanChooseFiles:YES];
    
    // Multiple files not allowed
    [openDlg setAllowsMultipleSelection:NO];
    
    // Can't select a directory
    [openDlg setCanChooseDirectories:NO];
    
    // Display the dialog. If the OK button was pressed,
    // process the files.
    if ( [openDlg runModal] == NSOKButton )
    {
        // Get an array containing the full filenames of all
        // files and directories selected.
        NSArray* urls = [openDlg URLs];
        
        selectedFile = [urls objectAtIndex:0];
        // Loop through all the files and process them.
        /*
         for(int i = 0; i < [urls count]; i++ )
         {
         NSString* url = [urls objectAtIndex:i];
         NSLog(@"Url: %@", url);
         }
         */
    }
    return selectedFile;
}

@end
