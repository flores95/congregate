//
//  dpTagsViewController.m
//  congregate
//
//  Created by Mike Flores on 1/7/13.
//  Copyright (c) 2013 digiputty, llc. All rights reserved.
//

#import "dpTagsViewController.h"
#import "Collectible.h"
#import "Tag.h"

@interface dpTagsViewController ()

@end

@implementation dpTagsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

-(void)viewDidLoad
{
    // get things sorted
    NSSortDescriptor *tagSort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    [[self primaryArrayController] setSortDescriptors:[NSArray arrayWithObject:tagSort]];
    NSSortDescriptor *itemSort = [NSSortDescriptor sortDescriptorWithKey:@"displayName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    [[self itemsArrayController] setSortDescriptors:[NSArray arrayWithObject:itemSort]];

}

- (IBAction)removeTag:(id)sender
{
    Tag *selectedTag = [self selectedObject];
    NSAlert *alert = [NSAlert alertWithMessageText: @"Remove Tag"
                                     defaultButton:@"YES"
                                   alternateButton:@"NO"
                                       otherButton:nil
                         informativeTextWithFormat:@"Are you sure you want to remove %@?", [selectedTag name]];
    NSInteger button = [alert runModal];
    if (button == NSAlertDefaultReturn)
    {
        for (Collectible *item in [selectedTag taggedItems])
        {
            [item removeTagsObject:selectedTag];
        }
        NSError *error = nil;
        [selectedTag deleteEntity:&error];
    }
}

@end
