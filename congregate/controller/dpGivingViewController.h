//
//  dpGivingViewController.h
//  congregate
//
//  Created by Mike Flores on 12/13/12.
//  Copyright (c) 2012 digiputty, llc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "dpMasterViewController.h"
#import "DPFillView.h"

@interface dpGivingViewController : dpMasterViewController

@property (weak) IBOutlet NSButton *batchButton;
@property (weak) IBOutlet NSButton *donationsButton;
@property (weak) IBOutlet NSButton *fundsButton;
@property (weak) IBOutlet NSButton *donorsButton;
@property (weak) IBOutlet NSButton *companiesButton;
@property (weak) IBOutlet DPFillView *navBarView;

@end
