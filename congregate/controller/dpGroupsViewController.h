//
//  dpGroupsViewController.h
//  congregate
//
//  Created by Mike Flores on 12/5/12.
//  Copyright (c) 2012 digiputty, llc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "dpMasterViewController.h"

@interface dpGroupsViewController : dpMasterViewController
@property (strong) IBOutlet NSButton *groupButton;
@property (strong) IBOutlet NSButton *tagsButton;
@property (weak) IBOutlet NSButton *attendanceButton;

@end
