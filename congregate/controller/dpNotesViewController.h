//
//  dpNotesViewController.h
//  congregate
//
//  Created by Mike Flores on 1/14/13.
//  Copyright (c) 2013 digiputty, llc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "DPViewController.h"

@interface dpNotesViewController : DPViewController

@end
