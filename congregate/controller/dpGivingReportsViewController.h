//
//  dpGivingReportsViewController.h
//  congregate
//
//  Created by Mike Flores on 12/18/12.
//  Copyright (c) 2012 digiputty, llc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface dpGivingReportsViewController : NSViewController

@property (strong) IBOutlet NSView *labelView;

- (IBAction)printLabel:(id)sender;

@end
