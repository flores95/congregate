//
//  dpGroupViewController.m
//  congregate
//
//  Created by Mike Flores on 12/5/12.
//  Copyright (c) 2012 digiputty, llc. All rights reserved.
//

#import "dpGroupViewController.h"
#import "Group.h"
#import "Person.h"

@interface dpGroupViewController ()

@end


@implementation dpGroupViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    // sort the arrays
    NSSortDescriptor *peopleSort = [NSSortDescriptor sortDescriptorWithKey:@"displayName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    [_peopleArrayController setSortDescriptors:[NSArray arrayWithObject:peopleSort]];
    NSSortDescriptor *memberSort = [NSSortDescriptor sortDescriptorWithKey:@"displayName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    [_membersArrayController setSortDescriptors:[NSArray arrayWithObject:memberSort]];
    NSSortDescriptor *groupSort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    [[self primaryArrayController] setSortDescriptors:[NSArray arrayWithObject:groupSort]];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

#pragma - IBActions

- (IBAction)newItem:(id)sender
{
    [super newItem:sender];
    [self addGroup:sender];
}

#pragma mark - Group Actions

- (IBAction)addGroup:(id)sender
{
    Group *newGroup = [Group createInContext:[[NSApp delegate] managedObjectContext]];
    [[self primaryArrayController] insertObject:newGroup atArrangedObjectIndex:0];

    [_groupPopover setAppearance:NSPopoverAppearanceHUD];
    [_groupPopover showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMinYEdge];
}

- (IBAction)finishGroupEdit:(id)sender
{
    Group *currentItem = [[[self primaryArrayController] arrangedObjects] objectAtIndex:[[self primaryArrayController] selectionIndex]];
    NSError *error = nil;
    if ([currentItem validateEntity:&error])
    {
        [_groupPopover close];
    } else {
        NSLog(@"%@", [error localizedDescription]);
    }
}

- (IBAction)cancelGroupEdit:(id)sender
{
    Group *currentItem = [[[self primaryArrayController] arrangedObjects] objectAtIndex:[[self primaryArrayController] selectionIndex]];
    if ([currentItem isInserted])
    {
        [[self primaryArrayController] remove:sender];
    }
    [_groupPopover close];
}

#pragma mark - Group Member Actions

- (IBAction)addMember:(id)sender
{
    if ([self selectedObject])
    {
        [[self memberPopover] setAppearance:NSPopoverAppearanceHUD];
        [[self memberPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMinYEdge];
    }
}

- (IBAction)finishMemberEdit:(id)sender
{
    Person *currentPerson = [[_peopleArrayController arrangedObjects] objectAtIndex:[_peopleArrayController selectionIndex]];
    Group *currentGroup = (Group *)[self selectedObject];
    [currentGroup addMembersObject:currentPerson];
    [_memberPopover close];
}

- (IBAction)cancelMemberEdit:(id)sender
{
    [_memberPopover close];
}

- (IBAction)removeMember:(id)sender
{
}

#pragma mark - Person Popover

- (void)controlTextDidBeginEditing:(NSNotification *)obj
{
}

- (void)controlTextDidChange:(NSNotification *)obj
{
    if ([obj object] == _personNameField)
    {
        NSPredicate *personFilterPred = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@", [_personNameField stringValue]];
        [_peopleArrayController setFilterPredicate:personFilterPred];
        [_peopleArrayController rearrangeObjects];
    }
}

- (void)controlTextDidEndEditing:(NSNotification *)obj
{
}

- (BOOL)isTextFieldInFocus:(NSTextField *)textField
{
	BOOL inFocus = NO;
	
	inFocus = ([[[textField window] firstResponder] isKindOfClass:[NSTextView class]]
			   && [[textField window] fieldEditor:NO forObject:nil]!=nil
			   && [textField isEqualTo:(id)[(NSTextView *)[[textField window] firstResponder]delegate]]);
	
	return inFocus;
}

@end
