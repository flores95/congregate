//
//  dpAdultsViewController.m
//  congregate
//
//  Created by Mike Flores on 12/7/12.
//  Copyright (c) 2012 digiputty, llc. All rights reserved.
//

#import "dpAdultsViewController.h"
#import "dpMainWindowController.h"
#import "CHCSVParser.h"

@interface dpAdultsViewController ()

@end

@implementation dpAdultsViewController

NSArray *_tabButtons;

@synthesize firstEditField = _firstEditField;

#pragma mark - INIT/SETUP

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //
    }
    
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // get things sorted
    NSSortDescriptor *personSort = [NSSortDescriptor sortDescriptorWithKey:@"displayName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    [[self primaryArrayController] setSortDescriptors:[NSArray arrayWithObject:personSort]];
    NSSortDescriptor *familySort = [NSSortDescriptor sortDescriptorWithKey:@"personDetail.name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    [[self familyMembersArrayController] setSortDescriptors:[NSArray arrayWithObject:familySort]];

    
    // deal with double clicks
    [_personTableView setTarget:self];
    [_personTableView setDoubleAction:@selector(editPerson:)];

    //setup detail tab buttons
    _tabButtons = [NSArray arrayWithObjects:[self tab1Button], [self tab2Button], [self tab3Button], [self tab4Button], nil];
    
    // cleanup collection view appearance
    NSArray *viewBGColors = [NSArray arrayWithObjects:[NSColor colorWithDeviceWhite:1.0 alpha:0], nil];
    [_addressCollectionView setBackgroundColors:viewBGColors];
    [_phoneCollectionView setBackgroundColors:viewBGColors];
    [_emailCollectionView setBackgroundColors:viewBGColors];
    [_socialCollectionView setBackgroundColors:viewBGColors];
    [_datesCollectionView setBackgroundColors:viewBGColors];
    [_familyCollectionView setBackgroundColors:viewBGColors];
    
    // setup the predicate(search) editor
    [self setupPredicateEditor];
}

- (void)setupPredicateEditor
{
    NSArray *stringPaths = [NSArray arrayWithObjects:
                           [NSExpression expressionForKeyPath:@"name"],
                           [NSExpression expressionForKeyPath:@"firstName"],
                           [NSExpression expressionForKeyPath:@"lastName"],
                           [NSExpression expressionForKeyPath:@"gender"],
                           [NSExpression expressionForKeyPath:@"churchStatus"],
                           nil];
    NSArray *stringOps = [NSArray arrayWithObjects:[NSNumber numberWithInteger:NSEqualToPredicateOperatorType],
                          [NSNumber numberWithInteger:NSNotEqualToPredicateOperatorType],
                          [NSNumber numberWithInteger:NSBeginsWithPredicateOperatorType],
                          [NSNumber numberWithInteger:NSEndsWithPredicateOperatorType],
                          [NSNumber numberWithInteger:NSContainsPredicateOperatorType],
                          nil];
    
    NSPredicateEditorRowTemplate *stringTemplate = [[NSPredicateEditorRowTemplate alloc] initWithLeftExpressions:stringPaths
                                                                              rightExpressionAttributeType:NSStringAttributeType
                                                                                                  modifier:NSDirectPredicateModifier
                                                                                                 operators:stringOps
                                                                                                   options:(NSCaseInsensitivePredicateOption | NSDiacriticInsensitivePredicateOption)];
    
    NSArray *boolPaths = [NSArray arrayWithObjects:
                           [NSExpression expressionForKeyPath:@"member"],
                           [NSExpression expressionForKeyPath:@"active"],
                           [NSExpression expressionForKeyPath:@"married"],
                           nil];
    NSArray *boolOps = [NSArray arrayWithObjects:
                        [NSNumber numberWithInteger:NSEqualToPredicateOperatorType],
                        [NSNumber numberWithInteger:NSNotEqualToPredicateOperatorType],
                        nil];
    
    NSPredicateEditorRowTemplate *boolTemplate = [[NSPredicateEditorRowTemplate alloc] initWithLeftExpressions:boolPaths
                                                                                   rightExpressionAttributeType:NSBooleanAttributeType
                                                                                                       modifier:NSDirectPredicateModifier
                                                                                                      operators:boolOps
                                                                                                        options:(NSCaseInsensitivePredicateOption | NSDiacriticInsensitivePredicateOption)];
    
    NSArray *datePaths = [NSArray arrayWithObjects:
                          [NSExpression expressionForKeyPath:@"birthday"],
                          nil];
    NSArray *dateOps = [NSArray arrayWithObjects:
                        [NSNumber numberWithInteger:NSEqualToPredicateOperatorType],
                        [NSNumber numberWithInteger:NSNotEqualToPredicateOperatorType],
                        [NSNumber numberWithInteger:NSGreaterThanPredicateOperatorType],
                        [NSNumber numberWithInteger:NSGreaterThanOrEqualToPredicateOperatorType],
                        [NSNumber numberWithInteger:NSLessThanPredicateOperatorType],
                        [NSNumber numberWithInteger:NSLessThanOrEqualToPredicateOperatorType],
                        nil];
    
    NSPredicateEditorRowTemplate *dateTemplate = [[NSPredicateEditorRowTemplate alloc] initWithLeftExpressions:datePaths
                                                                                  rightExpressionAttributeType:NSDateAttributeType
                                                                                                      modifier:NSDirectPredicateModifier
                                                                                                     operators:dateOps
                                                                                                       options:(NSCaseInsensitivePredicateOption | NSDiacriticInsensitivePredicateOption)];
    
    NSArray *childDatePaths = [NSArray arrayWithObjects:
                          [NSExpression expressionForKeyPath:@"significantDates.eventDate"],
                          nil];
    NSPredicateEditorRowTemplate *childDateTemplate = [[NSPredicateEditorRowTemplate alloc] initWithLeftExpressions:childDatePaths
                                                                                  rightExpressionAttributeType:NSDateAttributeType
                                                                                                      modifier:NSAnyPredicateModifier
                                                                                                     operators:dateOps
                                                                                                       options:(NSCaseInsensitivePredicateOption | NSDiacriticInsensitivePredicateOption)];
    
    NSArray *childStringPaths = [NSArray arrayWithObjects:
                                 [NSExpression expressionForKeyPath:@"tags.name"],
                                 [NSExpression expressionForKeyPath:@"notes.noteText"],
                                 [NSExpression expressionForKeyPath:@"family.name"],
                                 [NSExpression expressionForKeyPath:@"memberOf.name"],
                                 [NSExpression expressionForKeyPath:@"memberOf.type"],
                                 [NSExpression expressionForKeyPath:@"emailAddresses.address"],
                                 [NSExpression expressionForKeyPath:@"mailingAddresses.addressLine1"],
                                 [NSExpression expressionForKeyPath:@"mailingAddresses.addressLine2"],
                                 [NSExpression expressionForKeyPath:@"mailingAddresses.city"],
                                 [NSExpression expressionForKeyPath:@"mailingAddresses.state"],
                                 [NSExpression expressionForKeyPath:@"mailingAddresses.postalCode"],
                                 [NSExpression expressionForKeyPath:@"phoneNumbers.number"],
                                 [NSExpression expressionForKeyPath:@"significantDates.eventTitle"],
                                 [NSExpression expressionForKeyPath:@"significantDates.eventDescription"],
                                 [NSExpression expressionForKeyPath:@"userAccount.username"],
                                 nil];
  
    NSPredicateEditorRowTemplate *childStringTemplate = [[NSPredicateEditorRowTemplate alloc] initWithLeftExpressions:childStringPaths
                                                                                   rightExpressionAttributeType:NSStringAttributeType
                                                                                                       modifier:NSAnyPredicateModifier
                                                                                                      operators:stringOps
                                                                                                        options:(NSCaseInsensitivePredicateOption | NSDiacriticInsensitivePredicateOption)];
    
    
    NSArray *compoundTypes = [NSArray arrayWithObjects:[NSNumber numberWithInteger:NSNotPredicateType],
                              [NSNumber numberWithInteger:NSAndPredicateType],
                              [NSNumber numberWithInteger:NSOrPredicateType],
                              nil];
    NSPredicateEditorRowTemplate *compound = [[NSPredicateEditorRowTemplate alloc] initWithCompoundTypes:compoundTypes];
    
    NSArray *rowTemplates = [NSArray arrayWithObjects:stringTemplate, boolTemplate, dateTemplate, childDateTemplate, childStringTemplate, compound, nil];
    
    [_searchPredicateEditor setRowTemplates:rowTemplates];
}

#pragma mark - DETAIL TAB ACTIONS

- (IBAction)changeDetailTab:(id)sender
{
    [[self detailTabView] selectTabViewItemAtIndex:[(NSButton *)sender tag]];
    for (NSButton *btn in _tabButtons)
    {
        if (btn != sender)
        {
            [btn setState:0];
        }
    }

}

#pragma mark - Phone Number Actions

- (IBAction)addPhoneNumber:(id)sender
{
    PhoneNumber *newItem = [PhoneNumber createInContext:[[NSApp delegate] managedObjectContext]];
    [[self phoneArrayController] insertObject:newItem atArrangedObjectIndex:0];
    
    [_phonePopover setAppearance:NSPopoverAppearanceHUD];
    [_phonePopover showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMinYEdge];
}

- (IBAction)editPhone:(id)sender
{
    [[self phonePopover] setAppearance:NSPopoverAppearanceHUD];
    [[self phonePopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxXEdge];    
}

- (IBAction)deletePhone:(id)sender
{
    [_phoneArrayController remove:sender];
    [_phonePopover close];
}

- (IBAction)finishPhoneEdit:(id)sender
{
    PhoneNumber *currentItem = [[_phoneArrayController arrangedObjects] objectAtIndex:[_phoneArrayController selectionIndex]];
    NSError *error = nil;
    if ([currentItem validateEntity:&error])
    {
        [_phonePopover close];
    } else {
        [self showAlert:@"PROBLEM SAVING PHONE" WithError:error];
    }
}

- (IBAction)cancelPhoneEdit:(id)sender
{
    PhoneNumber *currentItem = [[_phoneArrayController arrangedObjects] objectAtIndex:[_phoneArrayController selectionIndex]];
    if ([currentItem isInserted])
    {
        [_phoneArrayController remove:sender];
    }
    [_phonePopover close];
}

- (void)selectPhone:(PhoneNumber *)phone
{
    [[self phoneArrayController] setSelectedObjects:[NSArray arrayWithObject:phone]];
}

- (IBAction)addAddress:(id)sender
{
    MailingAddress *newItem = [MailingAddress createInContext:[[NSApp delegate] managedObjectContext]];
    [[self addressArrayController] insertObject:newItem atArrangedObjectIndex:0];
    
    [_addressPopover setAppearance:NSPopoverAppearanceHUD];
    [_addressPopover showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMinYEdge];
}

#pragma mark - Address Actions

- (IBAction)editAddress:(id)sender
{
    [[self addressPopover] setAppearance:NSPopoverAppearanceHUD];
    [[self addressPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxXEdge];
}

- (IBAction)deleteAddress:(id)sender
{
    [_addressArrayController remove:sender];
    [_addressPopover close];
}

- (IBAction)finishAddressEdit:(id)sender
{
    MailingAddress *currentItem = [[_addressArrayController arrangedObjects] objectAtIndex:[_addressArrayController selectionIndex]];
    NSError *error = nil;
    if ([currentItem validateEntity:&error])
    {
        [_addressPopover close];
    } else {
        [self showAlert:@"PROBLEM SAVING ADDRESS" WithError:error];
    }
}

- (IBAction)cancelAddressEdit:(id)sender
{
    MailingAddress *currentItem = [[_addressArrayController arrangedObjects] objectAtIndex:[_addressArrayController selectionIndex]];
    if ([currentItem isInserted])
    {
        [_addressArrayController remove:sender];
    }
    [_addressPopover close];
}

- (void)selectAddress:(MailingAddress *)address
{
    [[self addressArrayController] setSelectedObjects:[NSArray arrayWithObject:address]];
}

#pragma mark - Email Actions

- (IBAction)addEmail:(id)sender
{
    EmailAddress *newItem = [EmailAddress createInContext:[[NSApp delegate] managedObjectContext]];
    [[self emailArrayController] insertObject:newItem atArrangedObjectIndex:0];
    
    [_emailPopover setAppearance:NSPopoverAppearanceHUD];
    [_emailPopover showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMinYEdge];
}

- (IBAction)editEmail:(id)sender
{
    [[self emailPopover] setAppearance:NSPopoverAppearanceHUD];
    [[self emailPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxXEdge];
}

- (IBAction)deleteEmail:(id)sender
{
    [_emailArrayController remove:sender];
    [_emailPopover close];
}

- (IBAction)finishEmailEdit:(id)sender
{
    EmailAddress *currentItem = [[_emailArrayController arrangedObjects] objectAtIndex:[_emailArrayController selectionIndex]];
    NSError *error = nil;
    if ([currentItem validateEntity:&error])
    {
        [_emailPopover close];
    } else {
        [self showAlert:@"PROBLEM SAVING EMAIL" WithError:error];
    }
}

- (IBAction)cancelEmailEdit:(id)sender
{
    EmailAddress *currentItem = [[_emailArrayController arrangedObjects] objectAtIndex:[_emailArrayController selectionIndex]];
    if ([currentItem isInserted])
    {
        [_emailArrayController remove:sender];
    }
    [_emailPopover close];
}

- (void)selectEmail:(EmailAddress *)email
{
    [[self emailArrayController] setSelectedObjects:[NSArray arrayWithObject:email]];
}

#pragma mark - Social Address Actions

- (IBAction)addSocialAddress:(id)sender
{
    SocialAddress *newItem = [SocialAddress createInContext:[[NSApp delegate] managedObjectContext]];
    [[self socialArrayController] insertObject:newItem atArrangedObjectIndex:0];
    
    [_socialPopover setAppearance:NSPopoverAppearanceHUD];
    [_socialPopover showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMinYEdge];
}

- (IBAction)editSocialAddress:(id)sender
{
    [[self socialPopover] setAppearance:NSPopoverAppearanceHUD];
    [[self socialPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxXEdge];
}

- (IBAction)deleteSocialAddress:(id)sender
{
    [_socialArrayController remove:sender];
    [_socialPopover close];
}

- (void)finishSocialEdit:(id)sender
{
    SocialAddress *currentItem = [[_socialArrayController arrangedObjects] objectAtIndex:[_socialArrayController selectionIndex]];
    NSError *error = nil;
    if ([currentItem validateEntity:&error])
    {
        [_socialPopover close];
    } else {
        [self showAlert:@"PROBLEM SAVING ADDRESS" WithError:error];
    }

}

- (IBAction)cancelSocialEdit:(id)sender
{
    SocialAddress *currentItem = [[_socialArrayController arrangedObjects] objectAtIndex:[_socialArrayController selectionIndex]];
    if ([currentItem isInserted])
    {
        [_socialArrayController remove:sender];
    }
    [_socialPopover close];
}

- (void)selectSocialAddress:(SocialAddress *)address
{
    [[self socialArrayController] setSelectedObjects:[NSArray arrayWithObject:address]];
}

#pragma mark - Date Actions

- (IBAction)addDate:(id)sender
{
    SignificantDate *newItem = [SignificantDate createInContext:[[NSApp delegate] managedObjectContext]];
    [[self datesArrayController] addObject:newItem];
    [[self datesPopover] setAppearance:NSPopoverAppearanceHUD];
    [[self datesPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxYEdge];
}

- (IBAction)editDate:(id)sender
{
    [[self datesPopover] setAppearance:NSPopoverAppearanceHUD];
    [[self datesPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxXEdge];
}

- (IBAction)deleteDate:(id)sender
{
    [[self datesArrayController] remove:sender];
    [_datesPopover close];
}

- (IBAction)finishDateEdit:(id)sender
{
    SignificantDate *currentItem = [[[self datesArrayController] arrangedObjects] objectAtIndex:[[self datesArrayController] selectionIndex]];
    NSError *error = nil;
    if ([currentItem validateEntity:&error])
    {
        [[self datesPopover] close];
    } else {
        [self showAlert:@"PROBLEM SAVING DATE" WithError:error];
    }
}

- (IBAction)cancelDateEdit:(id)sender
{
    Person *currentItem = [[[self datesArrayController] arrangedObjects] objectAtIndex:[[self datesArrayController] selectionIndex]];
    if ([currentItem isInserted])
    {
        [[self datesArrayController] remove:sender];
    }
    [[self datesPopover] close];
}

- (void)selectDate:(SignificantDate *)eventDate
{
    [[self datesArrayController] setSelectedObjects:[NSArray arrayWithObject:eventDate]];
}

#pragma mark - Person Actions

- (IBAction)addPerson:(id)sender
{
    Person *newAdult = [Person createInContext:[[NSApp delegate] managedObjectContext]];
    [newAdult setAdultValue:YES];

    // make a significant date with the change in status
    SignificantDate *regDate = [SignificantDate createInContext:[[NSApp delegate] managedObjectContext]];
    [regDate setEventDate:[NSDate date]];
    [regDate setEventTitle:@"Registered"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *user = [defaults valueForKey:kDPAppUserKey];
    [regDate setEventDescription:[NSString stringWithFormat:@"Registered by %@", user]];
    [newAdult addSignificantDatesObject:regDate];

    [[self primaryArrayController] addObject:newAdult];

    [[self personPopover] setAppearance:NSPopoverAppearanceHUD];
    [[self personPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxYEdge];
}

- (IBAction)deletePerson:(id)sender
{
    //NSAlert
    for (Person *curPerson in [[self primaryArrayController] selectedObjects])
    {
        NSError *error = nil;
        [curPerson deleteEntity:&error];
    }
    [_personPopover close];
}

- (void)editPerson:(id)sender
{
    [[self personPopover] setAppearance:NSPopoverAppearanceHUD];
    [[self personPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxXEdge];
}

- (IBAction)finishPersonEdit:(id)sender
{
    Person *currentItem = [[[self primaryArrayController] arrangedObjects] objectAtIndex:[[self primaryArrayController] selectionIndex]];
    NSError *error = nil;
    if ([currentItem validateEntity:&error])
    {
        [[self personPopover] close];
    } else {
        [self showAlert:@"PROBLEM SAVING PERSON" WithError:error];
    }
}

- (IBAction)cancelPersonEdit:(id)sender
{
    Person *currentItem = [[[self primaryArrayController] arrangedObjects] objectAtIndex:[[self primaryArrayController] selectionIndex]];
    if ([currentItem isInserted])
    {
        [[self primaryArrayController] remove:sender];
    }
    [[self personPopover] close];
}

-(IBAction)newItem:(id)sender
{
    [self addPerson:sender];
}

#pragma mark - Search Routines

- (IBAction)showSearchPopover:(id)sender
{
    if ([[self searchPopover] isShown])
    {
        [[self searchPopover] close];
    } else {
        [[self searchPopover] setAppearance:NSPopoverAppearanceHUD];
        [[self searchPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxXEdge];
    }
}

- (IBAction)performSearch:(id)sender
{
    NSPredicate *pred = [[self searchPredicateEditor] predicate];
    [[self primaryArrayController] setFilterPredicate:pred];
    [[self searchPopover] close];
}

#pragma mark - Export Routines

-(IBAction)showExport:(id)sender
{
    if ([[self exportPopover] isShown])
    {
        [[self exportPopover] close];
    } else {
        [[self exportPopover] setAppearance:NSPopoverAppearanceHUD];
        [[self exportPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxXEdge];
    }
}

- (IBAction)exportPrimaryEmail:(id)sender
{
    NSURL *fileURL = [self _selectExportFile];
    
    if (fileURL)
    {
        NSOutputStream *output = [NSOutputStream outputStreamWithURL:fileURL append:NO];
        CHCSVWriter *writer = [[CHCSVWriter alloc] initWithOutputStream:output encoding:NSUTF8StringEncoding delimiter:','];

        NSMutableArray *emails = [[NSMutableArray alloc] init];
        //loop through current arranged people and create csv file
        for (Person *curPerson in [[self primaryArrayController] arrangedObjects])
        {
            if ([curPerson primaryEmail])
            {
                // make sure there's no duplicate emails
                if (![emails containsObject:[curPerson primaryEmail]])
                {
                    [writer writeLineOfFields:@[[curPerson nickName], [curPerson lastName], [curPerson primaryEmail]]];
                    [emails addObject:[curPerson primaryEmail]];
                }
            }
        }
        
        [writer closeStream];
    }
    
    
    //close the export popover
    [_exportPopover close];
}

- (IBAction)exportPrimaryContactInfo:(id)sender
{
    NSURL *fileURL = [self _selectExportFile];
    
    if (fileURL)
    {
        NSOutputStream *output = [NSOutputStream outputStreamWithURL:fileURL append:NO];
        CHCSVWriter *writer = [[CHCSVWriter alloc] initWithOutputStream:output encoding:NSUTF8StringEncoding delimiter:','];
        
        //loop through current arranged people and create csv file
        for (Person *curPerson in [[self primaryArrayController] arrangedObjects])
        {
            NSString *fname = [curPerson firstName];
            NSString *lname = [curPerson lastName];
            NSString *nname = [curPerson nickName];
            NSString *title = [curPerson title];

            NSString *addr = @"";
            NSString *line1 = @"";
            NSString *line2 = @"";
            NSString *city = @"";
            NSString *state = @"";
            NSString *postalCode = @"";
            if ([curPerson primaryAddress])
            {
                addr = [curPerson.primaryAddress fullAddress];
                line1 = [curPerson.primaryAddress addressLine1];
                line2 = [curPerson.primaryAddress addressLine2];
                city = [curPerson.primaryAddress city];
                state = [curPerson.primaryAddress state];
                postalCode = [curPerson.primaryAddress postalCode];
            }
            NSString *phone = @"";
            if ([curPerson primaryPhone])
            {
                phone = [curPerson.primaryPhone number];
            }
            NSString *email = @"";
            if ([curPerson primaryEmail])
            {
                email = [curPerson.primaryEmail address];
            }
            
            if (!title) { title = @""; }
            if (!addr) { addr = @""; }
            if (!line1) { line1 = @""; }
            if (!line2) { line2 = @""; }
            if (!city) { city = @""; }
            if (!state) { state = @""; }
            if (!postalCode) { postalCode = @""; }
            if (!phone) { phone = @""; }
            if (!email) { email = @""; }
            
            
            [writer writeLineOfFields:@[fname, lname, nname, title, phone, email, addr, line1, line2, city, state, postalCode]];
        }
        
        [writer closeStream];
    }
    
    
    //close the export popover
    [_exportPopover close];
}

#pragma mark - Alerts

- (void)showAlert:(NSString *)title WithError:(NSError *)error
{
    NSAlert *alert = [NSAlert alertWithMessageText:title defaultButton:@"OK" alternateButton:@"Cancel" otherButton:nil informativeTextWithFormat:@"%@", [error localizedDescription]];
    [alert beginSheetModalForWindow:[[self view] window] modalDelegate:self  didEndSelector:@selector(alertDidEnd:returnCode:contextInfo:) contextInfo:nil];
}

- (void) alertDidEnd:(NSAlert *)alert returnCode:(int)returnCode contextInfo:(void *)contextInfo
{
    // don't need to do anything for now
}

#pragma mark - Selected Object Changes

- (void)setSelectedObject:(id)selectedObject
{
    [super setSelectedObject:selectedObject];
    
    // update necessary display items
    Person *selectedPerson = (Person *)selectedObject;
    if ([selectedPerson gender])
    {
        [_genderButton setHidden:NO];
        if ([[selectedPerson gender] isEqualToString:@"Male"])
        {
            [_genderButton setState:0];
        } else {
            [_genderButton setState:1];
        }
    }
}

#pragma mark - Select File
- (NSURL *) _selectExportFile
{
    // Create the File Open Dialog class.
    NSSavePanel *saveDlg = [NSSavePanel savePanel];
    
    // Enable the selection of files in the dialog.
    [saveDlg setAllowedFileTypes:@[@"csv", @"txt"]];
    
    // Display the dialog. If the OK button was pressed,
    // process the files.
    if ( [saveDlg runModal] == NSOKButton )
    {
        return [saveDlg URL];
    }
    return nil;
}

@end
