//
//  dpAdultsViewController.m
//  congregate
//
//  Created by Mike Flores on 12/7/12.
//  Copyright (c) 2012 digiputty, llc. All rights reserved.
//

#import "dpChildrenViewController.h"
#import "dpAppDelegate.h"

@interface dpChildrenViewController ()

@end

@implementation dpChildrenViewController

NSArray *_tabButtons;

#pragma mark - INIT PROCESSES

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //setup some variables for printing badges
    [self setToday:[NSDate date]];

    // get things sorted
    NSSortDescriptor *personSort = [NSSortDescriptor sortDescriptorWithKey:@"displayName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    [[self primaryArrayController] setSortDescriptors:[NSArray arrayWithObject:personSort]];
    NSSortDescriptor *familySort = [NSSortDescriptor sortDescriptorWithKey:@"personDetail.name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    [[self familyMembersArrayController] setSortDescriptors:[NSArray arrayWithObject:familySort]];

    
    // deal with double clicks
    [_personTableView setTarget:self];
    [_personTableView setDoubleAction:@selector(editPerson:)];

    //setup detail tab buttons
    _tabButtons = [NSArray arrayWithObjects:[self tab1Button], [self tab2Button], [self tab3Button], [self tab4Button], nil];
    
    // cleanup collection view appearance
    NSArray *viewBGColors = [NSArray arrayWithObjects:[NSColor colorWithDeviceWhite:1.0 alpha:0], nil];
    [_addressCollectionView setBackgroundColors:viewBGColors];
    [_phoneCollectionView setBackgroundColors:viewBGColors];
    [_emailCollectionView setBackgroundColors:viewBGColors];
    [_socialCollectionView setBackgroundColors:viewBGColors];
    [_datesCollectionView setBackgroundColors:viewBGColors];
    [_familyCollectionView setBackgroundColors:viewBGColors];
}

#pragma mark - NAVIGATION

- (IBAction)changeDetailTab:(id)sender
{
    [[self detailTabView] selectTabViewItemAtIndex:[(NSButton *)sender tag]];
    for (NSButton *btn in _tabButtons)
    {
        if (btn != sender)
        {
            [btn setState:0];
        }
    }

}

#pragma mark - PRINT

- (IBAction)printNameBadge:(id)sender
{
    dpAppDelegate *appDelegate = (dpAppDelegate *)[[NSApplication sharedApplication] delegate];
    NSPrintInfo *printInfo = [[NSPrintInfo alloc] init];
    [printInfo setPrinter:[NSPrinter printerWithName:[appDelegate labelPrinter]]];
    NSSize paperSize;
    paperSize = NSMakeSize (1200, 693);
    [printInfo setPaperSize:paperSize];
    [printInfo setOrientation:NSPortraitOrientation];
    [printInfo setHorizontallyCentered:YES];
    [printInfo setVerticallyCentered:YES];
    [printInfo setHorizontalPagination:NSFitPagination];
    [printInfo setVerticalPagination:NSFitPagination];
    NSPrintOperation *op = [NSPrintOperation printOperationWithView:_childLabelView printInfo:printInfo];
    [op setShowsPrintPanel:NO];
    [op runOperation];
    
    NSPrintOperation *op2 = [NSPrintOperation printOperationWithView:_claimTagView printInfo:printInfo];
    [op2 setShowsPrintPanel:NO];
    [op2 runOperation];
    
    // increase the alert number
    [appDelegate setAlertNumber:[appDelegate alertNumber] + 1];
}

#pragma mark - PERSON EDIT

- (IBAction)addPerson:(id)sender
{
    Person *newChild = [Person createInContext:[[NSApp delegate] managedObjectContext]];
    [newChild setAdultValue:NO];
    
    // make a significant date with the change in status
    SignificantDate *regDate = [SignificantDate createInContext:[[NSApp delegate] managedObjectContext]];
    [regDate setEventDate:[NSDate date]];
    [regDate setEventTitle:@"Registered"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *user = [defaults valueForKey:kDPAppUserKey];
    [regDate setEventDescription:[NSString stringWithFormat:@"Registered by %@", user]];
    [newChild addSignificantDatesObject:regDate];
    
    [[self primaryArrayController] addObject:newChild];
    [[self personPopover] setAppearance:NSPopoverAppearanceHUD];
    [[self personPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxYEdge];
}

- (IBAction)deletePerson:(id)sender
{
    [[self primaryArrayController] remove:sender];
    [_personPopover close];
}

- (void)editPerson:(id)sender
{
    [[self personPopover] setAppearance:NSPopoverAppearanceHUD];
    [[self personPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxXEdge];
}

- (IBAction)finishPersonEdit:(id)sender
{
    Person *currentItem = [[[self primaryArrayController] arrangedObjects] objectAtIndex:[[self primaryArrayController] selectionIndex]];
    NSError *error = nil;
    if ([currentItem validateEntity:&error])
    {
        [[self personPopover] close];
    } else {
        [self showAlert:@"PROBLEM SAVING PERSON" WithError:error];
    }
}

- (IBAction)cancelPersonEdit:(id)sender
{
    Person *currentItem = [[[self primaryArrayController] arrangedObjects] objectAtIndex:[[self primaryArrayController] selectionIndex]];
    if ([currentItem isInserted])
    {
        [[self primaryArrayController] remove:sender];
    }
    [[self personPopover] close];
}

- (IBAction)changeGradeAdjust:(id)sender
{
    [[self gradeAdjustField] setIntValue:[sender intValue]];
}

#pragma mark - PHONE EDIT

- (IBAction)addPhoneNumber:(id)sender
{
    PhoneNumber *newItem = [PhoneNumber createInContext:[[NSApp delegate] managedObjectContext]];
    [[self phoneArrayController] insertObject:newItem atArrangedObjectIndex:0];
    
    [_phonePopover setAppearance:NSPopoverAppearanceHUD];
    [_phonePopover showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMinYEdge];
}

- (IBAction)editPhone:(id)sender
{
    [[self phonePopover] setAppearance:NSPopoverAppearanceHUD];
    [[self phonePopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxXEdge];    
}

- (IBAction)deletePhone:(id)sender
{
    [_phoneArrayController remove:sender];
    [_phonePopover close];
}

- (IBAction)finishPhoneEdit:(id)sender
{
    PhoneNumber *currentItem = [[_phoneArrayController arrangedObjects] objectAtIndex:[_phoneArrayController selectionIndex]];
    NSError *error = nil;
    if ([currentItem validateEntity:&error])
    {
        [_phonePopover close];
    } else {
        [self showAlert:@"PROBLEM SAVING PHONE" WithError:error];
    }
}

- (IBAction)cancelPhoneEdit:(id)sender
{
    PhoneNumber *currentItem = [[_phoneArrayController arrangedObjects] objectAtIndex:[_phoneArrayController selectionIndex]];
    if ([currentItem isInserted])
    {
        [_phoneArrayController remove:sender];
    }
    [_phonePopover close];
}

- (void)selectPhone:(PhoneNumber *)phone
{
    [[self phoneArrayController] setSelectedObjects:[NSArray arrayWithObject:phone]];
}

#pragma mark - ADDRESS EDIT

- (IBAction)addAddress:(id)sender
{
    MailingAddress *newItem = [MailingAddress createInContext:[[NSApp delegate] managedObjectContext]];
    [[self addressArrayController] insertObject:newItem atArrangedObjectIndex:0];
    
    [_addressPopover setAppearance:NSPopoverAppearanceHUD];
    [_addressPopover showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMinYEdge];
}

- (IBAction)editAddress:(id)sender
{
    [[self addressPopover] setAppearance:NSPopoverAppearanceHUD];
    [[self addressPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxXEdge];
}

- (IBAction)deleteAddress:(id)sender
{
    [_addressArrayController remove:sender];
    [_addressPopover close];
}

- (IBAction)finishAddressEdit:(id)sender
{
    MailingAddress *currentItem = [[_addressArrayController arrangedObjects] objectAtIndex:[_addressArrayController selectionIndex]];
    NSError *error = nil;
    if ([currentItem validateEntity:&error])
    {
        [_addressPopover close];
    } else {
        [self showAlert:@"PROBLEM SAVING ADDRESS" WithError:error];
    }
}

- (IBAction)cancelAddressEdit:(id)sender
{
    MailingAddress *currentItem = [[_addressArrayController arrangedObjects] objectAtIndex:[_addressArrayController selectionIndex]];
    if ([currentItem isInserted])
    {
        [_addressArrayController remove:sender];
    }
    [_addressPopover close];
}

- (void)selectAddress:(MailingAddress *)address
{
    [[self addressArrayController] setSelectedObjects:[NSArray arrayWithObject:address]];
}

#pragma mark - EMAIL EDIT

- (IBAction)addEmail:(id)sender
{
    EmailAddress *newItem = [EmailAddress createInContext:[[NSApp delegate] managedObjectContext]];
    [[self emailArrayController] insertObject:newItem atArrangedObjectIndex:0];
    
    [_emailPopover setAppearance:NSPopoverAppearanceHUD];
    [_emailPopover showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMinYEdge];
}

- (IBAction)editEmail:(id)sender
{
    [[self emailPopover] setAppearance:NSPopoverAppearanceHUD];
    [[self emailPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxXEdge];
}

- (IBAction)deleteEmail:(id)sender
{
    [_emailArrayController remove:sender];
    [_emailPopover close];
}

- (IBAction)finishEmailEdit:(id)sender
{
    EmailAddress *currentItem = [[_emailArrayController arrangedObjects] objectAtIndex:[_emailArrayController selectionIndex]];
    NSError *error = nil;
    if ([currentItem validateEntity:&error])
    {
        [_emailPopover close];
    } else {
        [self showAlert:@"PROBLEM SAVING EMAIL" WithError:error];
    }
}

- (IBAction)cancelEmailEdit:(id)sender
{
    EmailAddress *currentItem = [[_emailArrayController arrangedObjects] objectAtIndex:[_emailArrayController selectionIndex]];
    if ([currentItem isInserted])
    {
        [_emailArrayController remove:sender];
    }
    [_emailPopover close];
}

- (void)selectEmail:(EmailAddress *)email
{
    [[self emailArrayController] setSelectedObjects:[NSArray arrayWithObject:email]];
}

- (IBAction)addSocialAddress:(id)sender
{
    SocialAddress *newItem = [SocialAddress createInContext:[[NSApp delegate] managedObjectContext]];
    [[self socialArrayController] insertObject:newItem atArrangedObjectIndex:0];
    
    [_socialPopover setAppearance:NSPopoverAppearanceHUD];
    [_socialPopover showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMinYEdge];
}

#pragma mark - SOCIAL EDIT

- (IBAction)editSocialAddress:(id)sender
{
    [[self socialPopover] setAppearance:NSPopoverAppearanceHUD];
    [[self socialPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxXEdge];
}

- (IBAction)deleteSocialAddress:(id)sender
{
    [_socialArrayController remove:sender];
    [_socialPopover close];
}

- (void)finishSocialEdit:(id)sender
{
    SocialAddress *currentItem = [[_socialArrayController arrangedObjects] objectAtIndex:[_socialArrayController selectionIndex]];
    NSError *error = nil;
    if ([currentItem validateEntity:&error])
    {
        [_socialPopover close];
    } else {
        [self showAlert:@"PROBLEM SAVING ADDRESS" WithError:error];
    }

}

- (IBAction)cancelSocialEdit:(id)sender
{
    SocialAddress *currentItem = [[_socialArrayController arrangedObjects] objectAtIndex:[_socialArrayController selectionIndex]];
    if ([currentItem isInserted])
    {
        [_socialArrayController remove:sender];
    }
    [_socialPopover close];
}

- (void)selectSocialAddress:(SocialAddress *)address
{
    [[self socialArrayController] setSelectedObjects:[NSArray arrayWithObject:address]];
}

#pragma mark - SIGNIFICANT DATE EDIT

- (IBAction)addDate:(id)sender
{
    SignificantDate *newItem = [SignificantDate createInContext:[[NSApp delegate] managedObjectContext]];
    [[self datesArrayController] addObject:newItem];
    [[self datesPopover] setAppearance:NSPopoverAppearanceHUD];
    [[self datesPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxYEdge];
}

- (IBAction)editDate:(id)sender
{
    [[self datesPopover] setAppearance:NSPopoverAppearanceHUD];
    [[self datesPopover] showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxXEdge];
}

- (IBAction)deleteDate:(id)sender
{
    [[self datesArrayController] remove:sender];
    [_datesPopover close];
}

- (IBAction)finishDateEdit:(id)sender
{
    SignificantDate *currentItem = [[[self datesArrayController] arrangedObjects] objectAtIndex:[[self datesArrayController] selectionIndex]];
    NSError *error = nil;
    if ([currentItem validateEntity:&error])
    {
        [[self datesPopover] close];
    } else {
        [self showAlert:@"PROBLEM SAVING DATE" WithError:error];
    }
}

- (IBAction)cancelDateEdit:(id)sender
{
    Person *currentItem = [[[self datesArrayController] arrangedObjects] objectAtIndex:[[self datesArrayController] selectionIndex]];
    if ([currentItem isInserted])
    {
        [[self datesArrayController] remove:sender];
    }
    [[self datesPopover] close];
}

- (void)selectDate:(SignificantDate *)eventDate
{
    [[self datesArrayController] setSelectedObjects:[NSArray arrayWithObject:eventDate]];
}

#pragma mark - MAIN MENU RESPONDERS

-(IBAction)newItem:(id)sender
{
    [self addPerson:sender];
}

#pragma mark - ALERT FUNCTIONS

- (void)showAlert:(NSString *)title WithError:(NSError *)error
{
    NSAlert *alert = [NSAlert alertWithMessageText:title defaultButton:@"OK" alternateButton:@"Cancel" otherButton:nil informativeTextWithFormat:@"%@", [error localizedDescription]];
    [alert beginSheetModalForWindow:[[self view] window] modalDelegate:self  didEndSelector:@selector(alertDidEnd:returnCode:contextInfo:) contextInfo:nil];
}

- (void) alertDidEnd:(NSAlert *)alert returnCode:(int)returnCode contextInfo:(void *)contextInfo
{
    // don't need to do anything for now
}

#pragma mark - Selected Object Changes

- (void)setSelectedObject:(id)selectedObject
{
    [super setSelectedObject:selectedObject];
    
    // update necessary display items
    Person *selectedPerson = (Person *)selectedObject;
    if ([selectedPerson gender])
    {
        [_genderButton setHidden:NO];
        if ([[selectedPerson gender] isEqualToString:@"Male"])
        {
            [_genderButton setState:0];
        } else {
            [_genderButton setState:1];
        }
    }
}

@end
