//
//  dpGivingReportsViewController.m
//  congregate
//
//  Created by Mike Flores on 12/18/12.
//  Copyright (c) 2012 digiputty, llc. All rights reserved.
//

#import "dpGivingReportsViewController.h"

@interface dpGivingReportsViewController ()

@end


NSPrintInfo *printInfo = nil;

@implementation dpGivingReportsViewController

@synthesize labelView = _labelView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    
    }
    
    return self;
}

- (void)awakeFromNib
{
    printInfo = [[NSPrintInfo alloc] init];
}

- (IBAction)printLabel:(id)sender {
    [printInfo setPrinter:[NSPrinter printerWithName:@"DYMO LabelWriter 400"]];
    NSSize paperSize;
    paperSize = NSMakeSize (693, 1200);
    [printInfo setPaperSize:paperSize];
    [printInfo setVerticalPagination:NSFitPagination];
    [printInfo setOrientation:NSPortraitOrientation];
    [[NSPrintOperation printOperationWithView:_labelView] runOperation];
}

@end
