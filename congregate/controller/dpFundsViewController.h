//
//  dpFundsViewController.h
//  congregate
//
//  Created by Mike Flores on 12/18/12.
//  Copyright (c) 2012 digiputty, llc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "DPViewController.h"

@interface dpFundsViewController : DPViewController

@property (weak) IBOutlet NSTextField *firstEditField;
@property (weak) IBOutlet NSTableView *fundTableView;

- (IBAction)addFund:(id)sender;
- (IBAction)newItem:(id)sender;
@end
