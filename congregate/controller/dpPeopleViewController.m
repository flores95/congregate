//
//  dpPeopleViewController.m
//  congregate
//
//  Created by Mike Flores on 12/12/12.
//  Copyright (c) 2012 digiputty, llc. All rights reserved.
//

#import "dpPeopleViewController.h"
#import "Person.h"

@interface dpPeopleViewController ()

@end

@implementation dpPeopleViewController

@synthesize countOfActive, countOfAdults, countOfAttending, countOfChildren, countOfGiving, countOfParticipating, countOfPeople, countOfSearching, countOfVisiting;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //
    }
    
    return self;
}

- (void)awakeFromNib
{
    [self getCounts];
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
}


- (void)getCounts
{
    [self setCountOfPeople:[Person countAllInContext:[[NSApp delegate] managedObjectContext]]];
    [self setCountOfAdults:[Person countAdultsInContext:[[NSApp delegate] managedObjectContext]]];
    [self setCountOfChildren:[Person countChildrenInContext:[[NSApp delegate] managedObjectContext]]];
    [self setCountOfActive:[Person countActive:YES InContext:[[NSApp delegate] managedObjectContext]]];
    [self setCountOfAttending:[Person countChurchStatus:@"Attending" InContext:[[NSApp delegate] managedObjectContext]]];
    [self setCountOfVisiting:[Person countChurchStatus:@"Visiting" InContext:[[NSApp delegate] managedObjectContext]]];
    [self setCountOfSearching:[Person countChurchStatus:@"Searching" InContext:[[NSApp delegate] managedObjectContext]]];
    [self setCountOfParticipating:[Person countChurchStatus:@"Participating" InContext:[[NSApp delegate] managedObjectContext]]];
    [self setCountOfGiving:[Person countChurchStatus:@"Giving" InContext:[[NSApp delegate] managedObjectContext]]];
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    // add subView buttons
    NSArray *buttons = [NSArray arrayWithObjects:[self peopleButton],
                                                 [self childrenButton],
                                                 [self familiesButton],
                                                 [self usersButton],
                                                 [self adultButton],
                                                  nil];
    self.subViewButtons = buttons;
    [_peopleButton performClick:nil];

}

-(void)dealloc
{
}


@end
