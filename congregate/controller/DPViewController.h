//
//  DPViewController.h
//  congregate
//
//  Created by Mike Flores on 1/17/13.
//  Copyright (c) 2013 digiputty, llc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface DPViewController : NSViewController

@property id selectedObject;

@property (strong) IBOutlet NSArrayController *primaryArrayController;
@property (strong) IBOutlet NSView *exportView;
@property NSArray *arrangedObjects;

- (IBAction)newItem:(id)sender;
- (IBAction)delete:(id)sender;

- (void)viewWillLoad;
- (void)viewDidLoad;


@end
