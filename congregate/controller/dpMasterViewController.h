//
//  dpMasterViewController.h
//  congregate
//
//  Created by Mike Flores on 1/7/13.
//  Copyright (c) 2013 digiputty, llc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "DPViewController.h"

@interface dpMasterViewController : NSViewController

@property (weak) IBOutlet NSView *subView;
@property DPViewController *subViewController;
@property (strong) IBOutlet NSView *dashboardView;
@property id selectedObject;
@property (strong) NSArray *subViewButtons;
@property (weak) NSArrayController *primaryArrayController;
@property NSArray *arrangedObjects;

- (IBAction)newItem:(id)sender;
- (IBAction)delete:(id)sender;
- (IBAction)changeSubView:(id)sender;
- (void)changeSubViewController:(NSString*)viewName;
- (void)viewWillLoad;
- (void)viewDidLoad;

@end
