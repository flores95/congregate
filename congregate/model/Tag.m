#import "Tag.h"


@interface Tag ()

// Private interface goes here.

@end


@implementation Tag

-(BOOL)validateEntity:(NSError **)error
{
    BOOL retVal = YES;
    NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
    
    NSMutableString *errorDesc = [[NSMutableString alloc] init];
    
    if (![self name])
    {
        retVal = NO;
        [errorDesc appendString:@"* Name Required\n"];
    }
    
    [errorDetail setValue:errorDesc forKey:NSLocalizedDescriptionKey];
    *error = [NSError errorWithDomain:@"myDomain" code:100 userInfo:errorDetail];
    
    return retVal;
}
@end
