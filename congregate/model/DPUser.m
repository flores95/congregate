#import "DPUser.h"


@interface DPUser ()

// Private interface goes here.

@end


@implementation DPUser

+ (id)findUser:(NSString *)username InContext:(NSManagedObjectContext *)context
{
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"username == %@", username];
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[self entityName]];
    [fetch setPredicate:pred];
    
    NSError *error = nil;
    NSArray *results = [context executeFetchRequest:fetch error:&error];
    
    if (results && [results count] > 0)
    {
        return [results objectAtIndex:0];
    } else {
        return nil;
    }
}

- (bool)authenticatePassword:(NSString *)password
{
    return [[self password] isEqualToString:password];
}

-(BOOL)validateEntity:(NSError **)error
{
    BOOL retVal = YES;
    NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
    
    NSMutableString *errorDesc = [[NSMutableString alloc] init];
    if (![self username])
    {
        retVal = NO;
        [errorDesc appendString:@"* Username Required\n"];
    }
    
    if (![self password])
    {
        retVal = NO;
        [errorDesc appendString:@"* Password Required\n"];
    }
    
    if (![self personDetail])
    {
        retVal = NO;
        [errorDesc appendString:@"* Associated Person Required\n"];
    }

    [errorDetail setValue:errorDesc forKey:NSLocalizedDescriptionKey];
    *error = [NSError errorWithDomain:@"CONGREGATE" code:100 userInfo:errorDetail];
    
    return retVal;
}

@end
