#import "SignificantDate.h"


@interface SignificantDate ()

// Private interface goes here.

@end


@implementation SignificantDate

-(BOOL)validateEntity:(NSError **)error
{
    BOOL retVal = YES;
    NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
    
    NSMutableString *errorDesc = [[NSMutableString alloc] init];
    if (![self eventDate])
    {
        retVal = NO;
        [errorDesc appendString:@"* Date Required\n"];
    }
    
    if (![self eventTitle])
    {
        retVal = NO;
        [errorDesc appendString:@"* Title Required\n"];
    }
    [errorDetail setValue:errorDesc forKey:NSLocalizedDescriptionKey];
    *error = [NSError errorWithDomain:@"myDomain" code:100 userInfo:errorDetail];
    
    return retVal;
}

@end
