#import "Family.h"
#import "FamilyMember.h"


@interface Family ()

// Private interface goes here.

@end


@implementation Family

- (id)initWithEntity:(NSEntityDescription *)entity insertIntoManagedObjectContext:(NSManagedObjectContext *)context
{
    self = [super initWithEntity:entity insertIntoManagedObjectContext:context];
    
    [self addObserver:self forKeyPath:@"members"     options:NSKeyValueObservingOptionNew context:NULL];
   
    return self;
}

#pragma mark - Observation
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"members"])
    {
        NSNumber *kind = [change objectForKey:NSKeyValueChangeKindKey];
        if ([kind integerValue] == NSKeyValueChangeInsertion || [kind integerValue] == NSKeyValueChangeRemoval)
        {
            // a new one was added update the calculated field
            [self setMemberNames:nil];
        }
    }
}

- (void)dealloc
{
    [self removeObserver:self forKeyPath:@"members"];
}

# pragma mark - Custom Accessors

- (void)setActive:(NSNumber *)active
{
    if (self.active != active)
    {
        if (![active boolValue])
        {
            for (FamilyMember *curMember in [self members])
            {
                if ([curMember personDetail])
                {
                    [curMember.personDetail setActiveValue:NO];
                }
            }
        }
    }
    
    [self willAccessValueForKey:@"active"];
    [self setPrimitiveActive:active];
    [self didAccessValueForKey:@"active"];
}


# pragma mark - Calculated Field Accessors
- (void)setHeadOfFamily:(Person *)headOfFamily
{
    // do nothing it's a calculated field
}
- (Person *)headOfFamily
{
    if ([[self members] count] > 0)
    {
        for (FamilyMember *member in [self members])
        {
            if ([member headValue])
            {
                return [member personDetail];
            }
        }
    }
    return nil;
}

-(void)setMemberNames:(NSString *)memberNames
{
    // do nothing it's a calculated field
}

-(NSString *)memberNames
{
    NSString *retVal = @"";
    if ([[self members] count] > 0)
    {
        int memberCount = 0;
        for (FamilyMember *member in [self members])
        {
            memberCount++;
            retVal = [retVal stringByAppendingFormat:@"%@", [member.personDetail nickName]];
            if (memberCount != [[self members] count]) {
                retVal = [retVal stringByAppendingString:@", "];
            }
        }
    }
    return retVal;
}

-(void)setChildren:(NSSet *)children
{
    // do nothing it's a calculated field
}
-(NSSet *)children
{
    NSMutableSet *childSet = [NSMutableSet set];
    if ([[self members] count] > 0)
    {
        for (FamilyMember *member in [self members])
        {
            if ([[member role] isEqualToString:@"Child"])
            {
                [childSet addObject:[member personDetail]];
            }
        }
    }
    return [NSSet setWithSet:childSet];
}

-(void)setAdults:(NSSet *)adults
{
    // do nothing it's a calculated field
}
-(NSSet *)adults
{
    NSMutableSet *adultSet = [NSMutableSet set];
    if ([[self members] count] > 0)
    {
        for (FamilyMember *member in [self members])
        {
            if ([[member role] isEqualToString:@"Adult"])
            {
                [adultSet addObject:[member personDetail]];
            }
        }
    }
    return [NSSet setWithSet:adultSet];
}

@end
