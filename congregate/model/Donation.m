#import "Donation.h"
#import "Donor.h"


@interface Donation ()

// Private interface goes here.

@end


@implementation Donation

- (void)setDonorName:(NSString *)donorName
{
    //should never be called ... this is a calculated value
}

- (NSString *)donorName
{
    NSString *returnValue = nil;
    if ([self donor])
    {
        returnValue = [[self donor] displayName];
    }
    return returnValue;
}

-(BOOL)validateEntity:(NSError **)error
{
    BOOL retVal = YES;
    NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
    
    NSMutableString *errorDesc = [[NSMutableString alloc] init];
    if (![self donor])
    {
        retVal = NO;
        [errorDesc appendString:@"* Donor Required\n"];
    }
    
    if (![self type])
    {
        retVal = NO;
        [errorDesc appendString:@"* Donation Type Required\n"];
    }

    if (![self fund])
    {
        retVal = NO;
        [errorDesc appendString:@"* Fund Required\n"];
    }
    
    if ([[self type] isEqualToString:@"Check"] && ![self checkNumber])
    {
        retVal = NO;
        [errorDesc appendString:@"* Check Number Required\n"];
    }
    [errorDetail setValue:errorDesc forKey:NSLocalizedDescriptionKey];
    *error = [NSError errorWithDomain:@"myDomain" code:100 userInfo:errorDetail];
    
    return retVal;
}

@end
