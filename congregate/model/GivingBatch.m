#import "GivingBatch.h"
#import "Donation.h"


@interface GivingBatch ()

// Private interface goes here.

@end


@implementation GivingBatch

- (BOOL)deleteEntity:(NSError *__autoreleasing *)error
{
    // delete all the donations
    for (Donation *obj in [self donations])
    {
        [[self managedObjectContext] deleteObject:obj];
    }
    [[self managedObjectContext] deleteObject:self];
    return YES;
}

@end
