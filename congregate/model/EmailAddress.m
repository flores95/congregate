#import "EmailAddress.h"
#import "Person.h"

@interface EmailAddress ()

// Private interface goes here.

@end


@implementation EmailAddress

- (id)initWithEntity:(NSEntityDescription *)entity insertIntoManagedObjectContext:(NSManagedObjectContext *)context
{
    self = [super initWithEntity:entity insertIntoManagedObjectContext:context];
    
    [self addObserver:self forKeyPath:@"primary"            options:NSKeyValueObservingOptionNew context:NULL];
    
    return self;
}

#pragma mark - Observation
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"primary"])
    {
        // only one primary so unset others if necessary
        if ([self.primary boolValue])
        {
            for (EmailAddress *addr in [[self person] emailAddresses])
            {
                if ([[addr primary] boolValue] && ![addr isEqual:self])
                {
                    [addr setPrimaryValue:NO];
                }
            }
            [[self person] setPrimaryEmail:nil]; // trigger the calculated field update;
        }
    }
}

- (void)dealloc
{
    
    [self removeObserver:self forKeyPath:@"primary"];
}

#pragma mark - Validation

-(BOOL)validateEntity:(NSError **)error
{
    BOOL retVal = YES;
    NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
    
    NSMutableString *errorDesc = [[NSMutableString alloc] init];
    
    if (![self label])
    {
        retVal = NO;
        [errorDesc appendString:@"* Label Required\n"];
    }
    
    if (![self address])
    {
        retVal = NO;
        [errorDesc appendString:@"* Address Required\n"];
    }
    
    //TODO - should check for valid address
    
    [errorDetail setValue:errorDesc forKey:NSLocalizedDescriptionKey];
    *error = [NSError errorWithDomain:@"myDomain" code:100 userInfo:errorDetail];
    
    return retVal;
}
@end
