// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ItemCollection.m instead.

#import "_ItemCollection.h"

const struct ItemCollectionAttributes ItemCollectionAttributes = {
	.desc = @"desc",
	.name = @"name",
	.public = @"public",
	.type = @"type",
};

const struct ItemCollectionRelationships ItemCollectionRelationships = {
	.collectionUsers = @"collectionUsers",
	.items = @"items",
};

const struct ItemCollectionFetchedProperties ItemCollectionFetchedProperties = {
};

@implementation ItemCollectionID
@end

@implementation _ItemCollection

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"ItemCollection" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"ItemCollection";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"ItemCollection" inManagedObjectContext:moc_];
}

- (ItemCollectionID*)objectID {
	return (ItemCollectionID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"publicValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"public"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic desc;






@dynamic name;






@dynamic public;



- (BOOL)publicValue {
	NSNumber *result = [self public];
	return [result boolValue];
}

- (void)setPublicValue:(BOOL)value_ {
	[self setPublic:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitivePublicValue {
	NSNumber *result = [self primitivePublic];
	return [result boolValue];
}

- (void)setPrimitivePublicValue:(BOOL)value_ {
	[self setPrimitivePublic:[NSNumber numberWithBool:value_]];
}





@dynamic type;






@dynamic collectionUsers;

	
- (NSMutableSet*)collectionUsersSet {
	[self willAccessValueForKey:@"collectionUsers"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"collectionUsers"];
  
	[self didAccessValueForKey:@"collectionUsers"];
	return result;
}
	

@dynamic items;

	
- (NSMutableSet*)itemsSet {
	[self willAccessValueForKey:@"items"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"items"];
  
	[self didAccessValueForKey:@"items"];
	return result;
}
	






@end
