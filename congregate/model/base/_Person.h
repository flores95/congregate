// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Person.h instead.

#import <CoreData/CoreData.h>
#import "Donor.h"

extern const struct PersonAttributes {
	__unsafe_unretained NSString *adult;
	__unsafe_unretained NSString *birthday;
	__unsafe_unretained NSString *churchStatus;
	__unsafe_unretained NSString *firstName;
	__unsafe_unretained NSString *gender;
	__unsafe_unretained NSString *grade;
	__unsafe_unretained NSString *gradeAdjust;
	__unsafe_unretained NSString *lastName;
	__unsafe_unretained NSString *married;
	__unsafe_unretained NSString *member;
	__unsafe_unretained NSString *middleName;
	__unsafe_unretained NSString *nickName;
	__unsafe_unretained NSString *school;
	__unsafe_unretained NSString *specialNeeds;
	__unsafe_unretained NSString *status;
	__unsafe_unretained NSString *suffix;
	__unsafe_unretained NSString *title;
} PersonAttributes;

extern const struct PersonRelationships {
	__unsafe_unretained NSString *attendance;
	__unsafe_unretained NSString *emailAddresses;
	__unsafe_unretained NSString *family;
	__unsafe_unretained NSString *leading;
	__unsafe_unretained NSString *mailingAddresses;
	__unsafe_unretained NSString *memberOf;
	__unsafe_unretained NSString *phoneNumbers;
	__unsafe_unretained NSString *significantDates;
	__unsafe_unretained NSString *socialAddresses;
	__unsafe_unretained NSString *userAccount;
} PersonRelationships;

extern const struct PersonFetchedProperties {
} PersonFetchedProperties;

@class Attendance;
@class EmailAddress;
@class FamilyMember;
@class Group;
@class MailingAddress;
@class Group;
@class PhoneNumber;
@class SignificantDate;
@class SocialAddress;
@class DPUser;



















@interface PersonID : NSManagedObjectID {}
@end

@interface _Person : Donor {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (PersonID*)objectID;





@property (nonatomic, strong) NSNumber* adult;



@property BOOL adultValue;
- (BOOL)adultValue;
- (void)setAdultValue:(BOOL)value_;

//- (BOOL)validateAdult:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* birthday;



//- (BOOL)validateBirthday:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* churchStatus;



//- (BOOL)validateChurchStatus:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* firstName;



//- (BOOL)validateFirstName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* gender;



//- (BOOL)validateGender:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* grade;



@property int16_t gradeValue;
- (int16_t)gradeValue;
- (void)setGradeValue:(int16_t)value_;

//- (BOOL)validateGrade:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* gradeAdjust;



@property int16_t gradeAdjustValue;
- (int16_t)gradeAdjustValue;
- (void)setGradeAdjustValue:(int16_t)value_;

//- (BOOL)validateGradeAdjust:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* lastName;



//- (BOOL)validateLastName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* married;



@property BOOL marriedValue;
- (BOOL)marriedValue;
- (void)setMarriedValue:(BOOL)value_;

//- (BOOL)validateMarried:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* member;



@property BOOL memberValue;
- (BOOL)memberValue;
- (void)setMemberValue:(BOOL)value_;

//- (BOOL)validateMember:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* middleName;



//- (BOOL)validateMiddleName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* nickName;



//- (BOOL)validateNickName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* school;



//- (BOOL)validateSchool:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* specialNeeds;



//- (BOOL)validateSpecialNeeds:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* status;



//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* suffix;



//- (BOOL)validateSuffix:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* title;



//- (BOOL)validateTitle:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *attendance;

- (NSMutableSet*)attendanceSet;




@property (nonatomic, strong) NSSet *emailAddresses;

- (NSMutableSet*)emailAddressesSet;




@property (nonatomic, strong) FamilyMember *family;

//- (BOOL)validateFamily:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSSet *leading;

- (NSMutableSet*)leadingSet;




@property (nonatomic, strong) NSSet *mailingAddresses;

- (NSMutableSet*)mailingAddressesSet;




@property (nonatomic, strong) NSSet *memberOf;

- (NSMutableSet*)memberOfSet;




@property (nonatomic, strong) NSSet *phoneNumbers;

- (NSMutableSet*)phoneNumbersSet;




@property (nonatomic, strong) NSSet *significantDates;

- (NSMutableSet*)significantDatesSet;




@property (nonatomic, strong) NSSet *socialAddresses;

- (NSMutableSet*)socialAddressesSet;




@property (nonatomic, strong) DPUser *userAccount;

//- (BOOL)validateUserAccount:(id*)value_ error:(NSError**)error_;





@end

@interface _Person (CoreDataGeneratedAccessors)

- (void)addAttendance:(NSSet*)value_;
- (void)removeAttendance:(NSSet*)value_;
- (void)addAttendanceObject:(Attendance*)value_;
- (void)removeAttendanceObject:(Attendance*)value_;

- (void)addEmailAddresses:(NSSet*)value_;
- (void)removeEmailAddresses:(NSSet*)value_;
- (void)addEmailAddressesObject:(EmailAddress*)value_;
- (void)removeEmailAddressesObject:(EmailAddress*)value_;

- (void)addLeading:(NSSet*)value_;
- (void)removeLeading:(NSSet*)value_;
- (void)addLeadingObject:(Group*)value_;
- (void)removeLeadingObject:(Group*)value_;

- (void)addMailingAddresses:(NSSet*)value_;
- (void)removeMailingAddresses:(NSSet*)value_;
- (void)addMailingAddressesObject:(MailingAddress*)value_;
- (void)removeMailingAddressesObject:(MailingAddress*)value_;

- (void)addMemberOf:(NSSet*)value_;
- (void)removeMemberOf:(NSSet*)value_;
- (void)addMemberOfObject:(Group*)value_;
- (void)removeMemberOfObject:(Group*)value_;

- (void)addPhoneNumbers:(NSSet*)value_;
- (void)removePhoneNumbers:(NSSet*)value_;
- (void)addPhoneNumbersObject:(PhoneNumber*)value_;
- (void)removePhoneNumbersObject:(PhoneNumber*)value_;

- (void)addSignificantDates:(NSSet*)value_;
- (void)removeSignificantDates:(NSSet*)value_;
- (void)addSignificantDatesObject:(SignificantDate*)value_;
- (void)removeSignificantDatesObject:(SignificantDate*)value_;

- (void)addSocialAddresses:(NSSet*)value_;
- (void)removeSocialAddresses:(NSSet*)value_;
- (void)addSocialAddressesObject:(SocialAddress*)value_;
- (void)removeSocialAddressesObject:(SocialAddress*)value_;

@end

@interface _Person (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveAdult;
- (void)setPrimitiveAdult:(NSNumber*)value;

- (BOOL)primitiveAdultValue;
- (void)setPrimitiveAdultValue:(BOOL)value_;




- (NSDate*)primitiveBirthday;
- (void)setPrimitiveBirthday:(NSDate*)value;




- (NSString*)primitiveChurchStatus;
- (void)setPrimitiveChurchStatus:(NSString*)value;




- (NSString*)primitiveFirstName;
- (void)setPrimitiveFirstName:(NSString*)value;




- (NSString*)primitiveGender;
- (void)setPrimitiveGender:(NSString*)value;




- (NSNumber*)primitiveGrade;
- (void)setPrimitiveGrade:(NSNumber*)value;

- (int16_t)primitiveGradeValue;
- (void)setPrimitiveGradeValue:(int16_t)value_;




- (NSNumber*)primitiveGradeAdjust;
- (void)setPrimitiveGradeAdjust:(NSNumber*)value;

- (int16_t)primitiveGradeAdjustValue;
- (void)setPrimitiveGradeAdjustValue:(int16_t)value_;




- (NSString*)primitiveLastName;
- (void)setPrimitiveLastName:(NSString*)value;




- (NSNumber*)primitiveMarried;
- (void)setPrimitiveMarried:(NSNumber*)value;

- (BOOL)primitiveMarriedValue;
- (void)setPrimitiveMarriedValue:(BOOL)value_;




- (NSNumber*)primitiveMember;
- (void)setPrimitiveMember:(NSNumber*)value;

- (BOOL)primitiveMemberValue;
- (void)setPrimitiveMemberValue:(BOOL)value_;




- (NSString*)primitiveMiddleName;
- (void)setPrimitiveMiddleName:(NSString*)value;




- (NSString*)primitiveNickName;
- (void)setPrimitiveNickName:(NSString*)value;




- (NSString*)primitiveSchool;
- (void)setPrimitiveSchool:(NSString*)value;




- (NSString*)primitiveSpecialNeeds;
- (void)setPrimitiveSpecialNeeds:(NSString*)value;




- (NSString*)primitiveStatus;
- (void)setPrimitiveStatus:(NSString*)value;




- (NSString*)primitiveSuffix;
- (void)setPrimitiveSuffix:(NSString*)value;




- (NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(NSString*)value;





- (NSMutableSet*)primitiveAttendance;
- (void)setPrimitiveAttendance:(NSMutableSet*)value;



- (NSMutableSet*)primitiveEmailAddresses;
- (void)setPrimitiveEmailAddresses:(NSMutableSet*)value;



- (FamilyMember*)primitiveFamily;
- (void)setPrimitiveFamily:(FamilyMember*)value;



- (NSMutableSet*)primitiveLeading;
- (void)setPrimitiveLeading:(NSMutableSet*)value;



- (NSMutableSet*)primitiveMailingAddresses;
- (void)setPrimitiveMailingAddresses:(NSMutableSet*)value;



- (NSMutableSet*)primitiveMemberOf;
- (void)setPrimitiveMemberOf:(NSMutableSet*)value;



- (NSMutableSet*)primitivePhoneNumbers;
- (void)setPrimitivePhoneNumbers:(NSMutableSet*)value;



- (NSMutableSet*)primitiveSignificantDates;
- (void)setPrimitiveSignificantDates:(NSMutableSet*)value;



- (NSMutableSet*)primitiveSocialAddresses;
- (void)setPrimitiveSocialAddresses:(NSMutableSet*)value;



- (DPUser*)primitiveUserAccount;
- (void)setPrimitiveUserAccount:(DPUser*)value;


@end
