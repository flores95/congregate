// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MailingAddress.m instead.

#import "_MailingAddress.h"

const struct MailingAddressAttributes MailingAddressAttributes = {
	.addressLine1 = @"addressLine1",
	.addressLine2 = @"addressLine2",
	.city = @"city",
	.country = @"country",
	.postalCode = @"postalCode",
	.state = @"state",
};

const struct MailingAddressRelationships MailingAddressRelationships = {
	.person = @"person",
};

const struct MailingAddressFetchedProperties MailingAddressFetchedProperties = {
};

@implementation MailingAddressID
@end

@implementation _MailingAddress

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"MailingAddress" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"MailingAddress";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"MailingAddress" inManagedObjectContext:moc_];
}

- (MailingAddressID*)objectID {
	return (MailingAddressID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic addressLine1;






@dynamic addressLine2;






@dynamic city;






@dynamic country;






@dynamic postalCode;






@dynamic state;






@dynamic person;

	






@end
