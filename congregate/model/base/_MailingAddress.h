// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MailingAddress.h instead.

#import <CoreData/CoreData.h>
#import "ContactMethod.h"

extern const struct MailingAddressAttributes {
	__unsafe_unretained NSString *addressLine1;
	__unsafe_unretained NSString *addressLine2;
	__unsafe_unretained NSString *city;
	__unsafe_unretained NSString *country;
	__unsafe_unretained NSString *postalCode;
	__unsafe_unretained NSString *state;
} MailingAddressAttributes;

extern const struct MailingAddressRelationships {
	__unsafe_unretained NSString *person;
} MailingAddressRelationships;

extern const struct MailingAddressFetchedProperties {
} MailingAddressFetchedProperties;

@class Person;








@interface MailingAddressID : NSManagedObjectID {}
@end

@interface _MailingAddress : ContactMethod {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (MailingAddressID*)objectID;





@property (nonatomic, strong) NSString* addressLine1;



//- (BOOL)validateAddressLine1:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* addressLine2;



//- (BOOL)validateAddressLine2:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* city;



//- (BOOL)validateCity:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* country;



//- (BOOL)validateCountry:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* postalCode;



//- (BOOL)validatePostalCode:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* state;



//- (BOOL)validateState:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) Person *person;

//- (BOOL)validatePerson:(id*)value_ error:(NSError**)error_;





@end

@interface _MailingAddress (CoreDataGeneratedAccessors)

@end

@interface _MailingAddress (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveAddressLine1;
- (void)setPrimitiveAddressLine1:(NSString*)value;




- (NSString*)primitiveAddressLine2;
- (void)setPrimitiveAddressLine2:(NSString*)value;




- (NSString*)primitiveCity;
- (void)setPrimitiveCity:(NSString*)value;




- (NSString*)primitiveCountry;
- (void)setPrimitiveCountry:(NSString*)value;




- (NSString*)primitivePostalCode;
- (void)setPrimitivePostalCode:(NSString*)value;




- (NSString*)primitiveState;
- (void)setPrimitiveState:(NSString*)value;





- (Person*)primitivePerson;
- (void)setPrimitivePerson:(Person*)value;


@end
