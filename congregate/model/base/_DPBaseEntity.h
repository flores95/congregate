// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to DPBaseEntity.h instead.

#import <CoreData/CoreData.h>
#import "DPSyncEntity.h"

extern const struct DPBaseEntityAttributes {
	__unsafe_unretained NSString *createdBy;
	__unsafe_unretained NSString *modifiedBy;
} DPBaseEntityAttributes;

extern const struct DPBaseEntityRelationships {
} DPBaseEntityRelationships;

extern const struct DPBaseEntityFetchedProperties {
} DPBaseEntityFetchedProperties;





@interface DPBaseEntityID : NSManagedObjectID {}
@end

@interface _DPBaseEntity : DPSyncEntity {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (DPBaseEntityID*)objectID;





@property (nonatomic, strong) NSString* createdBy;



//- (BOOL)validateCreatedBy:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* modifiedBy;



//- (BOOL)validateModifiedBy:(id*)value_ error:(NSError**)error_;






@end

@interface _DPBaseEntity (CoreDataGeneratedAccessors)

@end

@interface _DPBaseEntity (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveCreatedBy;
- (void)setPrimitiveCreatedBy:(NSString*)value;




- (NSString*)primitiveModifiedBy;
- (void)setPrimitiveModifiedBy:(NSString*)value;




@end
