// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Group.m instead.

#import "_Group.h"

const struct GroupAttributes GroupAttributes = {
	.endDate = @"endDate",
	.endTime = @"endTime",
	.maxMembers = @"maxMembers",
	.recurring = @"recurring",
	.startDate = @"startDate",
	.startTime = @"startTime",
	.type = @"type",
};

const struct GroupRelationships GroupRelationships = {
	.attendance = @"attendance",
	.leaders = @"leaders",
	.location = @"location",
	.members = @"members",
};

const struct GroupFetchedProperties GroupFetchedProperties = {
};

@implementation GroupID
@end

@implementation _Group

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Group" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Group";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Group" inManagedObjectContext:moc_];
}

- (GroupID*)objectID {
	return (GroupID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"maxMembersValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"maxMembers"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"recurringValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"recurring"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic endDate;






@dynamic endTime;






@dynamic maxMembers;



- (int16_t)maxMembersValue {
	NSNumber *result = [self maxMembers];
	return [result shortValue];
}

- (void)setMaxMembersValue:(int16_t)value_ {
	[self setMaxMembers:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveMaxMembersValue {
	NSNumber *result = [self primitiveMaxMembers];
	return [result shortValue];
}

- (void)setPrimitiveMaxMembersValue:(int16_t)value_ {
	[self setPrimitiveMaxMembers:[NSNumber numberWithShort:value_]];
}





@dynamic recurring;



- (BOOL)recurringValue {
	NSNumber *result = [self recurring];
	return [result boolValue];
}

- (void)setRecurringValue:(BOOL)value_ {
	[self setRecurring:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveRecurringValue {
	NSNumber *result = [self primitiveRecurring];
	return [result boolValue];
}

- (void)setPrimitiveRecurringValue:(BOOL)value_ {
	[self setPrimitiveRecurring:[NSNumber numberWithBool:value_]];
}





@dynamic startDate;






@dynamic startTime;






@dynamic type;






@dynamic attendance;

	
- (NSMutableSet*)attendanceSet {
	[self willAccessValueForKey:@"attendance"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"attendance"];
  
	[self didAccessValueForKey:@"attendance"];
	return result;
}
	

@dynamic leaders;

	
- (NSMutableSet*)leadersSet {
	[self willAccessValueForKey:@"leaders"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"leaders"];
  
	[self didAccessValueForKey:@"leaders"];
	return result;
}
	

@dynamic location;

	

@dynamic members;

	
- (NSMutableSet*)membersSet {
	[self willAccessValueForKey:@"members"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"members"];
  
	[self didAccessValueForKey:@"members"];
	return result;
}
	






@end
