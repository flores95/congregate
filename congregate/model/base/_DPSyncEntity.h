// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to DPSyncEntity.h instead.

#import <CoreData/CoreData.h>


extern const struct DPSyncEntityAttributes {
	__unsafe_unretained NSString *createdAt;
	__unsafe_unretained NSString *deleted;
	__unsafe_unretained NSString *objectId;
	__unsafe_unretained NSString *sourceCreatedAt;
	__unsafe_unretained NSString *sourceObjectId;
	__unsafe_unretained NSString *sourceUpdatedAt;
	__unsafe_unretained NSString *updatedAt;
} DPSyncEntityAttributes;

extern const struct DPSyncEntityRelationships {
} DPSyncEntityRelationships;

extern const struct DPSyncEntityFetchedProperties {
} DPSyncEntityFetchedProperties;










@interface DPSyncEntityID : NSManagedObjectID {}
@end

@interface _DPSyncEntity : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (DPSyncEntityID*)objectID;





@property (nonatomic, strong) NSDate* createdAt;



//- (BOOL)validateCreatedAt:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* deleted;



@property BOOL deletedValue;
- (BOOL)deletedValue;
- (void)setDeletedValue:(BOOL)value_;

//- (BOOL)validateDeleted:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* objectId;



//- (BOOL)validateObjectId:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* sourceCreatedAt;



//- (BOOL)validateSourceCreatedAt:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* sourceObjectId;



//- (BOOL)validateSourceObjectId:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* sourceUpdatedAt;



//- (BOOL)validateSourceUpdatedAt:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* updatedAt;



//- (BOOL)validateUpdatedAt:(id*)value_ error:(NSError**)error_;






@end

@interface _DPSyncEntity (CoreDataGeneratedAccessors)

@end

@interface _DPSyncEntity (CoreDataGeneratedPrimitiveAccessors)


- (NSDate*)primitiveCreatedAt;
- (void)setPrimitiveCreatedAt:(NSDate*)value;




- (NSNumber*)primitiveDeleted;
- (void)setPrimitiveDeleted:(NSNumber*)value;

- (BOOL)primitiveDeletedValue;
- (void)setPrimitiveDeletedValue:(BOOL)value_;




- (NSString*)primitiveObjectId;
- (void)setPrimitiveObjectId:(NSString*)value;




- (NSDate*)primitiveSourceCreatedAt;
- (void)setPrimitiveSourceCreatedAt:(NSDate*)value;




- (NSString*)primitiveSourceObjectId;
- (void)setPrimitiveSourceObjectId:(NSString*)value;




- (NSDate*)primitiveSourceUpdatedAt;
- (void)setPrimitiveSourceUpdatedAt:(NSDate*)value;




- (NSDate*)primitiveUpdatedAt;
- (void)setPrimitiveUpdatedAt:(NSDate*)value;




@end
