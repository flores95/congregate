// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Donation.m instead.

#import "_Donation.h"

const struct DonationAttributes DonationAttributes = {
	.amount = @"amount",
	.checkNumber = @"checkNumber",
	.nonDeductAmount = @"nonDeductAmount",
	.split = @"split",
	.type = @"type",
};

const struct DonationRelationships DonationRelationships = {
	.batch = @"batch",
	.donor = @"donor",
	.fund = @"fund",
};

const struct DonationFetchedProperties DonationFetchedProperties = {
};

@implementation DonationID
@end

@implementation _Donation

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Donation" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Donation";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Donation" inManagedObjectContext:moc_];
}

- (DonationID*)objectID {
	return (DonationID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"splitValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"split"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic amount;






@dynamic checkNumber;






@dynamic nonDeductAmount;






@dynamic split;



- (BOOL)splitValue {
	NSNumber *result = [self split];
	return [result boolValue];
}

- (void)setSplitValue:(BOOL)value_ {
	[self setSplit:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveSplitValue {
	NSNumber *result = [self primitiveSplit];
	return [result boolValue];
}

- (void)setPrimitiveSplitValue:(BOOL)value_ {
	[self setPrimitiveSplit:[NSNumber numberWithBool:value_]];
}





@dynamic type;






@dynamic batch;

	

@dynamic donor;

	

@dynamic fund;

	






@end
