// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Tag.h instead.

#import <CoreData/CoreData.h>
#import "DPBaseEntity.h"

extern const struct TagAttributes {
	__unsafe_unretained NSString *name;
} TagAttributes;

extern const struct TagRelationships {
	__unsafe_unretained NSString *tagUsers;
	__unsafe_unretained NSString *taggedItems;
} TagRelationships;

extern const struct TagFetchedProperties {
} TagFetchedProperties;

@class DPUser;
@class Collectible;



@interface TagID : NSManagedObjectID {}
@end

@interface _Tag : DPBaseEntity {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (TagID*)objectID;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *tagUsers;

- (NSMutableSet*)tagUsersSet;




@property (nonatomic, strong) NSSet *taggedItems;

- (NSMutableSet*)taggedItemsSet;





@end

@interface _Tag (CoreDataGeneratedAccessors)

- (void)addTagUsers:(NSSet*)value_;
- (void)removeTagUsers:(NSSet*)value_;
- (void)addTagUsersObject:(DPUser*)value_;
- (void)removeTagUsersObject:(DPUser*)value_;

- (void)addTaggedItems:(NSSet*)value_;
- (void)removeTaggedItems:(NSSet*)value_;
- (void)addTaggedItemsObject:(Collectible*)value_;
- (void)removeTaggedItemsObject:(Collectible*)value_;

@end

@interface _Tag (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;





- (NSMutableSet*)primitiveTagUsers;
- (void)setPrimitiveTagUsers:(NSMutableSet*)value;



- (NSMutableSet*)primitiveTaggedItems;
- (void)setPrimitiveTaggedItems:(NSMutableSet*)value;


@end
