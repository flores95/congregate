// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to PhoneNumber.m instead.

#import "_PhoneNumber.h"

const struct PhoneNumberAttributes PhoneNumberAttributes = {
	.canText = @"canText",
	.countryCode = @"countryCode",
	.number = @"number",
};

const struct PhoneNumberRelationships PhoneNumberRelationships = {
	.person = @"person",
};

const struct PhoneNumberFetchedProperties PhoneNumberFetchedProperties = {
};

@implementation PhoneNumberID
@end

@implementation _PhoneNumber

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"PhoneNumber" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"PhoneNumber";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"PhoneNumber" inManagedObjectContext:moc_];
}

- (PhoneNumberID*)objectID {
	return (PhoneNumberID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"canTextValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"canText"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic canText;



- (BOOL)canTextValue {
	NSNumber *result = [self canText];
	return [result boolValue];
}

- (void)setCanTextValue:(BOOL)value_ {
	[self setCanText:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveCanTextValue {
	NSNumber *result = [self primitiveCanText];
	return [result boolValue];
}

- (void)setPrimitiveCanTextValue:(BOOL)value_ {
	[self setPrimitiveCanText:[NSNumber numberWithBool:value_]];
}





@dynamic countryCode;






@dynamic number;






@dynamic person;

	






@end
