// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to FamilyMember.h instead.

#import <CoreData/CoreData.h>
#import "DPBaseEntity.h"

extern const struct FamilyMemberAttributes {
	__unsafe_unretained NSString *head;
	__unsafe_unretained NSString *role;
} FamilyMemberAttributes;

extern const struct FamilyMemberRelationships {
	__unsafe_unretained NSString *family;
	__unsafe_unretained NSString *personDetail;
} FamilyMemberRelationships;

extern const struct FamilyMemberFetchedProperties {
} FamilyMemberFetchedProperties;

@class Family;
@class Person;




@interface FamilyMemberID : NSManagedObjectID {}
@end

@interface _FamilyMember : DPBaseEntity {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (FamilyMemberID*)objectID;





@property (nonatomic, strong) NSNumber* head;



@property BOOL headValue;
- (BOOL)headValue;
- (void)setHeadValue:(BOOL)value_;

//- (BOOL)validateHead:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* role;



//- (BOOL)validateRole:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) Family *family;

//- (BOOL)validateFamily:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) Person *personDetail;

//- (BOOL)validatePersonDetail:(id*)value_ error:(NSError**)error_;





@end

@interface _FamilyMember (CoreDataGeneratedAccessors)

@end

@interface _FamilyMember (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveHead;
- (void)setPrimitiveHead:(NSNumber*)value;

- (BOOL)primitiveHeadValue;
- (void)setPrimitiveHeadValue:(BOOL)value_;




- (NSString*)primitiveRole;
- (void)setPrimitiveRole:(NSString*)value;





- (Family*)primitiveFamily;
- (void)setPrimitiveFamily:(Family*)value;



- (Person*)primitivePersonDetail;
- (void)setPrimitivePersonDetail:(Person*)value;


@end
