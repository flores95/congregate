// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Attendance.m instead.

#import "_Attendance.h"

const struct AttendanceAttributes AttendanceAttributes = {
	.attendanceDate = @"attendanceDate",
	.present = @"present",
};

const struct AttendanceRelationships AttendanceRelationships = {
	.group = @"group",
	.person = @"person",
};

const struct AttendanceFetchedProperties AttendanceFetchedProperties = {
};

@implementation AttendanceID
@end

@implementation _Attendance

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Attendance" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Attendance";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Attendance" inManagedObjectContext:moc_];
}

- (AttendanceID*)objectID {
	return (AttendanceID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"presentValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"present"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic attendanceDate;






@dynamic present;



- (BOOL)presentValue {
	NSNumber *result = [self present];
	return [result boolValue];
}

- (void)setPresentValue:(BOOL)value_ {
	[self setPresent:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitivePresentValue {
	NSNumber *result = [self primitivePresent];
	return [result boolValue];
}

- (void)setPrimitivePresentValue:(BOOL)value_ {
	[self setPrimitivePresent:[NSNumber numberWithBool:value_]];
}





@dynamic group;

	

@dynamic person;

	






@end
