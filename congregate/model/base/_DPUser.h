// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to DPUser.h instead.

#import <CoreData/CoreData.h>
#import "DPBaseEntity.h"

extern const struct DPUserAttributes {
	__unsafe_unretained NSString *password;
	__unsafe_unretained NSString *username;
} DPUserAttributes;

extern const struct DPUserRelationships {
	__unsafe_unretained NSString *personDetail;
	__unsafe_unretained NSString *userCollections;
	__unsafe_unretained NSString *userNotes;
	__unsafe_unretained NSString *userTags;
} DPUserRelationships;

extern const struct DPUserFetchedProperties {
} DPUserFetchedProperties;

@class Person;
@class ItemCollection;
@class Note;
@class Tag;




@interface DPUserID : NSManagedObjectID {}
@end

@interface _DPUser : DPBaseEntity {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (DPUserID*)objectID;





@property (nonatomic, strong) NSString* password;



//- (BOOL)validatePassword:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* username;



//- (BOOL)validateUsername:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) Person *personDetail;

//- (BOOL)validatePersonDetail:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSSet *userCollections;

- (NSMutableSet*)userCollectionsSet;




@property (nonatomic, strong) NSSet *userNotes;

- (NSMutableSet*)userNotesSet;




@property (nonatomic, strong) NSSet *userTags;

- (NSMutableSet*)userTagsSet;





@end

@interface _DPUser (CoreDataGeneratedAccessors)

- (void)addUserCollections:(NSSet*)value_;
- (void)removeUserCollections:(NSSet*)value_;
- (void)addUserCollectionsObject:(ItemCollection*)value_;
- (void)removeUserCollectionsObject:(ItemCollection*)value_;

- (void)addUserNotes:(NSSet*)value_;
- (void)removeUserNotes:(NSSet*)value_;
- (void)addUserNotesObject:(Note*)value_;
- (void)removeUserNotesObject:(Note*)value_;

- (void)addUserTags:(NSSet*)value_;
- (void)removeUserTags:(NSSet*)value_;
- (void)addUserTagsObject:(Tag*)value_;
- (void)removeUserTagsObject:(Tag*)value_;

@end

@interface _DPUser (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitivePassword;
- (void)setPrimitivePassword:(NSString*)value;




- (NSString*)primitiveUsername;
- (void)setPrimitiveUsername:(NSString*)value;





- (Person*)primitivePersonDetail;
- (void)setPrimitivePersonDetail:(Person*)value;



- (NSMutableSet*)primitiveUserCollections;
- (void)setPrimitiveUserCollections:(NSMutableSet*)value;



- (NSMutableSet*)primitiveUserNotes;
- (void)setPrimitiveUserNotes:(NSMutableSet*)value;



- (NSMutableSet*)primitiveUserTags;
- (void)setPrimitiveUserTags:(NSMutableSet*)value;


@end
