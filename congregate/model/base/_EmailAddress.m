// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to EmailAddress.m instead.

#import "_EmailAddress.h"

const struct EmailAddressAttributes EmailAddressAttributes = {
	.address = @"address",
};

const struct EmailAddressRelationships EmailAddressRelationships = {
	.person = @"person",
};

const struct EmailAddressFetchedProperties EmailAddressFetchedProperties = {
};

@implementation EmailAddressID
@end

@implementation _EmailAddress

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"EmailAddress" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"EmailAddress";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"EmailAddress" inManagedObjectContext:moc_];
}

- (EmailAddressID*)objectID {
	return (EmailAddressID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic address;






@dynamic person;

	






@end
