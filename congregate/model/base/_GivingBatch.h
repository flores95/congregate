// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to GivingBatch.h instead.

#import <CoreData/CoreData.h>
#import "DPBaseEntity.h"

extern const struct GivingBatchAttributes {
	__unsafe_unretained NSString *givingDate;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *open;
} GivingBatchAttributes;

extern const struct GivingBatchRelationships {
	__unsafe_unretained NSString *donations;
} GivingBatchRelationships;

extern const struct GivingBatchFetchedProperties {
} GivingBatchFetchedProperties;

@class Donation;





@interface GivingBatchID : NSManagedObjectID {}
@end

@interface _GivingBatch : DPBaseEntity {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (GivingBatchID*)objectID;





@property (nonatomic, strong) NSDate* givingDate;



//- (BOOL)validateGivingDate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* open;



@property BOOL openValue;
- (BOOL)openValue;
- (void)setOpenValue:(BOOL)value_;

//- (BOOL)validateOpen:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *donations;

- (NSMutableSet*)donationsSet;





@end

@interface _GivingBatch (CoreDataGeneratedAccessors)

- (void)addDonations:(NSSet*)value_;
- (void)removeDonations:(NSSet*)value_;
- (void)addDonationsObject:(Donation*)value_;
- (void)removeDonationsObject:(Donation*)value_;

@end

@interface _GivingBatch (CoreDataGeneratedPrimitiveAccessors)


- (NSDate*)primitiveGivingDate;
- (void)setPrimitiveGivingDate:(NSDate*)value;




- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;




- (NSNumber*)primitiveOpen;
- (void)setPrimitiveOpen:(NSNumber*)value;

- (BOOL)primitiveOpenValue;
- (void)setPrimitiveOpenValue:(BOOL)value_;





- (NSMutableSet*)primitiveDonations;
- (void)setPrimitiveDonations:(NSMutableSet*)value;


@end
