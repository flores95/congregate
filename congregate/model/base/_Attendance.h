// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Attendance.h instead.

#import <CoreData/CoreData.h>
#import "DPBaseEntity.h"

extern const struct AttendanceAttributes {
	__unsafe_unretained NSString *attendanceDate;
	__unsafe_unretained NSString *present;
} AttendanceAttributes;

extern const struct AttendanceRelationships {
	__unsafe_unretained NSString *group;
	__unsafe_unretained NSString *person;
} AttendanceRelationships;

extern const struct AttendanceFetchedProperties {
} AttendanceFetchedProperties;

@class Group;
@class Person;




@interface AttendanceID : NSManagedObjectID {}
@end

@interface _Attendance : DPBaseEntity {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (AttendanceID*)objectID;





@property (nonatomic, strong) NSDate* attendanceDate;



//- (BOOL)validateAttendanceDate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* present;



@property BOOL presentValue;
- (BOOL)presentValue;
- (void)setPresentValue:(BOOL)value_;

//- (BOOL)validatePresent:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) Group *group;

//- (BOOL)validateGroup:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) Person *person;

//- (BOOL)validatePerson:(id*)value_ error:(NSError**)error_;





@end

@interface _Attendance (CoreDataGeneratedAccessors)

@end

@interface _Attendance (CoreDataGeneratedPrimitiveAccessors)


- (NSDate*)primitiveAttendanceDate;
- (void)setPrimitiveAttendanceDate:(NSDate*)value;




- (NSNumber*)primitivePresent;
- (void)setPrimitivePresent:(NSNumber*)value;

- (BOOL)primitivePresentValue;
- (void)setPrimitivePresentValue:(BOOL)value_;





- (Group*)primitiveGroup;
- (void)setPrimitiveGroup:(Group*)value;



- (Person*)primitivePerson;
- (void)setPrimitivePerson:(Person*)value;


@end
