// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ItemCollection.h instead.

#import <CoreData/CoreData.h>
#import "DPBaseEntity.h"

extern const struct ItemCollectionAttributes {
	__unsafe_unretained NSString *desc;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *public;
	__unsafe_unretained NSString *type;
} ItemCollectionAttributes;

extern const struct ItemCollectionRelationships {
	__unsafe_unretained NSString *collectionUsers;
	__unsafe_unretained NSString *items;
} ItemCollectionRelationships;

extern const struct ItemCollectionFetchedProperties {
} ItemCollectionFetchedProperties;

@class DPUser;
@class Collectible;






@interface ItemCollectionID : NSManagedObjectID {}
@end

@interface _ItemCollection : DPBaseEntity {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (ItemCollectionID*)objectID;





@property (nonatomic, strong) NSString* desc;



//- (BOOL)validateDesc:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* public;



@property BOOL publicValue;
- (BOOL)publicValue;
- (void)setPublicValue:(BOOL)value_;

//- (BOOL)validatePublic:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* type;



//- (BOOL)validateType:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *collectionUsers;

- (NSMutableSet*)collectionUsersSet;




@property (nonatomic, strong) NSSet *items;

- (NSMutableSet*)itemsSet;





@end

@interface _ItemCollection (CoreDataGeneratedAccessors)

- (void)addCollectionUsers:(NSSet*)value_;
- (void)removeCollectionUsers:(NSSet*)value_;
- (void)addCollectionUsersObject:(DPUser*)value_;
- (void)removeCollectionUsersObject:(DPUser*)value_;

- (void)addItems:(NSSet*)value_;
- (void)removeItems:(NSSet*)value_;
- (void)addItemsObject:(Collectible*)value_;
- (void)removeItemsObject:(Collectible*)value_;

@end

@interface _ItemCollection (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveDesc;
- (void)setPrimitiveDesc:(NSString*)value;




- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;




- (NSNumber*)primitivePublic;
- (void)setPrimitivePublic:(NSNumber*)value;

- (BOOL)primitivePublicValue;
- (void)setPrimitivePublicValue:(BOOL)value_;




- (NSString*)primitiveType;
- (void)setPrimitiveType:(NSString*)value;





- (NSMutableSet*)primitiveCollectionUsers;
- (void)setPrimitiveCollectionUsers:(NSMutableSet*)value;



- (NSMutableSet*)primitiveItems;
- (void)setPrimitiveItems:(NSMutableSet*)value;


@end
