// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Location.h instead.

#import <CoreData/CoreData.h>
#import "Collectible.h"

extern const struct LocationAttributes {
	__unsafe_unretained NSString *desc;
} LocationAttributes;

extern const struct LocationRelationships {
	__unsafe_unretained NSString *groupMeetings;
} LocationRelationships;

extern const struct LocationFetchedProperties {
} LocationFetchedProperties;

@class Group;



@interface LocationID : NSManagedObjectID {}
@end

@interface _Location : Collectible {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (LocationID*)objectID;





@property (nonatomic, strong) NSString* desc;



//- (BOOL)validateDesc:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) Group *groupMeetings;

//- (BOOL)validateGroupMeetings:(id*)value_ error:(NSError**)error_;





@end

@interface _Location (CoreDataGeneratedAccessors)

@end

@interface _Location (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveDesc;
- (void)setPrimitiveDesc:(NSString*)value;





- (Group*)primitiveGroupMeetings;
- (void)setPrimitiveGroupMeetings:(Group*)value;


@end
