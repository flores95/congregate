// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to SocialAddress.m instead.

#import "_SocialAddress.h"

const struct SocialAddressAttributes SocialAddressAttributes = {
	.networkName = @"networkName",
	.url = @"url",
	.userName = @"userName",
};

const struct SocialAddressRelationships SocialAddressRelationships = {
	.person = @"person",
};

const struct SocialAddressFetchedProperties SocialAddressFetchedProperties = {
};

@implementation SocialAddressID
@end

@implementation _SocialAddress

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"SocialAddress" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"SocialAddress";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"SocialAddress" inManagedObjectContext:moc_];
}

- (SocialAddressID*)objectID {
	return (SocialAddressID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic networkName;






@dynamic url;






@dynamic userName;






@dynamic person;

	






@end
