// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to EmailAddress.h instead.

#import <CoreData/CoreData.h>
#import "ContactMethod.h"

extern const struct EmailAddressAttributes {
	__unsafe_unretained NSString *address;
} EmailAddressAttributes;

extern const struct EmailAddressRelationships {
	__unsafe_unretained NSString *person;
} EmailAddressRelationships;

extern const struct EmailAddressFetchedProperties {
} EmailAddressFetchedProperties;

@class Person;



@interface EmailAddressID : NSManagedObjectID {}
@end

@interface _EmailAddress : ContactMethod {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (EmailAddressID*)objectID;





@property (nonatomic, strong) NSString* address;



//- (BOOL)validateAddress:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) Person *person;

//- (BOOL)validatePerson:(id*)value_ error:(NSError**)error_;





@end

@interface _EmailAddress (CoreDataGeneratedAccessors)

@end

@interface _EmailAddress (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveAddress;
- (void)setPrimitiveAddress:(NSString*)value;





- (Person*)primitivePerson;
- (void)setPrimitivePerson:(Person*)value;


@end
