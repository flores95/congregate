// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to DPUser.m instead.

#import "_DPUser.h"

const struct DPUserAttributes DPUserAttributes = {
	.password = @"password",
	.username = @"username",
};

const struct DPUserRelationships DPUserRelationships = {
	.personDetail = @"personDetail",
	.userCollections = @"userCollections",
	.userNotes = @"userNotes",
	.userTags = @"userTags",
};

const struct DPUserFetchedProperties DPUserFetchedProperties = {
};

@implementation DPUserID
@end

@implementation _DPUser

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"DPUser" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"DPUser";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"DPUser" inManagedObjectContext:moc_];
}

- (DPUserID*)objectID {
	return (DPUserID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic password;






@dynamic username;






@dynamic personDetail;

	

@dynamic userCollections;

	
- (NSMutableSet*)userCollectionsSet {
	[self willAccessValueForKey:@"userCollections"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"userCollections"];
  
	[self didAccessValueForKey:@"userCollections"];
	return result;
}
	

@dynamic userNotes;

	
- (NSMutableSet*)userNotesSet {
	[self willAccessValueForKey:@"userNotes"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"userNotes"];
  
	[self didAccessValueForKey:@"userNotes"];
	return result;
}
	

@dynamic userTags;

	
- (NSMutableSet*)userTagsSet {
	[self willAccessValueForKey:@"userTags"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"userTags"];
  
	[self didAccessValueForKey:@"userTags"];
	return result;
}
	






@end
