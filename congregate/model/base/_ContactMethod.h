// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ContactMethod.h instead.

#import <CoreData/CoreData.h>
#import "DPBaseEntity.h"

extern const struct ContactMethodAttributes {
	__unsafe_unretained NSString *label;
	__unsafe_unretained NSString *primary;
	__unsafe_unretained NSString *publish;
} ContactMethodAttributes;

extern const struct ContactMethodRelationships {
} ContactMethodRelationships;

extern const struct ContactMethodFetchedProperties {
} ContactMethodFetchedProperties;






@interface ContactMethodID : NSManagedObjectID {}
@end

@interface _ContactMethod : DPBaseEntity {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (ContactMethodID*)objectID;





@property (nonatomic, strong) NSString* label;



//- (BOOL)validateLabel:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* primary;



@property BOOL primaryValue;
- (BOOL)primaryValue;
- (void)setPrimaryValue:(BOOL)value_;

//- (BOOL)validatePrimary:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* publish;



@property BOOL publishValue;
- (BOOL)publishValue;
- (void)setPublishValue:(BOOL)value_;

//- (BOOL)validatePublish:(id*)value_ error:(NSError**)error_;






@end

@interface _ContactMethod (CoreDataGeneratedAccessors)

@end

@interface _ContactMethod (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveLabel;
- (void)setPrimitiveLabel:(NSString*)value;




- (NSNumber*)primitivePrimary;
- (void)setPrimitivePrimary:(NSNumber*)value;

- (BOOL)primitivePrimaryValue;
- (void)setPrimitivePrimaryValue:(BOOL)value_;




- (NSNumber*)primitivePublish;
- (void)setPrimitivePublish:(NSNumber*)value;

- (BOOL)primitivePublishValue;
- (void)setPrimitivePublishValue:(BOOL)value_;




@end
