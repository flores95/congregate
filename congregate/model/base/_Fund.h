// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Fund.h instead.

#import <CoreData/CoreData.h>
#import "DPBaseEntity.h"

extern const struct FundAttributes {
	__unsafe_unretained NSString *desc;
	__unsafe_unretained NSString *internalName;
	__unsafe_unretained NSString *name;
} FundAttributes;

extern const struct FundRelationships {
	__unsafe_unretained NSString *donations;
} FundRelationships;

extern const struct FundFetchedProperties {
} FundFetchedProperties;

@class Donation;





@interface FundID : NSManagedObjectID {}
@end

@interface _Fund : DPBaseEntity {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (FundID*)objectID;





@property (nonatomic, strong) NSString* desc;



//- (BOOL)validateDesc:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* internalName;



//- (BOOL)validateInternalName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *donations;

- (NSMutableSet*)donationsSet;





@end

@interface _Fund (CoreDataGeneratedAccessors)

- (void)addDonations:(NSSet*)value_;
- (void)removeDonations:(NSSet*)value_;
- (void)addDonationsObject:(Donation*)value_;
- (void)removeDonationsObject:(Donation*)value_;

@end

@interface _Fund (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveDesc;
- (void)setPrimitiveDesc:(NSString*)value;




- (NSString*)primitiveInternalName;
- (void)setPrimitiveInternalName:(NSString*)value;




- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;





- (NSMutableSet*)primitiveDonations;
- (void)setPrimitiveDonations:(NSMutableSet*)value;


@end
