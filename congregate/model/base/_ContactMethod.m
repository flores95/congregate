// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ContactMethod.m instead.

#import "_ContactMethod.h"

const struct ContactMethodAttributes ContactMethodAttributes = {
	.label = @"label",
	.primary = @"primary",
	.publish = @"publish",
};

const struct ContactMethodRelationships ContactMethodRelationships = {
};

const struct ContactMethodFetchedProperties ContactMethodFetchedProperties = {
};

@implementation ContactMethodID
@end

@implementation _ContactMethod

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"ContactMethod" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"ContactMethod";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"ContactMethod" inManagedObjectContext:moc_];
}

- (ContactMethodID*)objectID {
	return (ContactMethodID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"primaryValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"primary"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"publishValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"publish"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic label;






@dynamic primary;



- (BOOL)primaryValue {
	NSNumber *result = [self primary];
	return [result boolValue];
}

- (void)setPrimaryValue:(BOOL)value_ {
	[self setPrimary:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitivePrimaryValue {
	NSNumber *result = [self primitivePrimary];
	return [result boolValue];
}

- (void)setPrimitivePrimaryValue:(BOOL)value_ {
	[self setPrimitivePrimary:[NSNumber numberWithBool:value_]];
}





@dynamic publish;



- (BOOL)publishValue {
	NSNumber *result = [self publish];
	return [result boolValue];
}

- (void)setPublishValue:(BOOL)value_ {
	[self setPublish:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitivePublishValue {
	NSNumber *result = [self primitivePublish];
	return [result boolValue];
}

- (void)setPrimitivePublishValue:(BOOL)value_ {
	[self setPrimitivePublish:[NSNumber numberWithBool:value_]];
}










@end
