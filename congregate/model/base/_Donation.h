// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Donation.h instead.

#import <CoreData/CoreData.h>
#import "DPBaseEntity.h"

extern const struct DonationAttributes {
	__unsafe_unretained NSString *amount;
	__unsafe_unretained NSString *checkNumber;
	__unsafe_unretained NSString *nonDeductAmount;
	__unsafe_unretained NSString *split;
	__unsafe_unretained NSString *type;
} DonationAttributes;

extern const struct DonationRelationships {
	__unsafe_unretained NSString *batch;
	__unsafe_unretained NSString *donor;
	__unsafe_unretained NSString *fund;
} DonationRelationships;

extern const struct DonationFetchedProperties {
} DonationFetchedProperties;

@class GivingBatch;
@class Donor;
@class Fund;







@interface DonationID : NSManagedObjectID {}
@end

@interface _Donation : DPBaseEntity {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (DonationID*)objectID;





@property (nonatomic, strong) NSDecimalNumber* amount;



//- (BOOL)validateAmount:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* checkNumber;



//- (BOOL)validateCheckNumber:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDecimalNumber* nonDeductAmount;



//- (BOOL)validateNonDeductAmount:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* split;



@property BOOL splitValue;
- (BOOL)splitValue;
- (void)setSplitValue:(BOOL)value_;

//- (BOOL)validateSplit:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* type;



//- (BOOL)validateType:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) GivingBatch *batch;

//- (BOOL)validateBatch:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) Donor *donor;

//- (BOOL)validateDonor:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) Fund *fund;

//- (BOOL)validateFund:(id*)value_ error:(NSError**)error_;





@end

@interface _Donation (CoreDataGeneratedAccessors)

@end

@interface _Donation (CoreDataGeneratedPrimitiveAccessors)


- (NSDecimalNumber*)primitiveAmount;
- (void)setPrimitiveAmount:(NSDecimalNumber*)value;




- (NSString*)primitiveCheckNumber;
- (void)setPrimitiveCheckNumber:(NSString*)value;




- (NSDecimalNumber*)primitiveNonDeductAmount;
- (void)setPrimitiveNonDeductAmount:(NSDecimalNumber*)value;




- (NSNumber*)primitiveSplit;
- (void)setPrimitiveSplit:(NSNumber*)value;

- (BOOL)primitiveSplitValue;
- (void)setPrimitiveSplitValue:(BOOL)value_;




- (NSString*)primitiveType;
- (void)setPrimitiveType:(NSString*)value;





- (GivingBatch*)primitiveBatch;
- (void)setPrimitiveBatch:(GivingBatch*)value;



- (Donor*)primitiveDonor;
- (void)setPrimitiveDonor:(Donor*)value;



- (Fund*)primitiveFund;
- (void)setPrimitiveFund:(Fund*)value;


@end
