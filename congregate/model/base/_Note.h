// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Note.h instead.

#import <CoreData/CoreData.h>
#import "DPBaseEntity.h"

extern const struct NoteAttributes {
	__unsafe_unretained NSString *noteText;
	__unsafe_unretained NSString *notedAt;
} NoteAttributes;

extern const struct NoteRelationships {
	__unsafe_unretained NSString *noteAbout;
	__unsafe_unretained NSString *noteUsers;
} NoteRelationships;

extern const struct NoteFetchedProperties {
} NoteFetchedProperties;

@class Collectible;
@class DPUser;




@interface NoteID : NSManagedObjectID {}
@end

@interface _Note : DPBaseEntity {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (NoteID*)objectID;





@property (nonatomic, strong) NSString* noteText;



//- (BOOL)validateNoteText:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* notedAt;



//- (BOOL)validateNotedAt:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) Collectible *noteAbout;

//- (BOOL)validateNoteAbout:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSSet *noteUsers;

- (NSMutableSet*)noteUsersSet;





@end

@interface _Note (CoreDataGeneratedAccessors)

- (void)addNoteUsers:(NSSet*)value_;
- (void)removeNoteUsers:(NSSet*)value_;
- (void)addNoteUsersObject:(DPUser*)value_;
- (void)removeNoteUsersObject:(DPUser*)value_;

@end

@interface _Note (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveNoteText;
- (void)setPrimitiveNoteText:(NSString*)value;




- (NSDate*)primitiveNotedAt;
- (void)setPrimitiveNotedAt:(NSDate*)value;





- (Collectible*)primitiveNoteAbout;
- (void)setPrimitiveNoteAbout:(Collectible*)value;



- (NSMutableSet*)primitiveNoteUsers;
- (void)setPrimitiveNoteUsers:(NSMutableSet*)value;


@end
