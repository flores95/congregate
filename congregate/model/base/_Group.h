// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Group.h instead.

#import <CoreData/CoreData.h>
#import "Collectible.h"

extern const struct GroupAttributes {
	__unsafe_unretained NSString *endDate;
	__unsafe_unretained NSString *endTime;
	__unsafe_unretained NSString *maxMembers;
	__unsafe_unretained NSString *recurring;
	__unsafe_unretained NSString *startDate;
	__unsafe_unretained NSString *startTime;
	__unsafe_unretained NSString *type;
} GroupAttributes;

extern const struct GroupRelationships {
	__unsafe_unretained NSString *attendance;
	__unsafe_unretained NSString *leaders;
	__unsafe_unretained NSString *location;
	__unsafe_unretained NSString *members;
} GroupRelationships;

extern const struct GroupFetchedProperties {
} GroupFetchedProperties;

@class Attendance;
@class Person;
@class Location;
@class Person;









@interface GroupID : NSManagedObjectID {}
@end

@interface _Group : Collectible {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (GroupID*)objectID;





@property (nonatomic, strong) NSDate* endDate;



//- (BOOL)validateEndDate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* endTime;



//- (BOOL)validateEndTime:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* maxMembers;



@property int16_t maxMembersValue;
- (int16_t)maxMembersValue;
- (void)setMaxMembersValue:(int16_t)value_;

//- (BOOL)validateMaxMembers:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* recurring;



@property BOOL recurringValue;
- (BOOL)recurringValue;
- (void)setRecurringValue:(BOOL)value_;

//- (BOOL)validateRecurring:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* startDate;



//- (BOOL)validateStartDate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* startTime;



//- (BOOL)validateStartTime:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* type;



//- (BOOL)validateType:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *attendance;

- (NSMutableSet*)attendanceSet;




@property (nonatomic, strong) NSSet *leaders;

- (NSMutableSet*)leadersSet;




@property (nonatomic, strong) Location *location;

//- (BOOL)validateLocation:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSSet *members;

- (NSMutableSet*)membersSet;





@end

@interface _Group (CoreDataGeneratedAccessors)

- (void)addAttendance:(NSSet*)value_;
- (void)removeAttendance:(NSSet*)value_;
- (void)addAttendanceObject:(Attendance*)value_;
- (void)removeAttendanceObject:(Attendance*)value_;

- (void)addLeaders:(NSSet*)value_;
- (void)removeLeaders:(NSSet*)value_;
- (void)addLeadersObject:(Person*)value_;
- (void)removeLeadersObject:(Person*)value_;

- (void)addMembers:(NSSet*)value_;
- (void)removeMembers:(NSSet*)value_;
- (void)addMembersObject:(Person*)value_;
- (void)removeMembersObject:(Person*)value_;

@end

@interface _Group (CoreDataGeneratedPrimitiveAccessors)


- (NSDate*)primitiveEndDate;
- (void)setPrimitiveEndDate:(NSDate*)value;




- (NSDate*)primitiveEndTime;
- (void)setPrimitiveEndTime:(NSDate*)value;




- (NSNumber*)primitiveMaxMembers;
- (void)setPrimitiveMaxMembers:(NSNumber*)value;

- (int16_t)primitiveMaxMembersValue;
- (void)setPrimitiveMaxMembersValue:(int16_t)value_;




- (NSNumber*)primitiveRecurring;
- (void)setPrimitiveRecurring:(NSNumber*)value;

- (BOOL)primitiveRecurringValue;
- (void)setPrimitiveRecurringValue:(BOOL)value_;




- (NSDate*)primitiveStartDate;
- (void)setPrimitiveStartDate:(NSDate*)value;




- (NSDate*)primitiveStartTime;
- (void)setPrimitiveStartTime:(NSDate*)value;




- (NSString*)primitiveType;
- (void)setPrimitiveType:(NSString*)value;





- (NSMutableSet*)primitiveAttendance;
- (void)setPrimitiveAttendance:(NSMutableSet*)value;



- (NSMutableSet*)primitiveLeaders;
- (void)setPrimitiveLeaders:(NSMutableSet*)value;



- (Location*)primitiveLocation;
- (void)setPrimitiveLocation:(Location*)value;



- (NSMutableSet*)primitiveMembers;
- (void)setPrimitiveMembers:(NSMutableSet*)value;


@end
