// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to GivingBatch.m instead.

#import "_GivingBatch.h"

const struct GivingBatchAttributes GivingBatchAttributes = {
	.givingDate = @"givingDate",
	.name = @"name",
	.open = @"open",
};

const struct GivingBatchRelationships GivingBatchRelationships = {
	.donations = @"donations",
};

const struct GivingBatchFetchedProperties GivingBatchFetchedProperties = {
};

@implementation GivingBatchID
@end

@implementation _GivingBatch

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"GivingBatch" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"GivingBatch";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"GivingBatch" inManagedObjectContext:moc_];
}

- (GivingBatchID*)objectID {
	return (GivingBatchID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"openValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"open"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic givingDate;






@dynamic name;






@dynamic open;



- (BOOL)openValue {
	NSNumber *result = [self open];
	return [result boolValue];
}

- (void)setOpenValue:(BOOL)value_ {
	[self setOpen:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveOpenValue {
	NSNumber *result = [self primitiveOpen];
	return [result boolValue];
}

- (void)setPrimitiveOpenValue:(BOOL)value_ {
	[self setPrimitiveOpen:[NSNumber numberWithBool:value_]];
}





@dynamic donations;

	
- (NSMutableSet*)donationsSet {
	[self willAccessValueForKey:@"donations"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"donations"];
  
	[self didAccessValueForKey:@"donations"];
	return result;
}
	






@end
