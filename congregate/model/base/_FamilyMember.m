// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to FamilyMember.m instead.

#import "_FamilyMember.h"

const struct FamilyMemberAttributes FamilyMemberAttributes = {
	.head = @"head",
	.role = @"role",
};

const struct FamilyMemberRelationships FamilyMemberRelationships = {
	.family = @"family",
	.personDetail = @"personDetail",
};

const struct FamilyMemberFetchedProperties FamilyMemberFetchedProperties = {
};

@implementation FamilyMemberID
@end

@implementation _FamilyMember

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"FamilyMember" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"FamilyMember";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"FamilyMember" inManagedObjectContext:moc_];
}

- (FamilyMemberID*)objectID {
	return (FamilyMemberID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"headValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"head"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic head;



- (BOOL)headValue {
	NSNumber *result = [self head];
	return [result boolValue];
}

- (void)setHeadValue:(BOOL)value_ {
	[self setHead:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveHeadValue {
	NSNumber *result = [self primitiveHead];
	return [result boolValue];
}

- (void)setPrimitiveHeadValue:(BOOL)value_ {
	[self setPrimitiveHead:[NSNumber numberWithBool:value_]];
}





@dynamic role;






@dynamic family;

	

@dynamic personDetail;

	






@end
