// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Collectible.m instead.

#import "_Collectible.h"

const struct CollectibleAttributes CollectibleAttributes = {
	.active = @"active",
	.imageURL = @"imageURL",
	.name = @"name",
};

const struct CollectibleRelationships CollectibleRelationships = {
	.collections = @"collections",
	.notes = @"notes",
	.tags = @"tags",
};

const struct CollectibleFetchedProperties CollectibleFetchedProperties = {
};

@implementation CollectibleID
@end

@implementation _Collectible

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Collectible" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Collectible";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Collectible" inManagedObjectContext:moc_];
}

- (CollectibleID*)objectID {
	return (CollectibleID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"activeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"active"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic active;



- (BOOL)activeValue {
	NSNumber *result = [self active];
	return [result boolValue];
}

- (void)setActiveValue:(BOOL)value_ {
	[self setActive:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveActiveValue {
	NSNumber *result = [self primitiveActive];
	return [result boolValue];
}

- (void)setPrimitiveActiveValue:(BOOL)value_ {
	[self setPrimitiveActive:[NSNumber numberWithBool:value_]];
}





@dynamic imageURL;






@dynamic name;






@dynamic collections;

	
- (NSMutableSet*)collectionsSet {
	[self willAccessValueForKey:@"collections"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"collections"];
  
	[self didAccessValueForKey:@"collections"];
	return result;
}
	

@dynamic notes;

	
- (NSMutableSet*)notesSet {
	[self willAccessValueForKey:@"notes"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"notes"];
  
	[self didAccessValueForKey:@"notes"];
	return result;
}
	

@dynamic tags;

	
- (NSMutableSet*)tagsSet {
	[self willAccessValueForKey:@"tags"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"tags"];
  
	[self didAccessValueForKey:@"tags"];
	return result;
}
	






@end
