// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Person.m instead.

#import "_Person.h"

const struct PersonAttributes PersonAttributes = {
	.adult = @"adult",
	.birthday = @"birthday",
	.churchStatus = @"churchStatus",
	.firstName = @"firstName",
	.gender = @"gender",
	.grade = @"grade",
	.gradeAdjust = @"gradeAdjust",
	.lastName = @"lastName",
	.married = @"married",
	.member = @"member",
	.middleName = @"middleName",
	.nickName = @"nickName",
	.school = @"school",
	.specialNeeds = @"specialNeeds",
	.status = @"status",
	.suffix = @"suffix",
	.title = @"title",
};

const struct PersonRelationships PersonRelationships = {
	.attendance = @"attendance",
	.emailAddresses = @"emailAddresses",
	.family = @"family",
	.leading = @"leading",
	.mailingAddresses = @"mailingAddresses",
	.memberOf = @"memberOf",
	.phoneNumbers = @"phoneNumbers",
	.significantDates = @"significantDates",
	.socialAddresses = @"socialAddresses",
	.userAccount = @"userAccount",
};

const struct PersonFetchedProperties PersonFetchedProperties = {
};

@implementation PersonID
@end

@implementation _Person

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Person" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Person";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Person" inManagedObjectContext:moc_];
}

- (PersonID*)objectID {
	return (PersonID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"adultValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"adult"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"gradeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"grade"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"gradeAdjustValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"gradeAdjust"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"marriedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"married"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"memberValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"member"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic adult;



- (BOOL)adultValue {
	NSNumber *result = [self adult];
	return [result boolValue];
}

- (void)setAdultValue:(BOOL)value_ {
	[self setAdult:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveAdultValue {
	NSNumber *result = [self primitiveAdult];
	return [result boolValue];
}

- (void)setPrimitiveAdultValue:(BOOL)value_ {
	[self setPrimitiveAdult:[NSNumber numberWithBool:value_]];
}





@dynamic birthday;






@dynamic churchStatus;






@dynamic firstName;






@dynamic gender;






@dynamic grade;



- (int16_t)gradeValue {
	NSNumber *result = [self grade];
	return [result shortValue];
}

- (void)setGradeValue:(int16_t)value_ {
	[self setGrade:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveGradeValue {
	NSNumber *result = [self primitiveGrade];
	return [result shortValue];
}

- (void)setPrimitiveGradeValue:(int16_t)value_ {
	[self setPrimitiveGrade:[NSNumber numberWithShort:value_]];
}





@dynamic gradeAdjust;



- (int16_t)gradeAdjustValue {
	NSNumber *result = [self gradeAdjust];
	return [result shortValue];
}

- (void)setGradeAdjustValue:(int16_t)value_ {
	[self setGradeAdjust:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveGradeAdjustValue {
	NSNumber *result = [self primitiveGradeAdjust];
	return [result shortValue];
}

- (void)setPrimitiveGradeAdjustValue:(int16_t)value_ {
	[self setPrimitiveGradeAdjust:[NSNumber numberWithShort:value_]];
}





@dynamic lastName;






@dynamic married;



- (BOOL)marriedValue {
	NSNumber *result = [self married];
	return [result boolValue];
}

- (void)setMarriedValue:(BOOL)value_ {
	[self setMarried:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveMarriedValue {
	NSNumber *result = [self primitiveMarried];
	return [result boolValue];
}

- (void)setPrimitiveMarriedValue:(BOOL)value_ {
	[self setPrimitiveMarried:[NSNumber numberWithBool:value_]];
}





@dynamic member;



- (BOOL)memberValue {
	NSNumber *result = [self member];
	return [result boolValue];
}

- (void)setMemberValue:(BOOL)value_ {
	[self setMember:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveMemberValue {
	NSNumber *result = [self primitiveMember];
	return [result boolValue];
}

- (void)setPrimitiveMemberValue:(BOOL)value_ {
	[self setPrimitiveMember:[NSNumber numberWithBool:value_]];
}





@dynamic middleName;






@dynamic nickName;






@dynamic school;






@dynamic specialNeeds;






@dynamic status;






@dynamic suffix;






@dynamic title;






@dynamic attendance;

	
- (NSMutableSet*)attendanceSet {
	[self willAccessValueForKey:@"attendance"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"attendance"];
  
	[self didAccessValueForKey:@"attendance"];
	return result;
}
	

@dynamic emailAddresses;

	
- (NSMutableSet*)emailAddressesSet {
	[self willAccessValueForKey:@"emailAddresses"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"emailAddresses"];
  
	[self didAccessValueForKey:@"emailAddresses"];
	return result;
}
	

@dynamic family;

	

@dynamic leading;

	
- (NSMutableSet*)leadingSet {
	[self willAccessValueForKey:@"leading"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"leading"];
  
	[self didAccessValueForKey:@"leading"];
	return result;
}
	

@dynamic mailingAddresses;

	
- (NSMutableSet*)mailingAddressesSet {
	[self willAccessValueForKey:@"mailingAddresses"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"mailingAddresses"];
  
	[self didAccessValueForKey:@"mailingAddresses"];
	return result;
}
	

@dynamic memberOf;

	
- (NSMutableSet*)memberOfSet {
	[self willAccessValueForKey:@"memberOf"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"memberOf"];
  
	[self didAccessValueForKey:@"memberOf"];
	return result;
}
	

@dynamic phoneNumbers;

	
- (NSMutableSet*)phoneNumbersSet {
	[self willAccessValueForKey:@"phoneNumbers"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"phoneNumbers"];
  
	[self didAccessValueForKey:@"phoneNumbers"];
	return result;
}
	

@dynamic significantDates;

	
- (NSMutableSet*)significantDatesSet {
	[self willAccessValueForKey:@"significantDates"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"significantDates"];
  
	[self didAccessValueForKey:@"significantDates"];
	return result;
}
	

@dynamic socialAddresses;

	
- (NSMutableSet*)socialAddressesSet {
	[self willAccessValueForKey:@"socialAddresses"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"socialAddresses"];
  
	[self didAccessValueForKey:@"socialAddresses"];
	return result;
}
	

@dynamic userAccount;

	






@end
