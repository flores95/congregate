// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Donor.h instead.

#import <CoreData/CoreData.h>
#import "Collectible.h"

extern const struct DonorAttributes {
} DonorAttributes;

extern const struct DonorRelationships {
	__unsafe_unretained NSString *donations;
} DonorRelationships;

extern const struct DonorFetchedProperties {
} DonorFetchedProperties;

@class Donation;


@interface DonorID : NSManagedObjectID {}
@end

@interface _Donor : Collectible {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (DonorID*)objectID;





@property (nonatomic, strong) NSSet *donations;

- (NSMutableSet*)donationsSet;





@end

@interface _Donor (CoreDataGeneratedAccessors)

- (void)addDonations:(NSSet*)value_;
- (void)removeDonations:(NSSet*)value_;
- (void)addDonationsObject:(Donation*)value_;
- (void)removeDonationsObject:(Donation*)value_;

@end

@interface _Donor (CoreDataGeneratedPrimitiveAccessors)



- (NSMutableSet*)primitiveDonations;
- (void)setPrimitiveDonations:(NSMutableSet*)value;


@end
