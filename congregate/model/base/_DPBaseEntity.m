// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to DPBaseEntity.m instead.

#import "_DPBaseEntity.h"

const struct DPBaseEntityAttributes DPBaseEntityAttributes = {
	.createdBy = @"createdBy",
	.modifiedBy = @"modifiedBy",
};

const struct DPBaseEntityRelationships DPBaseEntityRelationships = {
};

const struct DPBaseEntityFetchedProperties DPBaseEntityFetchedProperties = {
};

@implementation DPBaseEntityID
@end

@implementation _DPBaseEntity

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"DPBaseEntity" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"DPBaseEntity";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"DPBaseEntity" inManagedObjectContext:moc_];
}

- (DPBaseEntityID*)objectID {
	return (DPBaseEntityID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic createdBy;






@dynamic modifiedBy;











@end
