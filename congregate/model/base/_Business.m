// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Business.m instead.

#import "_Business.h"

const struct BusinessAttributes BusinessAttributes = {
	.address = @"address",
	.city = @"city",
	.phone = @"phone",
	.postalCode = @"postalCode",
	.state = @"state",
};

const struct BusinessRelationships BusinessRelationships = {
};

const struct BusinessFetchedProperties BusinessFetchedProperties = {
};

@implementation BusinessID
@end

@implementation _Business

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Business" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Business";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Business" inManagedObjectContext:moc_];
}

- (BusinessID*)objectID {
	return (BusinessID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic address;






@dynamic city;






@dynamic phone;






@dynamic postalCode;






@dynamic state;











@end
