// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Fund.m instead.

#import "_Fund.h"

const struct FundAttributes FundAttributes = {
	.desc = @"desc",
	.internalName = @"internalName",
	.name = @"name",
};

const struct FundRelationships FundRelationships = {
	.donations = @"donations",
};

const struct FundFetchedProperties FundFetchedProperties = {
};

@implementation FundID
@end

@implementation _Fund

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Fund" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Fund";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Fund" inManagedObjectContext:moc_];
}

- (FundID*)objectID {
	return (FundID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic desc;






@dynamic internalName;






@dynamic name;






@dynamic donations;

	
- (NSMutableSet*)donationsSet {
	[self willAccessValueForKey:@"donations"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"donations"];
  
	[self didAccessValueForKey:@"donations"];
	return result;
}
	






@end
