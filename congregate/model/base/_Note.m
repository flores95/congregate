// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Note.m instead.

#import "_Note.h"

const struct NoteAttributes NoteAttributes = {
	.noteText = @"noteText",
	.notedAt = @"notedAt",
};

const struct NoteRelationships NoteRelationships = {
	.noteAbout = @"noteAbout",
	.noteUsers = @"noteUsers",
};

const struct NoteFetchedProperties NoteFetchedProperties = {
};

@implementation NoteID
@end

@implementation _Note

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Note" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Note";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Note" inManagedObjectContext:moc_];
}

- (NoteID*)objectID {
	return (NoteID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic noteText;






@dynamic notedAt;






@dynamic noteAbout;

	

@dynamic noteUsers;

	
- (NSMutableSet*)noteUsersSet {
	[self willAccessValueForKey:@"noteUsers"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"noteUsers"];
  
	[self didAccessValueForKey:@"noteUsers"];
	return result;
}
	






@end
