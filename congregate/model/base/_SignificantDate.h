// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to SignificantDate.h instead.

#import <CoreData/CoreData.h>
#import "DPBaseEntity.h"

extern const struct SignificantDateAttributes {
	__unsafe_unretained NSString *eventDate;
	__unsafe_unretained NSString *eventDescription;
	__unsafe_unretained NSString *eventTitle;
} SignificantDateAttributes;

extern const struct SignificantDateRelationships {
	__unsafe_unretained NSString *person;
} SignificantDateRelationships;

extern const struct SignificantDateFetchedProperties {
} SignificantDateFetchedProperties;

@class Person;





@interface SignificantDateID : NSManagedObjectID {}
@end

@interface _SignificantDate : DPBaseEntity {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (SignificantDateID*)objectID;





@property (nonatomic, strong) NSDate* eventDate;



//- (BOOL)validateEventDate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* eventDescription;



//- (BOOL)validateEventDescription:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* eventTitle;



//- (BOOL)validateEventTitle:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) Person *person;

//- (BOOL)validatePerson:(id*)value_ error:(NSError**)error_;





@end

@interface _SignificantDate (CoreDataGeneratedAccessors)

@end

@interface _SignificantDate (CoreDataGeneratedPrimitiveAccessors)


- (NSDate*)primitiveEventDate;
- (void)setPrimitiveEventDate:(NSDate*)value;




- (NSString*)primitiveEventDescription;
- (void)setPrimitiveEventDescription:(NSString*)value;




- (NSString*)primitiveEventTitle;
- (void)setPrimitiveEventTitle:(NSString*)value;





- (Person*)primitivePerson;
- (void)setPrimitivePerson:(Person*)value;


@end
