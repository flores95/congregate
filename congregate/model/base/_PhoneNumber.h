// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to PhoneNumber.h instead.

#import <CoreData/CoreData.h>
#import "ContactMethod.h"

extern const struct PhoneNumberAttributes {
	__unsafe_unretained NSString *canText;
	__unsafe_unretained NSString *countryCode;
	__unsafe_unretained NSString *number;
} PhoneNumberAttributes;

extern const struct PhoneNumberRelationships {
	__unsafe_unretained NSString *person;
} PhoneNumberRelationships;

extern const struct PhoneNumberFetchedProperties {
} PhoneNumberFetchedProperties;

@class Person;





@interface PhoneNumberID : NSManagedObjectID {}
@end

@interface _PhoneNumber : ContactMethod {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (PhoneNumberID*)objectID;





@property (nonatomic, strong) NSNumber* canText;



@property BOOL canTextValue;
- (BOOL)canTextValue;
- (void)setCanTextValue:(BOOL)value_;

//- (BOOL)validateCanText:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* countryCode;



//- (BOOL)validateCountryCode:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* number;



//- (BOOL)validateNumber:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) Person *person;

//- (BOOL)validatePerson:(id*)value_ error:(NSError**)error_;





@end

@interface _PhoneNumber (CoreDataGeneratedAccessors)

@end

@interface _PhoneNumber (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveCanText;
- (void)setPrimitiveCanText:(NSNumber*)value;

- (BOOL)primitiveCanTextValue;
- (void)setPrimitiveCanTextValue:(BOOL)value_;




- (NSString*)primitiveCountryCode;
- (void)setPrimitiveCountryCode:(NSString*)value;




- (NSString*)primitiveNumber;
- (void)setPrimitiveNumber:(NSString*)value;





- (Person*)primitivePerson;
- (void)setPrimitivePerson:(Person*)value;


@end
