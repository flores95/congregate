// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Family.h instead.

#import <CoreData/CoreData.h>
#import "Collectible.h"

extern const struct FamilyAttributes {
	__unsafe_unretained NSString *lastName;
} FamilyAttributes;

extern const struct FamilyRelationships {
	__unsafe_unretained NSString *members;
} FamilyRelationships;

extern const struct FamilyFetchedProperties {
} FamilyFetchedProperties;

@class FamilyMember;



@interface FamilyID : NSManagedObjectID {}
@end

@interface _Family : Collectible {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (FamilyID*)objectID;





@property (nonatomic, strong) NSString* lastName;



//- (BOOL)validateLastName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *members;

- (NSMutableSet*)membersSet;





@end

@interface _Family (CoreDataGeneratedAccessors)

- (void)addMembers:(NSSet*)value_;
- (void)removeMembers:(NSSet*)value_;
- (void)addMembersObject:(FamilyMember*)value_;
- (void)removeMembersObject:(FamilyMember*)value_;

@end

@interface _Family (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveLastName;
- (void)setPrimitiveLastName:(NSString*)value;





- (NSMutableSet*)primitiveMembers;
- (void)setPrimitiveMembers:(NSMutableSet*)value;


@end
