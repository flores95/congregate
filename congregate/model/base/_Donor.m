// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Donor.m instead.

#import "_Donor.h"

const struct DonorAttributes DonorAttributes = {
};

const struct DonorRelationships DonorRelationships = {
	.donations = @"donations",
};

const struct DonorFetchedProperties DonorFetchedProperties = {
};

@implementation DonorID
@end

@implementation _Donor

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Donor" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Donor";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Donor" inManagedObjectContext:moc_];
}

- (DonorID*)objectID {
	return (DonorID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic donations;

	
- (NSMutableSet*)donationsSet {
	[self willAccessValueForKey:@"donations"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"donations"];
  
	[self didAccessValueForKey:@"donations"];
	return result;
}
	






@end
