// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to SocialAddress.h instead.

#import <CoreData/CoreData.h>
#import "ContactMethod.h"

extern const struct SocialAddressAttributes {
	__unsafe_unretained NSString *networkName;
	__unsafe_unretained NSString *url;
	__unsafe_unretained NSString *userName;
} SocialAddressAttributes;

extern const struct SocialAddressRelationships {
	__unsafe_unretained NSString *person;
} SocialAddressRelationships;

extern const struct SocialAddressFetchedProperties {
} SocialAddressFetchedProperties;

@class Person;





@interface SocialAddressID : NSManagedObjectID {}
@end

@interface _SocialAddress : ContactMethod {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (SocialAddressID*)objectID;





@property (nonatomic, strong) NSString* networkName;



//- (BOOL)validateNetworkName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* url;



//- (BOOL)validateUrl:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* userName;



//- (BOOL)validateUserName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) Person *person;

//- (BOOL)validatePerson:(id*)value_ error:(NSError**)error_;





@end

@interface _SocialAddress (CoreDataGeneratedAccessors)

@end

@interface _SocialAddress (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveNetworkName;
- (void)setPrimitiveNetworkName:(NSString*)value;




- (NSString*)primitiveUrl;
- (void)setPrimitiveUrl:(NSString*)value;




- (NSString*)primitiveUserName;
- (void)setPrimitiveUserName:(NSString*)value;





- (Person*)primitivePerson;
- (void)setPrimitivePerson:(Person*)value;


@end
