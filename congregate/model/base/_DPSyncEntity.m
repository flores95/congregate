// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to DPSyncEntity.m instead.

#import "_DPSyncEntity.h"

const struct DPSyncEntityAttributes DPSyncEntityAttributes = {
	.createdAt = @"createdAt",
	.deleted = @"deleted",
	.objectId = @"objectId",
	.sourceCreatedAt = @"sourceCreatedAt",
	.sourceObjectId = @"sourceObjectId",
	.sourceUpdatedAt = @"sourceUpdatedAt",
	.updatedAt = @"updatedAt",
};

const struct DPSyncEntityRelationships DPSyncEntityRelationships = {
};

const struct DPSyncEntityFetchedProperties DPSyncEntityFetchedProperties = {
};

@implementation DPSyncEntityID
@end

@implementation _DPSyncEntity

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"DPSyncEntity" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"DPSyncEntity";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"DPSyncEntity" inManagedObjectContext:moc_];
}

- (DPSyncEntityID*)objectID {
	return (DPSyncEntityID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"deletedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"deleted"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic createdAt;






@dynamic deleted;



- (BOOL)deletedValue {
	NSNumber *result = [self deleted];
	return [result boolValue];
}

- (void)setDeletedValue:(BOOL)value_ {
	[self setDeleted:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveDeletedValue {
	NSNumber *result = [self primitiveDeleted];
	return [result boolValue];
}

- (void)setPrimitiveDeletedValue:(BOOL)value_ {
	[self setPrimitiveDeleted:[NSNumber numberWithBool:value_]];
}





@dynamic objectId;






@dynamic sourceCreatedAt;






@dynamic sourceObjectId;






@dynamic sourceUpdatedAt;






@dynamic updatedAt;











@end
