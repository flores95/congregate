// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to SignificantDate.m instead.

#import "_SignificantDate.h"

const struct SignificantDateAttributes SignificantDateAttributes = {
	.eventDate = @"eventDate",
	.eventDescription = @"eventDescription",
	.eventTitle = @"eventTitle",
};

const struct SignificantDateRelationships SignificantDateRelationships = {
	.person = @"person",
};

const struct SignificantDateFetchedProperties SignificantDateFetchedProperties = {
};

@implementation SignificantDateID
@end

@implementation _SignificantDate

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"SignificantDate" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"SignificantDate";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"SignificantDate" inManagedObjectContext:moc_];
}

- (SignificantDateID*)objectID {
	return (SignificantDateID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic eventDate;






@dynamic eventDescription;






@dynamic eventTitle;






@dynamic person;

	






@end
