// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Collectible.h instead.

#import <CoreData/CoreData.h>
#import "DPBaseEntity.h"

extern const struct CollectibleAttributes {
	__unsafe_unretained NSString *active;
	__unsafe_unretained NSString *imageURL;
	__unsafe_unretained NSString *name;
} CollectibleAttributes;

extern const struct CollectibleRelationships {
	__unsafe_unretained NSString *collections;
	__unsafe_unretained NSString *notes;
	__unsafe_unretained NSString *tags;
} CollectibleRelationships;

extern const struct CollectibleFetchedProperties {
} CollectibleFetchedProperties;

@class ItemCollection;
@class Note;
@class Tag;





@interface CollectibleID : NSManagedObjectID {}
@end

@interface _Collectible : DPBaseEntity {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (CollectibleID*)objectID;





@property (nonatomic, strong) NSNumber* active;



@property BOOL activeValue;
- (BOOL)activeValue;
- (void)setActiveValue:(BOOL)value_;

//- (BOOL)validateActive:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* imageURL;



//- (BOOL)validateImageURL:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *collections;

- (NSMutableSet*)collectionsSet;




@property (nonatomic, strong) NSSet *notes;

- (NSMutableSet*)notesSet;




@property (nonatomic, strong) NSSet *tags;

- (NSMutableSet*)tagsSet;





@end

@interface _Collectible (CoreDataGeneratedAccessors)

- (void)addCollections:(NSSet*)value_;
- (void)removeCollections:(NSSet*)value_;
- (void)addCollectionsObject:(ItemCollection*)value_;
- (void)removeCollectionsObject:(ItemCollection*)value_;

- (void)addNotes:(NSSet*)value_;
- (void)removeNotes:(NSSet*)value_;
- (void)addNotesObject:(Note*)value_;
- (void)removeNotesObject:(Note*)value_;

- (void)addTags:(NSSet*)value_;
- (void)removeTags:(NSSet*)value_;
- (void)addTagsObject:(Tag*)value_;
- (void)removeTagsObject:(Tag*)value_;

@end

@interface _Collectible (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveActive;
- (void)setPrimitiveActive:(NSNumber*)value;

- (BOOL)primitiveActiveValue;
- (void)setPrimitiveActiveValue:(BOOL)value_;




- (NSString*)primitiveImageURL;
- (void)setPrimitiveImageURL:(NSString*)value;




- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;





- (NSMutableSet*)primitiveCollections;
- (void)setPrimitiveCollections:(NSMutableSet*)value;



- (NSMutableSet*)primitiveNotes;
- (void)setPrimitiveNotes:(NSMutableSet*)value;



- (NSMutableSet*)primitiveTags;
- (void)setPrimitiveTags:(NSMutableSet*)value;


@end
