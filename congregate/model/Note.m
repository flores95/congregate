#import "Note.h"


@interface Note ()

// Private interface goes here.

@end


@implementation Note

-(BOOL)validateEntity:(NSError **)error
{
    BOOL retVal = YES;
    NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
    
    NSMutableString *errorDesc = [[NSMutableString alloc] init];
    
    if (![self notedAt])
    {
        retVal = NO;
        [errorDesc appendString:@"* Date Required\n"];
    }
    
    if (![self noteText])
    {
        retVal = NO;
        [errorDesc appendString:@"* Note Required\n"];
    }
    
    if (![self noteAbout])
    {
        retVal = NO;
        [errorDesc appendString:@"* Note About Required\n"];
    }
    
    [errorDetail setValue:errorDesc forKey:NSLocalizedDescriptionKey];
    *error = [NSError errorWithDomain:@"myDomain" code:100 userInfo:errorDetail];
    
    return retVal;
}
@end
