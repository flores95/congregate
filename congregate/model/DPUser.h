#import "_DPUser.h"

@interface DPUser : _DPUser {}

+ (id)findUser:(NSString *)username InContext:(NSManagedObjectContext *)context;
- (bool)authenticatePassword:(NSString *)password;

@end
