#import "Collectible.h"


@interface Collectible ()

// Private interface goes here.

@end


@implementation Collectible

- (void)setCollectibleType:(NSString *)collectibleType
{
    // do nothing it's a calculated field
}
- (NSString *)collectibleType
{

    return [[self entity] name];
}


@end
