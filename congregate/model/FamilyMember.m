#import "FamilyMember.h"
#import "Family.h"


@interface FamilyMember ()

// Private interface goes here.

@end


@implementation FamilyMember

- (id)initWithEntity:(NSEntityDescription *)entity insertIntoManagedObjectContext:(NSManagedObjectContext *)context
{
    self = [super initWithEntity:entity insertIntoManagedObjectContext:context];
    
    [self addObserver:self forKeyPath:@"head"       options:NSKeyValueObservingOptionNew context:NULL];
    
    return self;
}

#pragma mark - Observation
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"head"])
    {
        // only one head so unset others if necessary
        if ([self.head boolValue])
        {
            for (FamilyMember *member in [[self family] members])
            {
                if ([[member head] boolValue] && ![member isEqual:self])
                {
                    [member setHeadValue:NO];
                }
            }
            [[self family] setHeadOfFamily:nil]; // trigger the calculated field update;
        }
    }
}

- (void)dealloc
{
    [self removeObserver:self forKeyPath:@"head"];
}

@end
