#import "Group.h"


@interface Group ()

// Private interface goes here.

@end


@implementation Group

- (BOOL)validateEntity:(NSError *__autoreleasing *)error
{
    BOOL retVal = YES;
    NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
    
    NSMutableString *errorDesc = [[NSMutableString alloc] init];

    if (![self name])
    {
        retVal = NO;
        [errorDesc appendString:@"Name Required\n"];
    }
    
    if ([self startDate] && [self endDate])
    {
        if ([self startDate] >= [self endDate])
        {
            retVal = NO;
            [errorDesc appendString:@"Start date later than end date.\n"];         
        }
    }
    
    /*
    if ([self startTime] && [self endTime])
    {
        if ([self startTime] >= [self endTime])
        {
            retVal = NO;
            [errorDesc appendString:@"Start time later than end time.\n"];
        }
    }
    */
    
    [errorDetail setValue:errorDesc forKey:NSLocalizedDescriptionKey];
    *error = [NSError errorWithDomain:@"CONGREGATE" code:100 userInfo:errorDetail];
    
    return retVal;
}

@end
