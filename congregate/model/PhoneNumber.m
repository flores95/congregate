#import "PhoneNumber.h"
#import "Person.h"


@interface PhoneNumber ()

// Private interface goes here.

@end


@implementation PhoneNumber

- (id)initWithEntity:(NSEntityDescription *)entity insertIntoManagedObjectContext:(NSManagedObjectContext *)context
{
    self = [super initWithEntity:entity insertIntoManagedObjectContext:context];
    
    [self addObserver:self forKeyPath:@"primary"            options:NSKeyValueObservingOptionNew context:NULL];
    
    return self;
}

#pragma mark - Observation

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"primary"])
    {
        // only one primary so unset others if necessary
        if ([self.primary boolValue])
        {
            for (PhoneNumber *num in [[self person] phoneNumbers])
            {
                if ([[num primary] boolValue] && ![num isEqual:self])
                {
                    [num setPrimaryValue:NO];
                }
            }
            [[self person] setPrimaryPhone:nil]; // trigger the calculated field update;
        }
    }
}

- (void)dealloc
{

    [self removeObserver:self forKeyPath:@"primary"];
}

#pragma mark - Validation

-(BOOL)validateEntity:(NSError **)error
{
    BOOL retVal = YES;
    NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
    
    NSMutableString *errorDesc = [[NSMutableString alloc] init];
    if (![self label])
    {
        retVal = NO;
        [errorDesc appendString:@"* Label Required\n"];
    }
    
    if (![self number])
    {
        retVal = NO;
        [errorDesc appendString:@"* Phone Number Required\n"];
    }
    [errorDetail setValue:errorDesc forKey:NSLocalizedDescriptionKey];
    *error = [NSError errorWithDomain:@"myDomain" code:100 userInfo:errorDetail];
    
    return retVal;
}

@end
