#import "_MailingAddress.h"

@interface MailingAddress : _MailingAddress {}

@property (strong, nonatomic) NSString *fullAddress;

@end
