#import "MailingAddress.h"
#import "Person.h"


@interface MailingAddress ()

// Private interface goes here.

@end


@implementation MailingAddress

- (id)initWithEntity:(NSEntityDescription *)entity insertIntoManagedObjectContext:(NSManagedObjectContext *)context
{
    self = [super initWithEntity:entity insertIntoManagedObjectContext:context];
    
    [self addObserver:self forKeyPath:@"addressLine1"       options:NSKeyValueObservingOptionNew context:NULL];
    [self addObserver:self forKeyPath:@"addressLine2"       options:NSKeyValueObservingOptionNew context:NULL];
    [self addObserver:self forKeyPath:@"city"               options:NSKeyValueObservingOptionNew context:NULL];
    [self addObserver:self forKeyPath:@"state"              options:NSKeyValueObservingOptionNew context:NULL];
    [self addObserver:self forKeyPath:@"postalCode"         options:NSKeyValueObservingOptionNew context:NULL];
    [self addObserver:self forKeyPath:@"primary"            options:NSKeyValueObservingOptionNew context:NULL];
    
    return self;
}

#pragma mark - Observation
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"addressLine1"] ||
        [keyPath isEqualToString:@"addressLine2"] ||
        [keyPath isEqualToString:@"city"] ||
        [keyPath isEqualToString:@"state"] ||
        [keyPath isEqualToString:@"postalCode"] )
    {
        [self setFullAddress:nil]; // this should just refresh the calculated field;
    }
    
    if ([keyPath isEqualToString:@"primary"])
    {
        // only one primary so unset others if necessary
        if ([self.primary boolValue])
        {
            for (MailingAddress *addr in [[self person] mailingAddresses])
            {
                if ([[addr primary] boolValue] && ![addr isEqual:self])
                {
                    [addr setPrimaryValue:NO];
                }
            }
            [[self person] setPrimaryAddress:nil]; // trigger the calculated field update;
        }
    }

}

- (void)dealloc
{
    [self removeObserver:self forKeyPath:@"addressLine1"];
    [self removeObserver:self forKeyPath:@"addressLine2"];
    [self removeObserver:self forKeyPath:@"city"];
    [self removeObserver:self forKeyPath:@"state"];
    [self removeObserver:self forKeyPath:@"postalCode"];
    [self removeObserver:self forKeyPath:@"primary"];
}

#pragma mark - Calculated Fields

- (void)setFullAddress:(NSString *)fullAddress
{
    // do nothing ... calculated field
}

-(NSString *)fullAddress
{

    NSString *addr = @"";
    if (self.addressLine1)
    {
        addr = [addr stringByAppendingFormat:@"%@\n", self.addressLine1];
    }
    if (self.addressLine2)
    {
        addr = [addr stringByAppendingFormat:@"%@\n", self.addressLine2];
    }
    if (self.city)
    {
        addr = [addr stringByAppendingFormat:@"%@", self.city];
    }
    if (self.state)
    {
        addr = [addr stringByAppendingFormat:@", %@", self.state];
    }
    if (self.postalCode)
    {
        addr = [addr stringByAppendingFormat:@"  %@", self.postalCode];
    }
    
    return [addr stringByReplacingOccurrencesOfString:@"\n\n" withString:@"\n"];
}

#pragma mark - Validation

-(BOOL)validateEntity:(NSError **)error
{
    BOOL retVal = YES;
    NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
    
    NSMutableString *errorDesc = [[NSMutableString alloc] init];
    
    if (![self label])
    {
        retVal = NO;
        [errorDesc appendString:@"* Label Required\n"];
    }
    
    if (![self addressLine1])
    {
        retVal = NO;
        [errorDesc appendString:@"* Address Line 1 Required\n"];
    }
    
    if (![self city])
    {
        retVal = NO;
        [errorDesc appendString:@"* City Required\n"];
    }

    if (![self state])
    {
        retVal = NO;
        [errorDesc appendString:@"* State Required\n"];
    }

    if (![self postalCode])
    {
        retVal = NO;
        [errorDesc appendString:@"* Postal Code Required\n"];
    }
    
    [errorDetail setValue:errorDesc forKey:NSLocalizedDescriptionKey];
    *error = [NSError errorWithDomain:@"myDomain" code:100 userInfo:errorDetail];
    
    return retVal;
}

@end
