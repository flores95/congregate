#import "_DPSyncEntity.h"
#import <ParseOSX/Parse.h>
#import "DPConstants.h"

@interface DPSyncEntity : _DPSyncEntity {}

@property (strong, nonatomic) PFObject *remoteObject;
@property (nonatomic) BOOL updatedFromRemote;
@property (nonatomic) BOOL updatingFromRemote;

+ (id) createInContext:(NSManagedObjectContext *)context;
+ (id) findFirstByAttribute:(NSString *)attribute WithValue:(id)value InContext:(NSManagedObjectContext *)context;
+ (NSArray *) findAllWithPredicate:(NSPredicate *)predicate InContext:(NSManagedObjectContext *)context;
+ (NSArray *) getUpdatedRemoteObjects;
+ (id) createOrUpdateFromRemoteObject:(PFObject *)remoteObj InContext:(NSManagedObjectContext *)context;
+ (NSString *)UUID;

- (void) addRelationshipsFromRemote:(PFObject *)remotObj;


@end
