#import "DPBaseEntity.h"

@interface DPBaseEntity ()

// Private interface goes here.

@end


@implementation DPBaseEntity

- (id) initWithEntity:(NSEntityDescription *)entity insertIntoManagedObjectContext:(NSManagedObjectContext *)context
{
    self = [super initWithEntity:entity insertIntoManagedObjectContext:context];
    return self;
}

+ (NSUInteger) countOfEntitiesWithPredicate:(NSPredicate *)predicate InContext:(NSManagedObjectContext *)context
{
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[self entityName]];
    [fetch setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *results = [context executeFetchRequest:fetch error:&error];
    
    if (results)
    {
        return [results count];
    } else {
        return 0;
    }

}

-(void)willSave
{
    if ([[self managedObjectContext] isEqual:[[NSApp delegate] managedObjectContext]])
    {
        NSDate *changeDate = [NSDate date];
        NSString *currentUsername = [[NSUserDefaults standardUserDefaults] valueForKey:kDPAppUserKey];
        if (!currentUsername)
        {
            currentUsername = @"_system";
        }
        
        if ([self isInserted])
        {
            if (![self sourceCreatedAt]) { [self setPrimitiveSourceCreatedAt:changeDate]; }
            if (![self createdBy]) { [self setPrimitiveCreatedBy:currentUsername]; }
        }
        if ([self isUpdated])
        {
            BOOL updated = NO;
            NSDictionary *changes = [self changedValues];
            for (NSString *key in [changes keyEnumerator])
            {
                NSArray *keysToIgnore = @[@"updatedAt", @"sourceUpdatedAt", @"createdBy", @"modifiedBy", @"createdAt", @"sourceCreatedAt"];
                if (![keysToIgnore containsObject:key])
                {
                    updated = YES;
                }
            }
            if (updated)
            {
                [self setPrimitiveSourceUpdatedAt:changeDate];
                [self setPrimitiveModifiedBy:currentUsername];                
            }
        }
    }
}

- (id)duplicateEntity
{
    // create a new entity
    id newEntity = [NSEntityDescription
                                    insertNewObjectForEntityForName:[[self entity] name]
                                    inManagedObjectContext:[self managedObjectContext]];
    
    // add all the attributes first
    NSDictionary *attrs = [[self entity] attributesByName];
    @try {
        //manually set the values so "nil" can be replaced with NSNull
        for (NSString *key in [attrs keyEnumerator])
        {
            id value = [self valueForKey:key];
            if (![key isEqualToString:@"objectId"] && ![key isEqualToString:@"sourceObjectId"]
                && ![key isEqualToString:@"updatedAt"] && ![key isEqualToString:@"sourceUpdatedAt"]
                && ![key isEqualToString:@"createdAt"] && ![key isEqualToString:@"sourceCreatedAt"])
            {
                [newEntity setValue:value forKey:key];
            }
        }
    }
    @catch (NSException *exception) {
        NSString *report = [NSString stringWithFormat:@" ~~~~~~~~~~ EXCEPTION DUPLICATING ENTITY %@", [exception description]];
        [[NSNotificationCenter defaultCenter] postNotificationName:kDPProcessNotification object:report];
    }
    @finally {
        return newEntity;
    }

}

-(BOOL)validateEntity:(NSError *__autoreleasing *)error
{
    return YES; // this should be overriden by sub classes
}

-(BOOL)deleteEntity:(NSError *__autoreleasing *)error
{
    @try {
        [[self managedObjectContext] deleteObject:self];
    }
    @catch (NSException *exception) {
        NSLog(@"ERROR DELETING ENTITY: %@", [exception description]);
    }
    @finally {
        return YES; // this should be overriden by sub classes
    }
}

@end
