#import "Donor.h"
#import "Person.h"
#import "Business.h"


@interface Donor ()

// Private interface goes here.

@end


@implementation Donor

- (void)setDisplayName:(NSString *)displayName
{
    //DO NOTHING IT'S NOT A PERMENANT ATTRIBUTE
}

- (NSString *)displayName
{
    NSString *returnValue = nil;
    if ([self isKindOfClass:[Person class]])
    {
        // basic lastname, firstname
        returnValue = [NSString stringWithFormat:@"%@, %@", [self valueForKey:@"lastName"], [self valueForKey:@"firstName"]];
        // add middle name if it exists
        if ([self valueForKey:@"middleName"])
        {
            returnValue = [NSString stringWithFormat:@"%@ %@", returnValue, [self valueForKey:@"middleName"]];
        }
        // add suffix if it exist
        if ([self valueForKey:@"suffix"])
        {
            returnValue = [NSString stringWithFormat:@"%@ %@", returnValue, [self valueForKey:@"suffix"]];
        }
    }
    
    if ([self isKindOfClass:[Business class]])
    {
        returnValue = [self name];
    }

    return returnValue;
}

@end
