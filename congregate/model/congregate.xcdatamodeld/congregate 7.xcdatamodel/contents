<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<model name="" userDefinedModelVersionIdentifier="" type="com.apple.IDECoreDataModeler.DataModel" documentVersion="1.0" lastSavedToolsVersion="1811" systemVersion="12C3104" minimumToolsVersion="Automatic" macOSVersion="Automatic" iOSVersion="Automatic">
    <entity name="Adult" representedClassName="Adult" parentEntity="Person" syncable="YES">
        <attribute name="anniversary" optional="YES" attributeType="Date" syncable="YES"/>
        <attribute name="backgroundCheck" optional="YES" attributeType="Date" syncable="YES"/>
        <attribute name="backgroundCheckNotes" optional="YES" attributeType="String" syncable="YES"/>
        <attribute name="directory" optional="YES" attributeType="Boolean" defaultValueString="YES" syncable="YES"/>
        <attribute name="married" optional="YES" attributeType="Boolean" defaultValueString="NO" syncable="YES"/>
        <attribute name="newsletter" optional="YES" attributeType="Boolean" defaultValueString="YES" syncable="YES"/>
        <attribute name="title" optional="YES" attributeType="String" syncable="YES"/>
        <relationship name="spouse" optional="YES" minCount="1" maxCount="1" deletionRule="Nullify" destinationEntity="Adult" inverseName="spouse" inverseEntity="Adult" syncable="YES"/>
    </entity>
    <entity name="Business" representedClassName="Business" parentEntity="Donor" syncable="YES">
        <attribute name="address" optional="YES" attributeType="String" syncable="YES"/>
        <attribute name="city" optional="YES" attributeType="String" syncable="YES"/>
        <attribute name="phone" optional="YES" attributeType="String" syncable="YES"/>
        <attribute name="postalCode" optional="YES" attributeType="String" syncable="YES"/>
        <attribute name="state" optional="YES" attributeType="String" syncable="YES"/>
    </entity>
    <entity name="Child" representedClassName="Child" parentEntity="Person" syncable="YES">
        <attribute name="alertNumber" optional="YES" attributeType="String" syncable="YES"/>
        <attribute name="grade" optional="YES" attributeType="Integer 16" defaultValueString="-5" syncable="YES"/>
        <attribute name="specialNeeds" optional="YES" attributeType="String" syncable="YES"/>
        <relationship name="school" optional="YES" minCount="1" maxCount="1" deletionRule="Nullify" destinationEntity="School" inverseName="students" inverseEntity="School" syncable="YES"/>
    </entity>
    <entity name="Collectible" representedClassName="Collectible" isAbstract="YES" parentEntity="CoreEntity" syncable="YES">
        <attribute name="name" attributeType="String" syncable="YES"/>
        <relationship name="collections" optional="YES" toMany="YES" deletionRule="Nullify" destinationEntity="ItemCollection" inverseName="items" inverseEntity="ItemCollection" syncable="YES"/>
        <relationship name="tags" optional="YES" toMany="YES" deletionRule="Nullify" destinationEntity="Tag" inverseName="items" inverseEntity="Tag" syncable="YES"/>
    </entity>
    <entity name="CoreEntity" representedClassName="CoreEntity" isAbstract="YES" syncable="YES">
        <attribute name="createdAt" optional="YES" attributeType="Date" syncable="YES"/>
        <attribute name="objectId" optional="YES" attributeType="String" syncable="YES"/>
        <attribute name="syncStatus" optional="YES" attributeType="Integer 16" defaultValueString="0" syncable="YES"/>
        <attribute name="updatedAt" optional="YES" attributeType="Date" syncable="YES"/>
    </entity>
    <entity name="Donation" representedClassName="Donation" parentEntity="CoreEntity" syncable="YES">
        <attribute name="amount" optional="YES" attributeType="Decimal" minValueString="0" defaultValueString="0.0" syncable="YES"/>
        <attribute name="checkNumber" optional="YES" attributeType="String" syncable="YES"/>
        <attribute name="split" optional="YES" attributeType="Boolean" syncable="YES"/>
        <attribute name="type" optional="YES" attributeType="String" syncable="YES"/>
        <relationship name="batch" optional="YES" minCount="1" maxCount="1" deletionRule="Nullify" destinationEntity="GivingBatch" inverseName="donations" inverseEntity="GivingBatch" syncable="YES"/>
        <relationship name="donor" optional="YES" minCount="1" maxCount="1" deletionRule="Nullify" destinationEntity="Donor" inverseName="donations" inverseEntity="Donor" syncable="YES"/>
        <relationship name="fund" minCount="1" maxCount="1" deletionRule="Nullify" destinationEntity="Fund" inverseName="donations" inverseEntity="Fund" syncable="YES"/>
    </entity>
    <entity name="Donor" representedClassName="Donor" isAbstract="YES" parentEntity="Collectible" syncable="YES">
        <attribute name="donorType" optional="YES" attributeType="String" syncable="YES"/>
        <relationship name="donations" optional="YES" toMany="YES" deletionRule="Nullify" destinationEntity="Donation" inverseName="donor" inverseEntity="Donation" syncable="YES"/>
    </entity>
    <entity name="EmailAddress" representedClassName="EmailAddress" parentEntity="Collectible" syncable="YES">
        <attribute name="address" attributeType="String" defaultValueString="yourname@yourprovider.com" syncable="YES"/>
        <attribute name="primary" attributeType="Boolean" defaultValueString="YES" syncable="YES"/>
        <attribute name="publish" attributeType="Boolean" defaultValueString="YES" syncable="YES"/>
        <relationship name="person" optional="YES" minCount="1" maxCount="1" deletionRule="Nullify" destinationEntity="Person" inverseName="emailAddresses" inverseEntity="Person" syncable="YES"/>
    </entity>
    <entity name="Family" representedClassName="Family" parentEntity="Collectible" syncable="YES">
        <attribute name="lastName" attributeType="String" defaultValueString="Last Name" syncable="YES"/>
        <relationship name="members" optional="YES" toMany="YES" deletionRule="Cascade" destinationEntity="FamilyMember" inverseName="family" inverseEntity="FamilyMember" syncable="YES"/>
    </entity>
    <entity name="FamilyMember" representedClassName="FamilyMember" parentEntity="CoreEntity" syncable="YES">
        <attribute name="head" attributeType="Boolean" defaultValueString="NO" syncable="YES"/>
        <attribute name="role" attributeType="String" defaultValueString="Mom/Dad/Son/Daughter" syncable="YES"/>
        <relationship name="family" optional="YES" minCount="1" maxCount="1" deletionRule="Nullify" destinationEntity="Family" inverseName="members" inverseEntity="Family" syncable="YES"/>
        <relationship name="personDetail" optional="YES" minCount="1" maxCount="1" deletionRule="Nullify" destinationEntity="Person" inverseName="family" inverseEntity="Person" syncable="YES"/>
    </entity>
    <entity name="Fund" representedClassName="Fund" parentEntity="CoreEntity" syncable="YES">
        <attribute name="desc" optional="YES" attributeType="String" syncable="YES"/>
        <attribute name="name" attributeType="String" defaultValueString="New Fund" syncable="YES"/>
        <relationship name="donations" optional="YES" toMany="YES" deletionRule="Nullify" destinationEntity="Donation" inverseName="fund" inverseEntity="Donation" syncable="YES"/>
    </entity>
    <entity name="GivingBatch" representedClassName="GivingBatch" parentEntity="CoreEntity" syncable="YES">
        <attribute name="name" attributeType="String" defaultValueString="New Batch" syncable="YES"/>
        <attribute name="open" attributeType="Boolean" defaultValueString="YES" syncable="YES"/>
        <relationship name="donations" optional="YES" toMany="YES" deletionRule="Nullify" destinationEntity="Donation" inverseName="batch" inverseEntity="Donation" syncable="YES"/>
    </entity>
    <entity name="Group" representedClassName="Group" parentEntity="Collectible" syncable="YES">
        <attribute name="endDate" optional="YES" attributeType="Date" syncable="YES"/>
        <attribute name="endTime" optional="YES" attributeType="Date" syncable="YES"/>
        <attribute name="maxMembers" optional="YES" attributeType="Integer 16" defaultValueString="0" syncable="YES"/>
        <attribute name="recurring" optional="YES" attributeType="Boolean" syncable="YES"/>
        <attribute name="startDate" optional="YES" attributeType="Date" syncable="YES"/>
        <attribute name="startTime" optional="YES" attributeType="Date" syncable="YES"/>
        <attribute name="type" optional="YES" attributeType="String" syncable="YES"/>
        <relationship name="leaders" optional="YES" toMany="YES" deletionRule="Nullify" destinationEntity="Person" inverseName="leading" inverseEntity="Person" syncable="YES"/>
        <relationship name="location" optional="YES" minCount="1" maxCount="1" deletionRule="Nullify" destinationEntity="Location" inverseName="groupMeetings" inverseEntity="Location" syncable="YES"/>
        <relationship name="members" optional="YES" toMany="YES" deletionRule="Nullify" ordered="YES" destinationEntity="Person" inverseName="member" inverseEntity="Person" syncable="YES"/>
    </entity>
    <entity name="ItemCollection" representedClassName="ItemCollection" parentEntity="CoreEntity" syncable="YES">
        <attribute name="desc" optional="YES" attributeType="String" syncable="YES"/>
        <attribute name="name" optional="YES" attributeType="String" syncable="YES"/>
        <attribute name="public" optional="YES" attributeType="Boolean" syncable="YES"/>
        <attribute name="type" optional="YES" attributeType="String" syncable="YES"/>
        <relationship name="creator" optional="YES" minCount="1" maxCount="1" deletionRule="Nullify" destinationEntity="User" inverseName="collections" inverseEntity="User" syncable="YES"/>
        <relationship name="items" optional="YES" toMany="YES" deletionRule="Nullify" destinationEntity="Collectible" inverseName="collections" inverseEntity="Collectible" syncable="YES"/>
    </entity>
    <entity name="Location" representedClassName="Location" parentEntity="Collectible" syncable="YES">
        <attribute name="desc" optional="YES" attributeType="String" syncable="YES"/>
        <relationship name="groupMeetings" optional="YES" minCount="1" maxCount="1" deletionRule="Nullify" destinationEntity="Group" inverseName="location" inverseEntity="Group" syncable="YES"/>
    </entity>
    <entity name="MailingAddress" representedClassName="MailingAddress" parentEntity="Collectible" syncable="YES">
        <attribute name="address" attributeType="String" defaultValueString="Street Address" syncable="YES"/>
        <attribute name="address2" optional="YES" attributeType="String" defaultValueString="Second Address Line" syncable="YES"/>
        <attribute name="city" attributeType="String" defaultValueString="Tucson" syncable="YES"/>
        <attribute name="country" attributeType="String" defaultValueString="United States" syncable="YES"/>
        <attribute name="postalCode" attributeType="String" defaultValueString="85704" syncable="YES"/>
        <attribute name="primary" attributeType="Boolean" defaultValueString="YES" syncable="YES"/>
        <attribute name="publish" attributeType="Boolean" defaultValueString="YES" syncable="YES"/>
        <attribute name="state" attributeType="String" defaultValueString="AZ" syncable="YES"/>
        <relationship name="person" optional="YES" minCount="1" maxCount="1" deletionRule="Nullify" destinationEntity="Person" inverseName="mailingAddresses" inverseEntity="Person" syncable="YES"/>
    </entity>
    <entity name="Person" representedClassName="Person" isAbstract="YES" parentEntity="Donor" syncable="YES">
        <attribute name="baptism" optional="YES" attributeType="Date" syncable="YES"/>
        <attribute name="birthday" optional="YES" attributeType="Date" syncable="YES"/>
        <attribute name="bornAgain" optional="YES" attributeType="Date" syncable="YES"/>
        <attribute name="churchStaus" attributeType="String" defaultValueString="Visitor" syncable="YES"/>
        <attribute name="dedication" optional="YES" attributeType="Date" syncable="YES"/>
        <attribute name="firstName" optional="YES" attributeType="String" syncable="YES"/>
        <attribute name="firstVisit" optional="YES" attributeType="Date" syncable="YES"/>
        <attribute name="gender" optional="YES" attributeType="String" syncable="YES"/>
        <attribute name="lastName" optional="YES" attributeType="String" syncable="YES"/>
        <attribute name="middleName" optional="YES" attributeType="String" syncable="YES"/>
        <attribute name="nickName" optional="YES" attributeType="String" syncable="YES"/>
        <attribute name="registration" optional="YES" attributeType="Date" syncable="YES"/>
        <attribute name="status" optional="YES" attributeType="String" syncable="YES"/>
        <attribute name="suffix" optional="YES" attributeType="String" syncable="YES"/>
        <relationship name="emailAddresses" optional="YES" toMany="YES" deletionRule="Cascade" destinationEntity="EmailAddress" inverseName="person" inverseEntity="EmailAddress" syncable="YES"/>
        <relationship name="family" optional="YES" minCount="1" maxCount="1" deletionRule="Nullify" destinationEntity="FamilyMember" inverseName="personDetail" inverseEntity="FamilyMember" syncable="YES"/>
        <relationship name="leading" optional="YES" toMany="YES" deletionRule="Nullify" destinationEntity="Group" inverseName="leaders" inverseEntity="Group" syncable="YES"/>
        <relationship name="mailingAddresses" optional="YES" toMany="YES" deletionRule="Cascade" destinationEntity="MailingAddress" inverseName="person" inverseEntity="MailingAddress" syncable="YES"/>
        <relationship name="member" optional="YES" toMany="YES" deletionRule="Nullify" destinationEntity="Group" inverseName="members" inverseEntity="Group" syncable="YES"/>
        <relationship name="phoneNumbers" optional="YES" toMany="YES" deletionRule="Cascade" destinationEntity="PhoneNumber" inverseName="person" inverseEntity="PhoneNumber" syncable="YES"/>
        <relationship name="socialAddresses" optional="YES" toMany="YES" deletionRule="Cascade" destinationEntity="SocialAddress" inverseName="person" inverseEntity="SocialAddress" syncable="YES"/>
        <relationship name="userAccount" optional="YES" minCount="1" maxCount="1" deletionRule="Cascade" destinationEntity="User" inverseName="personDetail" inverseEntity="User" syncable="YES"/>
    </entity>
    <entity name="PhoneNumber" representedClassName="PhoneNumber" parentEntity="Collectible" syncable="YES">
        <attribute name="areaCode" optional="YES" attributeType="String" defaultValueString="520" syncable="YES"/>
        <attribute name="canText" attributeType="Boolean" defaultValueString="NO" syncable="YES"/>
        <attribute name="countryCode" attributeType="String" defaultValueString="+1" syncable="YES"/>
        <attribute name="number" attributeType="String" syncable="YES"/>
        <attribute name="primary" optional="YES" attributeType="Boolean" syncable="YES"/>
        <attribute name="publish" attributeType="Boolean" defaultValueString="YES" syncable="YES"/>
        <relationship name="person" optional="YES" minCount="1" maxCount="1" deletionRule="Nullify" destinationEntity="Person" inverseName="phoneNumbers" inverseEntity="Person" syncable="YES"/>
    </entity>
    <entity name="School" representedClassName="School" parentEntity="Collectible" syncable="YES">
        <attribute name="desc" optional="YES" attributeType="String" syncable="YES"/>
        <relationship name="students" optional="YES" toMany="YES" deletionRule="Nullify" destinationEntity="Child" inverseName="school" inverseEntity="Child" syncable="YES"/>
    </entity>
    <entity name="SocialAddress" representedClassName="SocialAddress" parentEntity="Collectible" syncable="YES">
        <attribute name="networkName" optional="YES" attributeType="String" defaultValueString="Social Network Name" syncable="YES"/>
        <attribute name="url" attributeType="String" defaultValueString="http://socialnetwork.com/user" syncable="YES"/>
        <attribute name="userName" attributeType="String" defaultValueString="Username" syncable="YES"/>
        <relationship name="person" optional="YES" minCount="1" maxCount="1" deletionRule="Nullify" destinationEntity="Person" inverseName="socialAddresses" inverseEntity="Person" syncable="YES"/>
    </entity>
    <entity name="Tag" representedClassName="Tag" parentEntity="CoreEntity" syncable="YES">
        <attribute name="name" attributeType="String" defaultValueString="New Tag" syncable="YES"/>
        <relationship name="creator" optional="YES" minCount="1" maxCount="1" deletionRule="Nullify" destinationEntity="User" inverseName="tags" inverseEntity="User" syncable="YES"/>
        <relationship name="items" optional="YES" toMany="YES" deletionRule="Nullify" destinationEntity="Collectible" inverseName="tags" inverseEntity="Collectible" syncable="YES"/>
    </entity>
    <entity name="User" representedClassName="User" parentEntity="CoreEntity" syncable="YES">
        <attribute name="authData" optional="YES" attributeType="String" syncable="YES"/>
        <attribute name="password" attributeType="String" syncable="YES"/>
        <attribute name="username" attributeType="String" defaultValueString="Username" syncable="YES"/>
        <relationship name="collections" optional="YES" toMany="YES" deletionRule="Cascade" destinationEntity="ItemCollection" inverseName="creator" inverseEntity="ItemCollection" syncable="YES"/>
        <relationship name="personDetail" optional="YES" minCount="1" maxCount="1" deletionRule="Nullify" destinationEntity="Person" inverseName="userAccount" inverseEntity="Person" syncable="YES"/>
        <relationship name="tags" optional="YES" toMany="YES" deletionRule="Nullify" destinationEntity="Tag" inverseName="creator" inverseEntity="Tag" syncable="YES"/>
    </entity>
    <elements>
        <element name="Adult" positionX="160" positionY="192" width="128" height="165"/>
        <element name="Business" positionX="-387" positionY="-6" width="128" height="120"/>
        <element name="Child" positionX="160" positionY="192" width="128" height="105"/>
        <element name="Collectible" positionX="160" positionY="192" width="128" height="90"/>
        <element name="CoreEntity" positionX="160" positionY="192" width="128" height="105"/>
        <element name="Donation" positionX="160" positionY="192" width="128" height="150"/>
        <element name="Donor" positionX="160" positionY="192" width="128" height="73"/>
        <element name="EmailAddress" positionX="-54" positionY="228" width="128" height="105"/>
        <element name="Family" positionX="-270" positionY="153" width="128" height="75"/>
        <element name="FamilyMember" positionX="-459" positionY="189" width="128" height="105"/>
        <element name="Fund" positionX="-234" positionY="297" width="128" height="90"/>
        <element name="GivingBatch" positionX="-398" positionY="468" width="128" height="88"/>
        <element name="Group" positionX="117" positionY="627" width="128" height="195"/>
        <element name="ItemCollection" positionX="412" positionY="-9" width="128" height="135"/>
        <element name="Location" positionX="205" positionY="270" width="128" height="75"/>
        <element name="MailingAddress" positionX="322" positionY="450" width="128" height="180"/>
        <element name="Person" positionX="-227" positionY="414" width="128" height="375"/>
        <element name="PhoneNumber" positionX="466" positionY="264" width="128" height="150"/>
        <element name="School" positionX="331" positionY="153" width="128" height="75"/>
        <element name="SocialAddress" positionX="171" positionY="-9" width="128" height="105"/>
        <element name="Tag" positionX="-9" positionY="54" width="128" height="90"/>
        <element name="User" positionX="97" positionY="282" width="128" height="133"/>
    </elements>
</model>