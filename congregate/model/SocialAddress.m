#import "SocialAddress.h"


@interface SocialAddress ()

// Private interface goes here.

@end


@implementation SocialAddress

-(BOOL)validateEntity:(NSError **)error
{
    BOOL retVal = YES;
    NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
    
    NSMutableString *errorDesc = [[NSMutableString alloc] init];
    
    if (![self label])
    {
        retVal = NO;
        [errorDesc appendString:@"* Label Required\n"];
    }
    
    if (![self url])
    {
        retVal = NO;
        [errorDesc appendString:@"* URL Required\n"];
    }
    
    [errorDetail setValue:errorDesc forKey:NSLocalizedDescriptionKey];
    *error = [NSError errorWithDomain:@"myDomain" code:100 userInfo:errorDetail];
    
    return retVal;
}
@end
