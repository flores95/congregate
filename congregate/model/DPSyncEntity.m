#import "DPSyncEntity.h"
#import "DPSyncEngine.h"

@interface DPSyncEntity ()

// Private interface goes here.

@end


@implementation DPSyncEntity

@synthesize remoteObject = _remoteObject;
@synthesize updatedFromRemote = _updatedFromRemote;
@synthesize updatingFromRemote = _updatingFromRemote;

BOOL isBuildingRelationships = NO;

- (id) initWithEntity:(NSEntityDescription *)entity insertIntoManagedObjectContext:(NSManagedObjectContext *)context
{
    self = [super initWithEntity:entity insertIntoManagedObjectContext:context];
    // add basic data
    if (![self isFault])
    {
        if (![self sourceCreatedAt])
        {
            [self setSourceCreatedAt:[NSDate date]];
        }
        if (![self sourceUpdatedAt])
        {
            [self setSourceUpdatedAt:[NSDate date]];
        }
        [self setDeletedValue:NO];
        // this little magic bullet allows us to update locally created objects with remote data (namely the objectId generated from parse)
        if (![self sourceObjectId])
        {
            [self setSourceObjectId:[DPSyncEntity UUID]];
        }
        
    }
    
    // make sure the engine is watching me
    for (NSString *key in [[[self entity] propertiesByName] keyEnumerator]) {
        if (![key isEqualToString:@"objectId"] || ![key isEqualToString:@"createdAt"] || ![key isEqualToString:@"updatedAt"])
        {
            [self addObserver:[DPSyncEngine sharedInstance]
                   forKeyPath:key
                      options:NSKeyValueObservingOptionNew
                      context:NULL];            
        }
    }
    
    return self;
}

+ (NSString *)UUID
{
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    return (__bridge_transfer NSString *)string;
}

+ (id) createInContext:(NSManagedObjectContext *)context
{
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:[self entityName] inManagedObjectContext:context];
    return [[self alloc] initWithEntity:entityDesc insertIntoManagedObjectContext:context];
}

+ (id) findFirstByAttribute:(NSString *)attribute WithValue:(id)value InContext:(NSManagedObjectContext *)context
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", attribute, value];
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[self entityName]];
    [fetch setPredicate:predicate];

    NSError *error = nil;
    NSArray *results = [context executeFetchRequest:fetch error:&error];
    
    if (error)
    {
        NSLog(@" ... ERROR FINDING %@ where %@=%@", [self entityName], attribute, value);
    }
    if (results && [results count] > 0)
    {
        return [results objectAtIndex:0];
    } else {
        return nil;
    }

}

+ (NSArray *) findAllWithPredicate:(NSPredicate *)predicate InContext:(NSManagedObjectContext *)context
{
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:[self entityName]];
    [fetch setPredicate:predicate];
    
    NSError *error = nil;
    return [context executeFetchRequest:fetch error:&error];
}


+ (NSArray *)getUpdatedRemoteObjects
{
    
    PFQuery *query = [PFQuery queryWithClassName:[self entityName]];
    [query setLimit:1000];
    [query whereKey:@"updatedAt" greaterThan:[[[DPSyncEngine sharedInstance] appInfo] valueForKey:kDPLastSyncDateKey]];
    NSUInteger updateCount = [query countObjects];
    NSMutableArray *remoteUpdates = [[NSMutableArray alloc] initWithCapacity:updateCount];
    if (updateCount > 1000)
    {
        int i = 0;
        while ([remoteUpdates count] < updateCount) {
            [query setSkip:i*1000];
            [remoteUpdates addObjectsFromArray:[query findObjects]];
            i++;
        }
    } else {
        [remoteUpdates addObjectsFromArray:[query findObjects]];
    }
    return remoteUpdates;
}
    
+ (id)createOrUpdateFromRemoteObject:(PFObject *)remoteObj InContext:(NSManagedObjectContext *)context
{
   
    //NSLog(@"#######################################################");
    //NSLog(@"####");
    // match to local object and update or create new
    NSString *remoteObjId = [remoteObj objectId];
    NSString *sourceObjId = [remoteObj valueForKey:@"sourceObjectId"];
    id localObj = [self findFirstByAttribute:@"sourceObjectId" WithValue:sourceObjId InContext:context];
    if (!localObj)
    {
        //NSLog(@"#### CREATING NEW LOCAL OBJECT ");
        localObj = [self createInContext:context];
        [localObj setValue:remoteObjId forKey:@"objectId"];
        [localObj setValue:sourceObjId forKey:@"sourceObjectId"];
    }

    // this flag keeps up from auto creating related data that should only be done on local changes
    [localObj setUpdatingFromRemote:YES];
    
    // add all the remote attributes
    //NSDictionary *props = [[localObj entity] propertiesByName];
    NSDictionary *attrs = [[localObj entity] attributesByName];
    //NSLog(@"#### ATTRIBUTES: ");
    for (NSString *key in [attrs keyEnumerator])
    {
        if (![key isEqualToString:@"objectId"] || ![key isEqualToString:@"updatedAt"] || ![key isEqualToString:@"createdAt"])
        {
           //NSLog(@"    %@ = %@ ", key, [remoteObj valueForKey:key]);
           id remoteVal = [remoteObj valueForKey:key];
            /*
            if ([[props valueForKey:key] attributeType] == NSBooleanAttributeType)
            {
                ([remoteVal boolValue] == YES) ? [localObj setValue:[NSNumber numberWithInt:1] forKey:key] : [localObj setValue:[NSNumber numberWithInt:0] forKey:key];
                if ([remoteVal boolValue] == YES && [key isEqualToString:@"deleted"]) {
                    NSLog(@"found deleted item");
                }
            }
            */
            if ([remoteVal isKindOfClass:[NSNull class]]) {
                [localObj setNilValueForKey:key];
            } else {
                [localObj setValue:remoteVal forKey:key];
            }
            
            // do something special for deleted
            if ([key isEqualToString:@"deleted"])
            {
                if ([remoteVal boolValue] == YES)
                {
                    //NSLog(@"#### DELETED OBJECT #####");
                    [localObj setDeleted:remoteVal];
                    NSLog(@"VALUE OF DELETED IS: %@ for %@(%@)", [localObj deleted], [remoteObj parseClassName], [localObj sourceObjectId]);
                    //[localObj setPrimitiveValue:[NSNumber numberWithBool:YES] forKey:key];
                    //[localObj setDeleted:[NSNumber numberWithBool:YES]];
                } else {
                    [localObj setDeleted:[NSNumber numberWithBool:NO]];
                }
            }
        }
    }
    
    //NSLog(@"#### SETTING SYSTEM SYNC VALUES #####");
    [localObj setValue:[remoteObj objectId]  forKey:@"objectId"];
    //NSLog(@"    objectId = %@ ", [remoteObj objectId]);
    [localObj setValue:[remoteObj updatedAt] forKey:@"updatedAt"];
    //NSLog(@"   updatedAt = %@ ", [remoteObj updatedAt]);
    [localObj setValue:[remoteObj createdAt] forKey:@"createdAt"];
    //NSLog(@"    createdAt = %@ ", [remoteObj createdAt]);

    [localObj setUpdatingFromRemote:NO];
    return localObj;
}

- (void)addRelationshipsFromRemote:(PFObject *)remoteObj
{
    // this flag keeps up from auto creating related data that should only be done on local changes
    [self setUpdatingFromRemote:YES];

    NSDictionary *relationships = [[self entity] relationshipsByName];
    //chase down the relationships
    //NSLog(@" :::::::::::::ADDING RELATIONSHIPS FOR %@/%@:::::::::", [remoteObj parseClassName], [remoteObj valueForKey:@"sourceObjectId"]);
    for (NSString *key in [relationships keyEnumerator])
    {
        //NSLog(@" ------ RELATIONSHIP: %@", key);
        NSString *relatedObjectClass = [[(NSRelationshipDescription *)[relationships valueForKey:key] destinationEntity] name];
        //NSLog(@" ------ CLASS: %@", relatedObjectClass);
        if ([[relationships valueForKey:key] isToMany])
        {
            if (![[remoteObj objectForKey:key] isEqualTo:[NSNull null]]) {
                //NSLog(@" ------ RELATED KEYS: %@", [remoteObj objectForKey:key]);
                NSMutableSet *localObjects = [[NSMutableSet alloc] init];
                for (NSString *relatedObjId in [remoteObj objectForKey:key]) {
                    id localRelatedObj = [NSClassFromString(relatedObjectClass) findFirstByAttribute:@"sourceObjectId"
                                                                                           WithValue:relatedObjId
                                                                                           InContext:[self managedObjectContext]];
                    if (localRelatedObj)
                    {
                        [localObjects addObject:localRelatedObj];
                    }
                }
                [self setValue:localObjects forKey:key];
                //NSLog(@" ------ %lu objects", [localObjects count]);
            }
        } else {
            NSString *relatedObjId = [remoteObj valueForKey:key];
            if (relatedObjId)
            {
                id localRelatedObj = [NSClassFromString(relatedObjectClass) findFirstByAttribute:@"sourceObjectId"
                                                                                  WithValue:relatedObjId
                                                                                  InContext:[self managedObjectContext]];
                if (localRelatedObj)
                {
                    [self setValue:localRelatedObj forKey:key];
                }
            }
        }
    }
    //NSLog(@" ************** ADDED RELATIONSHIPS FOR %@/%@ ***************", [remoteObj parseClassName], [remoteObj valueForKey:@"sourceObjectId"]);
    [self setUpdatingFromRemote:NO];
    
    
}
    
- (PFObject *)remoteObject {
        
    // rebuild the remote object in case values have changed
    _remoteObject = [PFObject objectWithClassName:[[self entity] name]];

    // add all the attributes first
    NSDictionary *attrs = [[self entity] attributesByName];
    @try {
        //manually set the values so "nil" can be replaced with NSNull
        for (NSString *key in [attrs keyEnumerator])
        {
            id value = [self valueForKey:key];
            if ([key isEqualToString:@"objectId"])
            {
                if (value)
                {
                    [_remoteObject setObjectId:value];
                }
            } else if (![key isEqualToString:@"updatedAt"] && ![key isEqualToString:@"createdAt"]) {
                if (value == nil) { value = [NSNull null]; }
                [_remoteObject setObject:value forKey:key];
            }
        }
    }
    @catch (NSException *exception) {
        NSString *report = [NSString stringWithFormat:@" ~~~~~~~~~~ EXCEPTION ADDING ATTRIBUTES TO REMOTE: %@", [exception description]];
        [[NSNotificationCenter defaultCenter] postNotificationName:kDPProcessNotification object:report];
    }

    // Instead of using ineffecient PFRelation we are storing all relationships as arrays using the sourceObjectId
    // we use sourceObjectId so we don't have to make a second pass for new objects that don't have objectId from PARSE
    NSDictionary *relationships = [[self entity] relationshipsByName];
    //chase down the relationships ... could this create a loop?
    for (NSString *key in [relationships keyEnumerator])
    {
        @try
        {
            if ([[relationships objectForKey:key] isToMany])
            {
                NSArray *relatedObjects = [self valueForKey:key];
                //NSMutableArray *relatedIds = [[NSMutableArray alloc] initWithCapacity:[relatedObjects count]];
                if ([relatedObjects count] > 0)
                {
                    //NSLog(@" XXXXXX - ADDING %@ to %@", [relatedObjects valueForKey:@"sourceObjectId"], key);
                    //[_remoteObject setObject:[relatedObjects valueForKey:@"sourceObjectId"] forKey:key];
                    //NSLog(@" XXXXXX - %@ : %@", key, [_remoteObject objectForKey:key]);
                    
                     for (id relatedObj in relatedObjects)
                    {
                        [_remoteObject addUniqueObject:(NSString *)[relatedObj valueForKey:@"sourceObjectId"] forKey:key];
                        //[relatedIds addObject:(NSString *)[relatedObjects valueForKey:@"sourceObjectId"]];
                    }
                    
                } else {
                    [_remoteObject setObject:[NSNull null] forKey:key];
                }
            } else {
                id relatedObj = [self valueForKey:key];
                if (relatedObj) {
                    [_remoteObject setValue:[relatedObj valueForKey:@"sourceObjectId"] forKey:key];
                } else {
                    [_remoteObject setObject:[NSNull null] forKey:key];
                }
            }
        }
        @catch (NSException *exception)
        {
            NSString *report = [NSString stringWithFormat:@" ~~~~~~~~~~ EXCEPTION ADDING RELATION(%@) TO REMOTE: %@", key, [exception description]];
            [[NSNotificationCenter defaultCenter] postNotificationName:kDPProcessNotification object:report];
        }

    }
    
    return _remoteObject;
}

- (void)dealloc
{
    // remove observers
    for (NSString *key in [[[self entity] propertiesByName] keyEnumerator]) {
        if (![key isEqualToString:@"objectId"] || ![key isEqualToString:@"createdAt"] || ![key isEqualToString:@"updatedAt"])
        {
            [self removeObserver:[DPSyncEngine sharedInstance] forKeyPath:key];
        }
    }
}

@end
