#import "_DPBaseEntity.h"

@interface DPBaseEntity : _DPBaseEntity {}

+ (NSUInteger) countOfEntitiesWithPredicate:(NSPredicate *)predicate InContext:(NSManagedObjectContext *)context;

- (id)duplicateEntity;
- (BOOL)validateEntity:(NSError **)error;
- (BOOL)deleteEntity:(NSError **)error;

@end
