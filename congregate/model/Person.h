#import "_Person.h"
#import "PhoneNumber.h"
#import "MailingAddress.h"
#import "EmailAddress.h"

@interface Person : _Person
{
    NSString *gradeName;
    NSInteger *age;
}

@property (strong, nonatomic) NSString *gradeName;
@property (strong, nonatomic) NSNumber *age;
@property (strong, nonatomic) EmailAddress *primaryEmail;
@property (strong, nonatomic) PhoneNumber *primaryPhone;
@property (strong, nonatomic) MailingAddress *primaryAddress;

// some typically querries
+ (int) countAllInContext:(NSManagedObjectContext *)context;
+ (int) countAdultsInContext:(NSManagedObjectContext *)context;
+ (int) countChildrenInContext:(NSManagedObjectContext *)context;
+ (int) countActive:(BOOL)isActive InContext:(NSManagedObjectContext *)context;
+ (int) countChurchStatus:(NSString *)status InContext:(NSManagedObjectContext *)context;

// group related queries
- (NSSet *) currentCheckInGroups;

@end
