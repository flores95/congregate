#import "_Donor.h"

@interface Donor : _Donor {
    NSString *displayName;
}

@property NSString *displayName;

@end
