#import "_Family.h"
#import "Person.h"

@interface Family : _Family {}

@property (strong, nonatomic) Person *headOfFamily;
@property (strong, nonatomic) NSString *memberNames;
@property (strong, nonatomic) NSSet *children;
@property (strong, nonatomic) NSSet *adults;

@end
