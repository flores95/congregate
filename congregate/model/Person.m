#import "Person.h"
#import "SignificantDate.h"
#import "MailingAddress.h"
#import "EmailAddress.h"
#import "PhoneNumber.h"
#import "FamilyMember.h"
#import "Family.h"

@interface Person ()

// Private interface goes here.

@end


@implementation Person

- (id)initWithEntity:(NSEntityDescription *)entity insertIntoManagedObjectContext:(NSManagedObjectContext *)context
{
    self = [super initWithEntity:entity insertIntoManagedObjectContext:context];
    
    [self addObserver:self forKeyPath:@"firstName"          options:NSKeyValueObservingOptionNew context:NULL];
    [self addObserver:self forKeyPath:@"lastName"           options:NSKeyValueObservingOptionNew context:NULL];
    [self addObserver:self forKeyPath:@"nickName"           options:NSKeyValueObservingOptionNew context:NULL];
    [self addObserver:self forKeyPath:@"birthday"           options:NSKeyValueObservingOptionNew context:NULL];
    [self addObserver:self forKeyPath:@"gradeAdjust"        options:NSKeyValueObservingOptionNew context:NULL];
    [self addObserver:self forKeyPath:@"mailingAddresses"   options:NSKeyValueObservingOptionNew context:NULL];
    [self addObserver:self forKeyPath:@"phoneNumbers"       options:NSKeyValueObservingOptionNew context:NULL];
    [self addObserver:self forKeyPath:@"emailAddresses"     options:NSKeyValueObservingOptionNew context:NULL];
    [self addObserver:self forKeyPath:@"active"             options:NSKeyValueObservingOptionNew context:NULL];
    
    return self;
}

- (BOOL)deleteEntity:(NSError *__autoreleasing *)error
{
    // remove the associated addresses and such
    for (MailingAddress *addr in [self mailingAddresses])
    {
        [addr deleteEntity:error];
    }
    for (PhoneNumber *num in [self phoneNumbers])
    {
        [num deleteEntity:error];
    }
    for (EmailAddress *email in [self emailAddresses])
    {
        [email deleteEntity:error];
    }
    if ([self family])
    {
        Family *fam = [[self family] family];
        [[self family] deleteEntity:error];
        // delete the family if it doesn't have any membeers
        if ([[fam members] count] == 1)
        {
            [fam deleteEntity:error];
        }
    }
    return [super deleteEntity:error];
}

#pragma mark - Observation
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"firstName"])
    {
        if (![self nickName])
        {
            [self setNickName:[self firstName]];
        }
        [self setName:[NSString stringWithFormat:@"%@ %@", [self nickName], [self lastName]]];
    }
    
    if ([keyPath isEqualToString:@"lastName"])
    {
        [self setName:[NSString stringWithFormat:@"%@ %@", [self nickName], [self lastName]]];
    }
    
    if ([keyPath isEqualToString:@"nickName"])
    {
        [self setName:[NSString stringWithFormat:@"%@ %@", [self nickName], [self lastName]]];
    }
    
  
    if ([keyPath isEqualToString:@"birthday"] || [keyPath isEqualToString:@"gradeAdjust"])
    {
        //TODO - need to calculate based on August 1st of year born???
        /*
         
         if birthMonth > 8
         if birthMonth < 8
         if curMonth > birthMonth and curMonth < 8
         
         */
        if ([self birthday] && ![self adultValue])
        {
            int adjust = 0;
            if ([self gradeAdjust])
            {
                adjust = [self gradeAdjustValue];
            }
            NSDateComponents *curDateComponents = [[NSCalendar currentCalendar] components:NSYearCalendarUnit fromDate:[NSDate date]];
            NSInteger curYear = [curDateComponents year];
            
            NSDateComponents *birthDateComponents = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:[self birthday]];
            NSInteger birthYear = [birthDateComponents year];
            NSInteger birthMonth = [birthDateComponents month];
            NSInteger birthDay = [birthDateComponents day];
            
            NSDateComponents *birthdayComponents = [[NSDateComponents alloc] init];
            [birthdayComponents setYear:curYear];
            [birthdayComponents setMonth:birthMonth];
            [birthdayComponents setDay:birthDay];
            NSDate *curBirthday = [[NSCalendar currentCalendar] dateFromComponents:birthdayComponents];
            
            NSDateComponents *cutoffDateComponents = [[NSDateComponents alloc] init];
            [cutoffDateComponents setYear:curYear];
            [cutoffDateComponents setMonth:8];
            [cutoffDateComponents setDay:1];
            NSDate *cutoffDate = [[NSCalendar currentCalendar] dateFromComponents:cutoffDateComponents];
            
            NSInteger gradeNumber = curYear - birthYear - 6 + adjust;
            if ([curBirthday isGreaterThanOrEqualTo:cutoffDate])
            {
                gradeNumber = gradeNumber - 1;
            }
            if ([[NSDate date] isGreaterThanOrEqualTo:cutoffDate])
            {
                gradeNumber = gradeNumber + 1;
            }
            switch (gradeNumber) {
                case -1:
                    [self setGradeName:@"PreK"];
                case 0:
                    [self setGradeName:@"K"];
                case 1:
                    [self setGradeName:@"1st"];
                case 2:
                    [self setGradeName:@"2nd"];
                case 3:
                    [self setGradeName:@"3rd"];
                case 4:
                    [self setGradeName:@"4th"];
                case 5:
                    [self setGradeName:@"5th"];
                case 6:
                    [self setGradeName:@"6th"];
                case 7:
                    [self setGradeName:@"7th"];
                case 8:
                    [self setGradeName:@"8th"];
                case 9:
                    [self setGradeName:@"9th"];
                case 10:
                    [self setGradeName:@"10th"];
                case 11:
                    [self setGradeName:@"11th"];
                case 12:
                    [self setGradeName:@"12th"];
            
                default:
                    [self setGradeName:nil];
            }
            
            [self setGradeValue:gradeNumber];
        }
    }
    
    if ([keyPath isEqualToString:@"mailingAddresses"])
    {
        NSNumber *kind = [change objectForKey:NSKeyValueChangeKindKey];
        if ([kind integerValue] == NSKeyValueChangeInsertion)  
        {
            // a new one was added update the calculated field
            [self setPrimaryAddress:nil];
        }
    }
    
    if ([keyPath isEqualToString:@"phoneNumbers"])
    {
        NSNumber *kind = [change objectForKey:NSKeyValueChangeKindKey];
        if ([kind integerValue] == NSKeyValueChangeInsertion)
        {
            // a new one was added update the calculated field
            [self setPrimaryPhone:nil];
        }
    }
    
    if ([keyPath isEqualToString:@"emailAddresses"])
    {
        NSNumber *kind = [change objectForKey:NSKeyValueChangeKindKey];
        if ([kind integerValue] == NSKeyValueChangeInsertion)
        {
            // a new one was added update the calculated field
            [self setPrimaryEmail:nil];
        }
    }

}

- (void)dealloc
{
    [self removeObserver:self forKeyPath:@"firstName"];
    [self removeObserver:self forKeyPath:@"lastName"];
    [self removeObserver:self forKeyPath:@"nickName"];
    [self removeObserver:self forKeyPath:@"birthday"];
    [self removeObserver:self forKeyPath:@"gradeAdjust"];
    [self removeObserver:self forKeyPath:@"mailingAddresses"];
    [self removeObserver:self forKeyPath:@"phoneNumbers"];
    [self removeObserver:self forKeyPath:@"emailAddresses"];
}

# pragma mark - Custom Accessors

- (void)setChurchStatus:(NSString *)churchStatus
{
    if (!self.updatingFromRemote && self.churchStatus && self.churchStatus != churchStatus)
    {
        // make a significant date with the change in status
        SignificantDate *changeDate = [SignificantDate createInContext:[self managedObjectContext]];
        [changeDate setEventDate:[NSDate date]];
        [changeDate setEventTitle:[NSString stringWithFormat:@"%@ Status", churchStatus]];
        [changeDate setEventDescription:[NSString stringWithFormat:@"Previous status: %@", self.churchStatus]];
        [self addSignificantDatesObject:changeDate];
    }

    [self willAccessValueForKey:@"churchStatus"];
    [self setPrimitiveChurchStatus:churchStatus];
    [self didAccessValueForKey:@"churchStatus"];
}

- (void)setActive:(NSNumber *)active
{
    if (self.active != active)
    {
        if (![active boolValue])
        {
            // make a significant date with the change in status
            SignificantDate *changeDate = [SignificantDate createInContext:[self managedObjectContext]];
            [changeDate setEventDate:[NSDate date]];
            [changeDate setEventTitle:@"Made Inactive"];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSString *user = [defaults valueForKey:kDPAppUserKey];
            [changeDate setEventDescription:[NSString stringWithFormat:@"Editor: %@", user]];
            [self addSignificantDatesObject:changeDate];
        }
    }
    
    [self willAccessValueForKey:@"active"];
    [self setPrimitiveActive:active];
    [self didAccessValueForKey:@"active"];
}

# pragma mark - Calculated Field Accessors

- (void)setGradeName:(NSString *)gradeName
{
    // do nothing this is a calculated field
}

- (NSString *)gradeName
{
    // TODO - Should remove "grade" as a stored property and always calculate it
    // WORKAROUND - ignore "grade" property and calculate
    
    NSInteger gradeNumber = 99; //
    if ([self birthday] && ![self adultValue])
    {
        int adjust = 0;
        if ([self gradeAdjust])
        {
            adjust = [self gradeAdjustValue];
        }
        NSDateComponents *curDateComponents = [[NSCalendar currentCalendar] components:NSYearCalendarUnit fromDate:[NSDate date]];
        NSInteger curYear = [curDateComponents year];
        
        NSDateComponents *birthDateComponents = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:[self birthday]];
        NSInteger birthYear = [birthDateComponents year];
        NSInteger birthMonth = [birthDateComponents month];
        NSInteger birthDay = [birthDateComponents day];
        
        NSDateComponents *birthdayComponents = [[NSDateComponents alloc] init];
        [birthdayComponents setYear:curYear];
        [birthdayComponents setMonth:birthMonth];
        [birthdayComponents setDay:birthDay];
        NSDate *curBirthday = [[NSCalendar currentCalendar] dateFromComponents:birthdayComponents];
        
        //TODO - need system preference for promotion date
        NSDateComponents *cutoffDateComponents = [[NSDateComponents alloc] init];
        [cutoffDateComponents setYear:curYear];
        [cutoffDateComponents setMonth:8];
        [cutoffDateComponents setDay:1];
        NSDate *cutoffDate = [[NSCalendar currentCalendar] dateFromComponents:cutoffDateComponents];
        
        gradeNumber = curYear - birthYear - 6 + adjust;
        if ([curBirthday isGreaterThanOrEqualTo:cutoffDate])
        {
            gradeNumber = gradeNumber - 1;
        }
        if ([[NSDate date] isGreaterThanOrEqualTo:cutoffDate])
        {
            gradeNumber = gradeNumber + 1;
        }
    }
    
    switch (gradeNumber) {
        case -1:
            return @"PreK";
        case 0:
            return @"K";
        case 1:
            return @"1st";
        case 2:
            return @"2nd";
        case 3:
            return @"3rd";
        case 4:
            return @"4th";
        case 5:
            return @"5th";
        case 6:
            return @"6th";
        case 7:
            return @"7th";
        case 8:
            return @"8th";
        case 9:
            return @"9th";
        case 10:
            return @"10th";
        case 11:
            return @"11th";
        case 12:
            return @"12th";
           
        default:
            return nil;
    }
}

- (void)setAge:(NSNumber *)age
{
    // do nothing it's a calculated field
}
- (NSNumber *)age
{
    NSNumber *returnVal = nil;
    if ([self birthday])
    {
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *components = [calendar components:NSYearCalendarUnit
                                                   fromDate:[self birthday]
                                                     toDate:[NSDate date]
                                                    options:0];
        returnVal = [NSNumber numberWithInteger:[components year]];
    }
    return returnVal;
}

- (void)setPrimaryAddress:(MailingAddress *)primaryAddress
{
    // do nothing it's a calculated field
}

- (MailingAddress *)primaryAddress
{
    if ([[self mailingAddresses] count] > 0)
    {
        for (MailingAddress *addr in [self mailingAddresses])
        {
            if ([addr primaryValue])
            {
                return addr;
            }
        }
    }
    return nil;
}

- (void)setPrimaryEmail:(EmailAddress *)primaryEmail
{
    // do nothing it's a calculated field
}

- (EmailAddress *)primaryEmail
{
    if ([[self emailAddresses] count] > 0)
    {
        for (EmailAddress *addr in [self emailAddresses])
        {
            if ([addr primaryValue])
            {
                return addr;
            }
        }
    }
    return nil;
}

- (void)setPrimaryPhone:(PhoneNumber *)primaryPhone
{
    // do nothing it's a calculated field
}

- (PhoneNumber *)primaryPhone
{
    if ([[self phoneNumbers] count] > 0)
    {
        for (PhoneNumber *addr in [self phoneNumbers])
        {
            if ([addr primaryValue])
            {
                return addr;
            }
        }
    }
    return nil;
}

#pragma mark - Detail Queries

- (NSSet *)currentCheckInGroups
{
    NSSet *results = nil;
    
    if ([[self memberOf] count] > 0)
    {
        //TODO - add filter that only returns classes meeting today or withing 1 or 2 hours
        //NSPredicate *currentPred = [NSPredicate predicateWithFormat:@""]
        results = [self memberOf];
    }
    
    return results;
}

#pragma mark - Class Querries
+ (int) countAllInContext:(NSManagedObjectContext *)context
{
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"deleted != 1"];
    return (int)[Person countOfEntitiesWithPredicate:pred InContext:context];
}

+ (int) countAdultsInContext:(NSManagedObjectContext *)context
{
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"deleted != 1 and adult == 1"];
    return (int)[Person countOfEntitiesWithPredicate:pred InContext:context];
    
}

+ (int) countChildrenInContext:(NSManagedObjectContext *)context
{
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"deleted != 1 and adult == 0"];
    return (int)[Person countOfEntitiesWithPredicate:pred InContext:context];
   
}

+ (int) countActive:(BOOL)isActive InContext:(NSManagedObjectContext *)context
{
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"deleted != 1 and active == 1"];
    return (int)[Person countOfEntitiesWithPredicate:pred InContext:context];
    
}

+ (int) countChurchStatus:(NSString *)status InContext:(NSManagedObjectContext *)context
{
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"deleted != 1 and churchStatus == %@", status];
    return (int)[Person countOfEntitiesWithPredicate:pred InContext:context];
    
}

#pragma mark - Validation

-(BOOL)validateEntity:(NSError **)error
{
    BOOL retVal = YES;
    NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
    
    NSMutableString *errorDesc = [[NSMutableString alloc] init];
    if (![self firstName])
    {
        retVal = NO;
        [errorDesc appendString:@"* First Name Required\n"];
    }
    
    if (![self lastName])
    {
        retVal = NO;
        [errorDesc appendString:@"* Last Name Required\n"];
    }
    
    if (![self gender])
    {
        retVal = NO;
        [errorDesc appendString:@"* Gender Required\n"];
    }
    [errorDetail setValue:errorDesc forKey:NSLocalizedDescriptionKey];
    *error = [NSError errorWithDomain:@"myDomain" code:100 userInfo:errorDetail];
    
    return retVal;
}

@end
