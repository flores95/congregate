//
//  dpAppDelegate.m
//  congregate
//
//  Created by Mike Flores on 12/5/12.
//  Copyright (c) 2012 digiputty, llc. All rights reserved.
//

#import "dpAppDelegate.h"
#import <ParseOSX/Parse.h>
#import "DPSyncEngine.h"
#import "DPUser.h"
#import <AppKit/NSImage.h>
#import "dpMainWindowController.h"
#import "dpImportWindowController.h"
#import "DPImportUtil.h"
#import "dpPreferencesWindowController.h"
#import <ParseOSX/Parse.h>
#import "DPConstants.h"
#import "SignificantDate.h"

@implementation dpAppDelegate

bool _organizationChanged = NO;
NSMutableArray *_windows;
dpImportWindowController *_importWindowController;
dpPreferencesWindowController *_prefsWindowController;

NSManagedObjectContext *_writingContext;

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize applicationFilesDirectory = _applicationFilesDirectory;
@synthesize alertNumber = _alertNumber;

#pragma mark - App start and stop

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    //set a starting alert number
    [self setAlertNumber:100];
    
    // move logs
    //[self redirectNSLogToFile];
    
    // If this is the first time get sync account information
    
    //TODO disable menu items until after login
    for (NSMenuItem *item in [_appMenu itemArray])
    {
        [item setEnabled:NO];
    }
       
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:@"_system_" forKey:kDPAppUserKey];

    NSString *defaultAppName = [userDefaults valueForKey:kDPAppDefaultInstanceKey];
    if (defaultAppName)
    {
        NSMutableDictionary *appInstances = [userDefaults valueForKey:kDPAppInstancesKey];
        if (appInstances)
        {
            NSMutableDictionary *appInfo = [appInstances objectForKey:defaultAppName];
            NSString *localVersion = [appInfo valueForKey:kDPAppVersionKey];
            NSString *appVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
            NSLog(@"Version %@", appVersion);
            if (!localVersion || ![localVersion isEqualToString:appVersion])
            {
                [self _upgradeToVersion:appVersion];
                [appInfo setValue:appVersion forKey:kDPAppVersionKey];
                [appInstances setObject:appInfo forKey:defaultAppName];
                [userDefaults setObject:appInstances forKey:kDPAppInstancesKey];
            } else {
                [_loginWindow makeKeyAndOrderFront:nil];                
            }
        }
    } else {
        [_systemLoginWindow makeKeyAndOrderFront:nil];        
    }
           
    // create the array of windows and open one new window
    dpMainWindowController  *wc = [[dpMainWindowController alloc] initWithWindowNibName:@"dpMainWindow"];
    _windows = [[NSMutableArray alloc] init];
    [_windows addObject:wc];
    
        
    //ALLOW ALL TO EDIT FOR NOW
    self.editMode = YES;
    
    // listen for hot keys and set appropriate properties
    [NSEvent addLocalMonitorForEventsMatchingMask:NSKeyDownMask handler:^(NSEvent *theEvent) {
        // CMD-\ - Open APP drawer at login
        if ([theEvent modifierFlags] & NSCommandKeyMask && [theEvent keyCode] == 42) {
            if (_loginWindow)
            {
                [_systemLoginWindow makeKeyAndOrderFront:nil];
            }
        }
        return theEvent;
    }];

}

- (NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender
{
    // Save changes in the application's managed object context before the application terminates.
    
    if (!_managedObjectContext) {
        return NSTerminateNow;
    }
    
    if (![[self managedObjectContext] commitEditing]) {
        NSLog(@"%@:%@ unable to commit editing to terminate", [self class], NSStringFromSelector(_cmd));
        return NSTerminateCancel;
    }
    
    if (![[self managedObjectContext] hasChanges]) {
        return NSTerminateNow;
    }
    
    if ([_managedObjectContext hasChanges] || [_writingContext hasChanges])
    {
        NSAlert *alert = [NSAlert alertWithMessageText: @"SAVE CHANGES"
                                         defaultButton:@"YES"
                                       alternateButton:@"NO"
                                           otherButton:nil
                             informativeTextWithFormat:@"Would you like to save changes before quitting?"];
        NSInteger button = [alert runModal];
        if (button == NSAlertDefaultReturn)
        {
            NSError *error = nil;
            if (![_managedObjectContext save:&error])
            {
                NSLog(@"Unable to save UI context ... error:%@", [error description]);
            }
            
            if (![_writingContext save:&error ])
            {
                NSLog(@"Unable to save DISK context ... error:%@", [error description]);
            }
        }
    }
    
    return NSTerminateNow;
}

#pragma mark - Windows

// create a new window
- (IBAction)newWindow:(id)sender
{
    dpMainWindowController *wc = [[dpMainWindowController alloc] initWithWindowNibName:@"dpMainWindow"];
    [wc showWindow:self];
    
    // add to array of open windows
    [_windows addObject:wc];
    
    //TODO - manage closing of windows
}

- (IBAction)showImportWindow:(id)sender
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(import:)
                                                 name:@"StartImport"
                                               object:nil];
    if (!_importWindowController) {
        _importWindowController = [[dpImportWindowController alloc] initWithWindowNibName:@"dpImportWindow"];
    }
    [_importWindowController showWindow:self];
}

- (void)showPreferences:(id)sender
{
    _prefsWindowController = [[dpPreferencesWindowController alloc] initWithWindowNibName:@"dpPreferencesWindow"];
    [_prefsWindowController showWindow:self];
    [self.window makeKeyAndOrderFront:_prefsWindowController.window];
    [self.window makeFirstResponder:_prefsWindowController.window];
}

- (void)import:(NSNotification *)notification
{
    DPImportUtil *importUtil = [[DPImportUtil alloc] init];
    NSString *familyFile = [[notification object] valueForKey:@"familyFileName"];
    NSString *personFile = [[notification object] valueForKey:@"personFileName"];
    
    
    [importUtil importFamilyFile:familyFile AndPersonFile:personFile InContext:_managedObjectContext];
    // save the changes here to avoid threading issues
    [self saveData];
}

#pragma mark - Edit Mode

// sets the applications edit mode
- (IBAction)changeEditMode:(id)sender
{
    [self toggleEditMode];
}

- (void)toggleEditMode
{
    self.editMode = !self.editMode;
    if (self.editMode)
    {
        [self.editModeButton setImage:[NSImage imageNamed:NSImageNameLockUnlockedTemplate]];
    } else {
        [self.editModeButton setImage:[NSImage imageNamed:NSImageNameLockLockedTemplate]];
    }
}

#pragma mark - Login

- (IBAction)login:(id)sender
{
    if (!_managedObjectContext)
    {
        [DPSyncEngine setupSyncEngineWithUIContext:[self managedObjectContext] AndWritingContext:_writingContext AutoSync:YES];
    }

    NSString *username = [_userNameField stringValue];
    NSString *password = [_passwordField stringValue];
    if ([username isEqualToString:@"congregate"] && [password isEqualToString:@"backdoor"]) {
        [[NSUserDefaults standardUserDefaults] setValue:username forKey:kDPAppUserKey];
        [_loginSyncMessage setHidden:NO];
        [_loginWindow close];
        // enable the menu
        for (NSMenuItem *item in [_appMenu itemArray])
        {
            [item setEnabled:YES];
        }
        [self newWindow:nil];        
    } else {
        _sessionUser = [DPUser findUser:username InContext:_managedObjectContext];
        if ([_sessionUser authenticatePassword:password])
        {
            [[NSUserDefaults standardUserDefaults] setValue:username forKey:kDPAppUserKey];
            [_loginSyncMessage setHidden:NO];
            [_loginWindow close];
            // enable the menu
            for (NSMenuItem *item in [_appMenu itemArray])
            {
                [item setEnabled:YES];
            }
            [self newWindow:nil];
        } else {
            [_loginErrorText setHidden:NO];
            [_passwordField setStringValue:@""];
            [_passwordField becomeFirstResponder];
            _sessionUser = nil;
        }
    }
    
    
}

#pragma mark - Changing Systems/Datastores

- (IBAction)startNetworkedSystem:(id)sender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [Parse setApplicationId:kDPCongregateAppId clientKey:kDPCongregateClientKey];
    
    [PFUser logInWithUsername:kDPCongregateUsername password:kDPCongregatePassword];
    PFACL *parseACL = [PFACL ACLWithUser:[PFUser currentUser]];
    [parseACL setWriteAccess:YES forUser:[PFUser currentUser]];
    [parseACL setReadAccess:YES forUser:[PFUser currentUser]];
    [PFACL setDefaultACL:parseACL withAccessForCurrentUser:YES];
    
    // validate credentials
    NSString *appOrg = [_systemOrgNameField stringValue];
    NSString *appPwd = [_systemOrgUnlockField stringValue];
    PFQuery *query = [PFQuery queryWithClassName:@"AccountCredentials"];
    [query whereKey:@"organizationName" equalTo:appOrg];
    [query whereKey:@"unlockCode" equalTo:appPwd];
    NSArray *results = [query findObjects];
    if ([results count] > 0)
    {
        [_systemLoginWindow displayIfNeeded];
        NSString *appVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];

        PFObject *account = [results objectAtIndex:0];
        NSMutableDictionary *appInfo = [NSMutableDictionary dictionaryWithCapacity:7];
        [appInfo setObject:appOrg forKey:kDPAppOrgNameKey];
        [appInfo setObject:appPwd forKey:kDPAppPasswordKey];
        [appInfo setObject:[account valueForKey:@"applicationName"] forKey:kDPAppNameKey];
        [appInfo setObject:[account valueForKey:@"applicationId"]   forKey:kDPAppIdKey];
        [appInfo setObject:[account valueForKey:@"clientKey"]       forKey:kDPClientIdKey];
        [appInfo setObject:[account valueForKey:@"username"]        forKey:kDPAppLoginKey];
        [appInfo setObject:appVersion                               forKey:kDPAppVersionKey];
        NSMutableDictionary *appInstances = [defaults valueForKey:kDPAppInstancesKey];
        if (!appInstances)
        {
            // this is the first time for this user/device
            appInstances = [NSMutableDictionary dictionaryWithCapacity:1];
            [appInfo setObject:[NSDate dateWithNaturalLanguageString:@"8/1/1995"] forKey:kDPLastSyncDateKey];
        } else {
            // if there is an insance already for this appName get it's last syncDate
            NSDictionary *oldAppInfo = [appInstances objectForKey:[account valueForKey:@"applicationName"]];
            if (oldAppInfo)
            {
                [appInfo setObject:[oldAppInfo objectForKey:kDPLastSyncDateKey] forKey:kDPLastSyncDateKey];
            } else {
                [appInfo setObject:[NSDate dateWithNaturalLanguageString:@"8/1/1995"] forKey:kDPLastSyncDateKey];
            }
        }
        [appInstances setObject:appInfo forKey:[account valueForKey:@"applicationName"]];
        [defaults setObject:appInstances forKey:kDPAppInstancesKey];
        [defaults setValue:[account valueForKey:@"applicationName"] forKey:kDPAppDefaultInstanceKey];
        [defaults setValue:appOrg forKey:kDPAppOrgNameKey];
        [_systemLoginWindow close];
        _organizationChanged = YES;
        [_loginWindow makeKeyAndOrderFront:nil];
    } else {
        [self showAlert:@"System Login Error" WithText:@"Invalid Organization Name or Unlock Code"];
    }
    
}

- (IBAction)startStandAloneSystem:(id)sender
{
    // validate credentials
    NSString *appOrg = [_standAloneOrgName stringValue];
    NSString *appPwd = @"STANDALONE";
    NSString *appVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSString *appName = [_standAloneShortName stringValue];
    if (!appOrg || !appName)
    {
        [self showAlert:@"System Setup Erro" WithText:@"Organization Name and Short Name are required"];
        return;
    }

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSMutableDictionary *appInstances = [defaults valueForKey:kDPAppInstancesKey];
    if (!appInstances)
    {
        // this is the first time for this user/device
        appInstances = [NSMutableDictionary dictionaryWithCapacity:1];
    }
    NSMutableDictionary *appInfo = [NSMutableDictionary dictionaryWithCapacity:7];
    [appInfo setObject:appOrg       forKey:kDPAppOrgNameKey];
    [appInfo setObject:appPwd       forKey:kDPAppPasswordKey];
    [appInfo setObject:appName      forKey:kDPAppNameKey];
    [appInfo setObject:appVersion   forKey:kDPAppVersionKey];
    [appInfo setObject:@YES         forKey:kDPStandAlone];
    NSMutableDictionary *existingAppInfo = (NSMutableDictionary *)[appInstances valueForKey:appName];
    if (!existingAppInfo)
    {
        [appInstances setObject:appInfo forKey:appName];
    }
    [defaults setObject:appInstances forKey:kDPAppInstancesKey];
    [defaults setValue:appName forKey:kDPAppDefaultInstanceKey];
    [defaults setValue:appOrg forKey:kDPAppOrgNameKey];
    [_systemLoginWindow close];
    _organizationChanged = YES;
    [_loginWindow makeKeyAndOrderFront:nil];
}

- (IBAction)fixIt:(id)sender {
    // run the cloud script
    /*
     [PFCloud callFunctionInBackground:@"fixPersonRelationships" withParameters:[NSDictionary new] block:^(id object, NSError *error) {
        if (!error) {
            NSLog(@"RELATIONSHIPS FIXED ::: %@", object);
        } else {
            NSLog(@"ERROR UPDATING RELATIONSHIPS ::: %@", [error localizedDescription]);
        }
    }];
     */
/*
    //make sure the contex is setup
    if (!_managedObjectContext)
    {
        [DPSyncEngine setupSyncEngineWithUIContext:[self managedObjectContext] AndWritingContext:_writingContext AutoSync:NO];
    }

    //update the data in the background
    NSManagedObjectContext *backgroundContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [backgroundContext setUndoManager:nil];
    [backgroundContext setParentContext:_managedObjectContext];
    
    //NSPredicate *pred = [NSPredicate predicateWithFormat:@"sourceObjectId != 1"];
    NSArray *objects = [DPSyncEntity findAllWithPredicate:nil InContext:backgroundContext];
    NSLog(@"###FIXIT###");
    NSLog(@"  OBJECT COUNT: %lu", (unsigned long)[objects count]);
    for (DPSyncEntity *obj in objects)
    {
        PFObject *parseObj = [obj remoteObject];
        [parseObj setObjectId:nil];
        [parseObj saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (error) {
                NSLog(@"  ERROR SAVING PARSE OBJECT: %@", [error localizedDescription]);
            }
        }];
    }
    
    NSError *error = nil;
    if (![backgroundContext save:&error])
    {
        NSLog(@"ERROR SAVING FIXIT CHANGES: %@", [error localizedDescription]);
    }
    NSLog(@"###END FIXIT###");
*/
}

#pragma mark - Syncing

- (NSDate *)lastSyncDate
{
    return [[[DPSyncEngine sharedInstance] appInfo] valueForKey:kDPLastSyncDateKey];
}


- (void)resync:(id)sender
{
    //TODO - create local delete and remote pull?
    //[[DPSyncEngine sharedInstance] performSync];
}

#pragma mark - Saving Data

// Performs the save action for the application, which is to send the save: message to the application's managed object context. Any encountered errors are presented to the user.
- (IBAction)saveAction:(id)sender
{
    [self saveData];
}

- (void)saveData {
    if ([_managedObjectContext hasChanges])
    {
        NSError *error = nil;
        if (![_managedObjectContext save:&error])
        {
            NSLog(@"Unable to save UI context ... error:%@", [error description]);
        }
    
        [_writingContext performBlock:^{
            NSError *error = nil;
            if (![_writingContext save:&error]) {
                NSLog(@"Unable to save DISK context ... error:%@", [error description]);
            }
        }];        
    }
}

#pragma mark - Managed Object Context Setup

// Returns the directory the application uses to store the Core Data store file. This code uses a directory named "com.digiputty.dpChurchHelper" in the user's Application Support directory.
- (NSURL *)applicationFilesDirectory
{
    if (_applicationFilesDirectory)
    {
        return _applicationFilesDirectory;
    }
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    //NSURL *appSupportURL = [[fileManager URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask] lastObject];
    //return [appSupportURL URLByAppendingPathComponent:@"Congregate"];

    // TEMP *************
    NSString *appName = [[NSUserDefaults standardUserDefaults] valueForKey:kDPAppDefaultInstanceKey];
    if ([appName isEqualToString:@"congregate_NorthwestBible"]) {
        // temporarily force dropbox file
        NSURL *url = [[fileManager URLsForDirectory:NSUserDirectory inDomains:NSLocalDomainMask] lastObject];
        url = [url URLByAppendingPathComponent:NSUserName()];
        url = [url URLByAppendingPathComponent:@"DropBox"];
        url = [url URLByAppendingPathComponent:@"Congregate"];
        return url;
    } else {
        NSURL *appSupportURL = [[fileManager URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask] lastObject];
        return [appSupportURL URLByAppendingPathComponent:@"Congregate"];        
    }
    // TEMP *************
    
}

// Creates if necessary and returns the managed object model for the application.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel) {
        return _managedObjectModel;
    }
	
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"congregate" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. (The directory for the store is created, if necessary.)
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator) {
        return _persistentStoreCoordinator;
    }
    
    NSManagedObjectModel *mom = [self managedObjectModel];
    if (!mom) {
        NSLog(@"%@:%@ No model to generate a store from", [self class], NSStringFromSelector(_cmd));
        return nil;
    }
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *applicationFilesDirectory = [self applicationFilesDirectory];
    NSError *error = nil;
    
    NSDictionary *properties = [applicationFilesDirectory resourceValuesForKeys:@[NSURLIsDirectoryKey] error:&error];
    
    if (!properties) {
        BOOL ok = NO;
        if ([error code] == NSFileReadNoSuchFileError) {
            ok = [fileManager createDirectoryAtPath:[applicationFilesDirectory path] withIntermediateDirectories:YES attributes:nil error:&error];
        }
        if (!ok) {
            [[NSApplication sharedApplication] presentError:error];
            return nil;
        }
    } else {
        if (![[properties valueForKey:NSURLIsDirectoryKey] boolValue]) {
            // Customize and localize this error.
            NSString *failureDescription = [NSString stringWithFormat:@"Expected a folder to store application data, found a file (%@).", [applicationFilesDirectory path]];
            
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            [dict setValue:failureDescription forKey:NSLocalizedDescriptionKey];
            error = [NSError errorWithDomain:@"DP_CONGREGATE" code:101 userInfo:dict];
            
            [[NSApplication sharedApplication] presentError:error];
            return nil;
        }
    }
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                            [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                            [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];

    NSString *appName = [[NSUserDefaults standardUserDefaults] valueForKey:kDPAppDefaultInstanceKey];
    NSString *localDBName = nil;
    if (appName) {
        localDBName = [NSString stringWithFormat:@"%@.sqlite", appName];
    } else {
        localDBName = @"congregate_localonly.sqlite"; // this should never happen
    }
    NSURL *url = [applicationFilesDirectory URLByAppendingPathComponent:localDBName];
    NSPersistentStoreCoordinator *coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:mom];
    if (![coordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:url options:options error:&error]) {
        [[NSApplication sharedApplication] presentError:error];
        return nil;
    }
    _persistentStoreCoordinator = coordinator;
    
    [_persistentStoreCoordinator unlock];
    
    return _persistentStoreCoordinator;
}

// Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setValue:@"Failed to initialize the store" forKey:NSLocalizedDescriptionKey];
        [dict setValue:@"There was an error building up the data file." forKey:NSLocalizedFailureReasonErrorKey];
        NSError *error = [NSError errorWithDomain:@"DP_CONGREGATE" code:9999 userInfo:dict];
        [[NSApplication sharedApplication] presentError:error];
        return nil;
    }
    
    //setup a context to write with and the main context
    _writingContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [_writingContext setPersistentStoreCoordinator:coordinator];
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setParentContext:_writingContext];
    
    return _managedObjectContext;
}

// Returns the NSUndoManager for the application. In this case, the manager returned is that of the managed object context for the application.
- (NSUndoManager *)windowWillReturnUndoManager:(NSWindow *)window
{
    return [[self managedObjectContext] undoManager];
}

#pragma mark - Alerts

- (void)showAlert:(NSString *)title WithError:(NSError *)error
{
    [self showAlert:title WithText:[error localizedDescription]];
}

- (void)showAlert:(NSString *)title WithText:(NSString *)messageText
{
    NSAlert *alert = [NSAlert alertWithMessageText:title defaultButton:@"OK" alternateButton:nil otherButton:nil informativeTextWithFormat:@"%@", messageText];
    [alert beginSheetModalForWindow:[self window] modalDelegate:self  didEndSelector:@selector(alertDidEnd:returnCode:contextInfo:) contextInfo:nil];
}

- (void) alertDidEnd:(NSAlert *)alert returnCode:(int)returnCode contextInfo:(void *)contextInfo
{
    // do nothing for normal alert closes
}

#pragma mark - Version Upgrades

- (void)_upgradeToVersion:(NSString *)version
{
    //TODO - should be doing something to keep from running the wrong client
    [_loginWindow makeKeyAndOrderFront:nil];
}

- (void)_upgradeAlertDidEnd:(NSAlert *)alert returnCode:(int)returnCode contextInfo:(void *)contextInfo
{
    [self _upgradeDatastore];
}

// wholesale replacement of datastore from remote
- (void)_upgradeDatastore
{
   // for this version we're going to delete the local datastore and reload from remote data
    NSString *appName = [[NSUserDefaults standardUserDefaults] valueForKey:kDPAppDefaultInstanceKey];
    NSString *localDBName = [NSString stringWithFormat:@"%@.sqlite", appName];
    NSURL *applicationFilesDirectory = [self applicationFilesDirectory];
    NSURL *url = [applicationFilesDirectory URLByAppendingPathComponent:localDBName];
    NSError *error = nil;
    [[NSFileManager defaultManager] removeItemAtURL:url error:&error];
    if (error) {
        NSLog(@"upgrade error: %@", [error localizedDescription]);
    }
    [DPSyncEngine setupSyncEngineWithUIContext:[self managedObjectContext] AndWritingContext:_writingContext AutoSync:YES];
    [[DPSyncEngine sharedInstance] updateLastSyncDate:[NSDate dateWithNaturalLanguageString:@"8/1/1995"]];
    [[DPSyncEngine sharedInstance] performInitialSyncInContext:[self managedObjectContext]];
    [[DPSyncEngine sharedInstance] startAutoSync];
    [_loginWindow makeKeyAndOrderFront:nil];
}

#pragma mark - Logging

- (void)redirectNSLogToFile {
    NSString *logPath = [NSString stringWithFormat:@"%@/Dropbox/Congregate/%@_console.log", NSHomeDirectory(), [[NSHost currentHost] name]];
    freopen([logPath cStringUsingEncoding:NSASCIIStringEncoding],"a+",stderr);
}

@end
