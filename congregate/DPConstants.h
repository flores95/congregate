//
//  DPConstants.h
//  congregate
//
//  Created by Mike Flores on 2/2/13.
//  Copyright (c) 2013 digiputty, llc. All rights reserved.
//

// PARSE.com credentials
#define kDPCongregateAppId          @"uEUEadCM1JQsrf1TVzylPzeK2oCjwfoka1fKMTZn"
#define kDPCongregateClientKey      @"ViwJPwE84TZIo2I0pcUqGKV7XCBKTZnvWjJqKzyO"
#define kDPCongregateUsername       @"dpadmin"
#define kDPCongregatePassword       @"742!ADM!3494"

// references to user default data
#define kDPStandAlone               @"standAlone"
#define kDPAppInstancesKey          @"appInstances"
#define kDPAppDefaultInstanceKey    @"appDefaultInstanceName"
#define kDPLastSyncDateKey          @"lastSyncDate"
#define kDPCanSyncKey               @"syncWithRemote"
#define kDPAppVersionKey            @"appVersion"
#define kDPAppUserKey               @"currentUsername"
#define kDPAppNameKey               @"appName"
#define kDPAppDatafile              @"appDatafile"
#define kDPAppOrgNameKey            @"appOrganizationName"
#define kDPAppLoginKey              @"appLogin"
#define kDPAppPasswordKey           @"appPassword"
#define kDPAppIdKey                 @"appId"
#define kDPClientIdKey              @"clientKey"
#define kDPProcessNotification      @"DP_PROCESS_NOTIFICATION"
