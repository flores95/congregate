//
//  dpAppDelegate.h
//  congregate
//
//  Created by Mike Flores on 12/5/12.
//  Copyright (c) 2012 digiputty, llc. All rights reserved.
//
#import <Cocoa/Cocoa.h>
#import "DPUser.h"

@interface dpAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;
@property (assign) IBOutlet NSButton *editModeButton;
@property (assign) IBOutlet NSButton *syncButton;
@property (assign) IBOutlet NSString *familyFileName;
@property (assign) IBOutlet NSString *personFileName;
@property (weak) IBOutlet NSTextField *userNameField;
@property (weak) IBOutlet NSSecureTextField *passwordField;
@property (assign) IBOutlet NSWindow *loginWindow;
@property (weak) IBOutlet NSTextField *loginErrorText;
@property (weak) IBOutlet NSMenu *appMenu;
@property (weak) IBOutlet NSDrawer *systemLoginDrawer;
@property (assign) IBOutlet NSWindow *systemLoginWindow;
@property (weak) IBOutlet NSTextField *systemOrgNameField;
@property (weak) IBOutlet NSTextField *systemOrgUnlockField;
@property (weak) IBOutlet NSTextField *loginSyncMessage;
@property (weak) IBOutlet NSTextField *standAloneShortName;
@property (weak) IBOutlet NSTextField *standAloneOrgName;

@property DPUser *sessionUser;

@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSURL *applicationFilesDirectory;

@property BOOL editMode; // true = edit --- false = no edit

@property NSInteger alertNumber;
@property NSString *labelPrinter;

- (IBAction)saveAction:(id)sender;
- (IBAction)changeEditMode:(id)sender;
- (IBAction)newWindow:(id)sender;
- (IBAction)showImportWindow:(id)sender;
- (IBAction)showPreferences:(id)sender;
- (IBAction)resync:(id)sender;
- (IBAction)login:(id)sender;
- (IBAction)startNetworkedSystem:(id)sender;
- (IBAction)startStandAloneSystem:(id)sender;
- (IBAction)fixIt:(id)sender;

- (NSDate *)lastSyncDate;

@end
