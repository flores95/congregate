//
//  dpMainWindowController.h
//  congregate
//
//  Created by Mike Flores on 1/9/13.
//  Copyright (c) 2013 digiputty, llc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "dpMasterViewController.h"
#import "DPSyncEntity.h"
#import "DPFillView.h"
#import "Tag.h"

@interface dpMainWindowController : NSWindowController <NSDrawerDelegate>

@property (weak) DPSyncEntity *selectedObject;
@property (weak) NSArray *arrangedObjects;
@property BOOL canExport;

@property (weak) IBOutlet DPFillView *mainView;
@property (strong) dpMasterViewController *mainViewController;
@property (weak) IBOutlet NSArrayController *primaryArrayController;
@property (strong) IBOutlet NSArrayController *tagsArrayController;
@property (weak) IBOutlet NSCollectionView *tagsCollectionView;
@property (weak) IBOutlet NSBox *tagsBox;
@property (strong) IBOutlet NSArrayController *systemTagsArrayController;
@property (weak) IBOutlet NSCollectionView *systemTagsCollectionView;
@property (weak) IBOutlet NSBox *systemTagsBox;
@property (strong) IBOutlet NSArrayController *notesArrayController;
@property (strong) IBOutlet NSDrawer *tagsDrawer;
@property (strong) IBOutlet NSDrawer *syncDrawer;
@property (strong) IBOutlet NSDrawer *notesDrawer;
@property (strong) IBOutlet DPFillView *infoLCDView;
@property (strong) IBOutlet NSView *exportView;

- (IBAction)addTag:(id)sender;
- (IBAction)addNote:(id)sender;
- (IBAction)toggleTagDrawer:(id)sender;
- (IBAction)toggleNoteDrawer:(id)sender;
- (IBAction)toggleSyncDrawer:(id)sender;
- (IBAction)newItem:(id)sender;
- (IBAction)delete:(id)sender;
- (void)selectTag:(Tag *)selectedTag;
- (void)selectTagForAll:(Tag *)selectedTag;
- (void)removeTag:(Tag *)selectedTag;

- (IBAction)changeView:(id)sender;
- (IBAction)toggleSyncWithRemote:(id)sender;
- (void)changeViewController:(NSString*)viewName;

@end
