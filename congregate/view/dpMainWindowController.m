//
//  dpMainWindowController.m
//  congregate
//
//  Created by Mike Flores on 1/9/13.
//  Copyright (c) 2013 digiputty, llc. All rights reserved.
//

#import "dpMainWindowController.h"
#import "Tag.h"
#import "Note.h"
#import "Collectible.h"
#import "DPSyncEngine.h"

@interface dpMainWindowController ()

@end

@implementation dpMainWindowController

- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
    }
    
    return self;
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    [self changeViewController:@"People"];
    
    // get things sorted
    NSSortDescriptor *tagSort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    [[self tagsArrayController] setSortDescriptors:[NSArray arrayWithObject:tagSort]];
    NSSortDescriptor *systemTagSort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    [[self systemTagsArrayController] setSortDescriptors:[NSArray arrayWithObject:systemTagSort]];

    
    // canExport is set by subviews that are capable of exporting
    [self setCanExport:NO];
  
    // cleanup collection view appearance
    NSArray *viewBGColors = [NSArray arrayWithObjects:[NSColor colorWithDeviceWhite:1.0 alpha:0], nil];
    [_systemTagsCollectionView setBackgroundColors:viewBGColors];
    [_tagsCollectionView setBackgroundColors:viewBGColors];

    
    // create the info LCD in the toolbar
    /*
    NSView * contentView = self.window.contentView;
    NSSize winSize = contentView.frame.size; // Size of window content view
    NSSize infoSize = _infoLCDView.frame.size; // Size of view we're flipping out

    [_infoLCDView setFrameOrigin:NSMakePoint(winSize.width/2 - infoSize.width/2, winSize.height - 20)];
    [contentView addSubview:_infoLCDView positioned:NSWindowAbove relativeTo:nil];
    */
}

- (void)awakeFromNib
{
    [_mainView setBackground:[NSColor controlBackgroundColor]];
}

- (void)drawerWillOpen:(NSNotification *)notification
{
/*
    // refresh the system tags
    [_systemTagsArrayController fetch:nil];
    for (id sysTag in [[self systemTagsArrayController] arrangedObjects])
    {
        //NSRect frame = NSMakeRect(10, 40, 90, 40);
        NSButton *newButton = [[NSButton alloc] init];
        [newButton setTitle:[(Tag *)sysTag name]];
        [newButton setBezelStyle:NSRoundedBezelStyle];
        [newButton setButtonType:NSMomentaryPushInButton];
        [_systemTagsBox addSubview:newButton];
        [newButton sizeToFit];
        [newButton needsDisplay];
    }
    [_systemTagsBox needsDisplay];
*/
}

- (IBAction)addTag:(id)sender
{
    if (_selectedObject && [_selectedObject isKindOfClass:[Collectible class]])
    {
        Tag *newTag = [Tag createInContext:[_selectedObject managedObjectContext]];
        [(Collectible *)_selectedObject addTagsObject:newTag];
    }
}

- (IBAction)addNote:(id)sender
{
    if (_selectedObject && [_selectedObject isKindOfClass:[Collectible class]])
    {
        Note *newNote = [Note createInContext:[_selectedObject managedObjectContext]];
        [(Collectible *)_selectedObject addNotesObject:newNote];
    }
}

- (IBAction)changeView:(id)sender
{
    NSString *selectedItem = [[sender selectedCell] title];
    //NSLog(selectedItem);
    [self changeViewController:selectedItem];
}

- (IBAction)toggleSyncWithRemote:(id)sender
{
    //bool syncWithRemote = [[NSUserDefaults standardUserDefaults] boolForKey:@"SYNC_WITH_REMOTE"];
    //[[NSUserDefaults standardUserDefaults] setBool:!syncWithRemote forKey:@"SYNC_WITH_REMOTE"];
    //[[DPSyncEngine sharedInstance] performSync];
}

- (IBAction)toggleTagDrawer:(id)sender
{
    [_tagsDrawer toggle:sender];
}

- (IBAction)toggleNoteDrawer:(id)sender
{
    [_notesDrawer toggle:sender];
}

- (IBAction)toggleSyncDrawer:(id)sender
{
    [_syncDrawer toggle:sender];
}

- (IBAction)newItem:(id)sender
{
    if ([_mainViewController respondsToSelector:@selector(newItem:)])
    {
        [_mainViewController newItem:sender];
    }
}

- (IBAction)delete:(id)sender
{
    if ([_mainViewController respondsToSelector:@selector(delete:)])
    {
        [_mainViewController delete:sender];
    }
}

-(void)selectTag:(id)selectedTag
{
    if ([_selectedObject isKindOfClass:[Collectible class]])
    {
        [(Collectible *)_selectedObject addTagsObject:selectedTag];
    }
}

-(void)selectTagForAll:(Tag *)selectedTag
{
    if ([_selectedObject isKindOfClass:[Collectible class]])
    {
        // make sure they really want to add the tag to every item
        NSAlert *alert = [NSAlert alertWithMessageText: @"Add To All"
                                         defaultButton:@"YES"
                                       alternateButton:@"NO"
                                           otherButton:nil
                             informativeTextWithFormat:@"Do you want to add %@ to all %lu items", [selectedTag name], (unsigned long)[_arrangedObjects count]];
        NSInteger button = [alert runModal];
        if (button == NSAlertDefaultReturn) {
            for (Collectible *curItem in _arrangedObjects)
            {
                [curItem addTagsObject:selectedTag];
            }
        }
    }
}

-(void)removeTag:(Tag *)selectedTag
{
    [_tagsArrayController removeObject:selectedTag];
}

- (void)changeViewController:(NSString *)viewName
{
    [self removeView];
    NSString *nibName = [NSString stringWithFormat:@"%s%@%s", "dp", [viewName stringByReplacingOccurrencesOfString:@" " withString:@""], "View"];
    [self setMainViewController:[(dpMasterViewController *)[NSClassFromString([NSString stringWithFormat:@"%@Controller", nibName]) alloc] initWithNibName:nibName bundle:nil] ];
    [_mainView addSubview:[_mainViewController view]];
    [[self mainViewController] addObserver:self forKeyPath:@"selectedObject" options:NSKeyValueObservingOptionNew context:nil];
    [[self mainViewController] addObserver:self forKeyPath:@"arrangedObjects" options:NSKeyValueObservingOptionNew context:nil];
    
}

- (void)removeView
{
    [self setPrimaryArrayController:nil];
    [_mainViewController removeObserver:self forKeyPath:@"selectedObject"];
    [_mainViewController removeObserver:self forKeyPath:@"arrangedObjects"];
    [self clearRelatedData];
    [[_mainViewController view] removeFromSuperview];
    
    if ([self canExport])
    {
        [self setExportView:nil];
        [self setCanExport:NO];
    }
    //[self setTagsPredicate:[NSPredicate predicateWithFormat:@"ANY taggedItems.sourceObjectId == nil"]];
    //[self setNotesPredicate:[NSPredicate predicateWithFormat:@"ANY noteAbout.sourceObjectId == nil"]];
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"selectedObject"])
    {
        if ([_mainViewController selectedObject])
        {
            [self setSelectedObject:[_mainViewController selectedObject]];
            if ([_selectedObject isKindOfClass:[Collectible class]])
            {
                [_systemTagsBox setHidden:NO];
                [_tagsBox setHidden:NO];
                [_tagsArrayController bind:@"contentSet" toObject:[self selectedObject] withKeyPath:@"tags" options:nil];
                [_notesArrayController bind:@"contentSet" toObject:[self selectedObject] withKeyPath:@"notes" options:nil];
            } else {
                [_tagsArrayController unbind:@"contentSet"];
                [_notesArrayController unbind:@"contentSet"];
                [_systemTagsBox setHidden:YES];
                [_tagsBox setHidden:YES];
            }
        } else {
            [_tagsArrayController unbind:@"contentSet"];
            [_notesArrayController unbind:@"contentSet"];
        }
    }
    if ([keyPath isEqualToString:@"arrangedObjects"])
    {
        NSArray *newObjects = [_mainViewController arrangedObjects];
        if (newObjects)
        {
            [self setArrangedObjects:[change objectForKey:NSKeyValueChangeNewKey]];            
        }
    }
}

- (void)clearRelatedData
{
    NSPredicate *tagsPred = [NSPredicate predicateWithFormat:@"ANY taggedItems.sourceObjectId == nil"];
    [_tagsArrayController setFetchPredicate:tagsPred];
    [_tagsArrayController fetch:nil];
    NSPredicate *notesPred = [NSPredicate predicateWithFormat:@"ANY noteAbout.sourceObjectId == nil"];
    [_notesArrayController setFetchPredicate:notesPred];
    [_notesArrayController fetch:nil];
}

@end
