//
//  DPTagButton.m
//  congregate
//
//  Created by Mike Flores on 3/25/13.
//  Copyright (c) 2013 digiputty, llc. All rights reserved.
//

#import "DPTagButton.h"

@implementation DPTagButton

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
    [super drawRect:dirtyRect];
}

- (void)setTitle:(NSString*)aString
{
    //get the current frame
    NSRect frame = [self frame];
    
    //call super
    [super setTitle:aString];
    
    //resize to fit the new string
    [self sizeToFit];
    
    //calculate the difference between the two frame widths
    NSSize newSize = self.frame.size;
    CGFloat widthDelta = newSize.width - NSWidth(frame);
    //set the frame origin
    [self setFrameOrigin:NSMakePoint(NSMinX(self.frame) - widthDelta, NSMinY(self.frame))];
    [self setNeedsDisplay];
}

@end
