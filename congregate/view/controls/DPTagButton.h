//
//  DPTagButton.h
//  congregate
//
//  Created by Mike Flores on 3/25/13.
//  Copyright (c) 2013 digiputty, llc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface DPTagButton : NSButton

@end
