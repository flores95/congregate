//
//  DPCollectionItemView.h
//  congregate
//
//  Created by Mike Flores on 2/17/13.
//  Copyright (c) 2013 digiputty, llc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface DPCollectionItemView : NSView

@property BOOL selected;

@end
