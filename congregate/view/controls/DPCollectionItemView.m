//
//  DPCollectionItemView.m
//  congregate
//
//  Created by Mike Flores on 2/17/13.
//  Copyright (c) 2013 digiputty, llc. All rights reserved.
//

#import "DPCollectionItemView.h"

@implementation DPCollectionItemView

BOOL _selected;

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    
    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
    if ([self selected]) {
        [[NSColor selectedControlColor] set];
        NSRectFill(dirtyRect);
    }
}

- (BOOL)selected
{
    return _selected;
}

- (void)setSelected:(BOOL)selected
{
    if (_selected == selected) {
        return;
    }
    
    _selected = selected;
    [self setNeedsDisplay:YES];
}


@end
