//
//  DPNavButton.h
//  congregate
//
//  Created by Mike Flores on 2/11/13.
//  Copyright (c) 2013 digiputty, llc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface DPNavButton : NSButton
@end
