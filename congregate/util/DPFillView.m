//
//  DPFillView.m
//  congregate
//
//  Created by Mike Flores on 2/9/13.
//  Copyright (c) 2013 digiputty, llc. All rights reserved.
//

#import "DPFillView.h"

@implementation DPFillView

@synthesize background;

-(void)drawRect:(NSRect)dirtyRect
{
    // first fill the whole thing with the stroke color
    [stroke set];
    NSRectFill([self bounds]);
    
    // now fill the body with the fill color
    NSRect rect = [self bounds];
    
    // calculate border
    int xOffSet = 0;
    int yOffSet = 0;
    int widthOffSet = 0;
    int heightOffSet = 0;
    if (borderTop)      { heightOffSet += strokeWidth; }
    if (borderBottom)   { yOffSet += strokeWidth; heightOffSet += strokeWidth; }
    if (borderRight)    { widthOffSet += strokeWidth;  }
    if (borderLeft)     { xOffSet += strokeWidth; widthOffSet += strokeWidth;  }
    
    NSRect bodyRect = NSMakeRect(rect.origin.x+xOffSet, rect.origin.y+yOffSet, rect.size.width-widthOffSet, rect.size.height-heightOffSet);

    if (gradient) {
        NSGradient *grd = gradientWithColor(background);
        [grd drawInRect:bodyRect angle:90];
    } else {
        [background set];
        NSRectFill(bodyRect);
    }
}

static NSGradient *gradientWithColor(NSColor *mainColor) {
    // calculate secondary color
    NSColor *secondaryColor = [mainColor highlightWithLevel:.6];
    NSArray *colors = [NSArray arrayWithObjects:mainColor, secondaryColor, nil];
    const CGFloat locations[4] = { 0.0, 1.0 };
    return [[NSGradient alloc] initWithColors:colors atLocations:locations colorSpace:[NSColorSpace sRGBColorSpace]];
}

@end
