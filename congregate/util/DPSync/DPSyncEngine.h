//
//  DPSyncEngine.h
//  congregate
//
//  Created by Mike Flores on 1/17/13.
//  Copyright (c) 2013 digiputty, llc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "DPConstants.h"


@interface DPSyncEngine : NSObject

@property NSMutableDictionary *appInfo;
@property NSManagedObjectContext *UIContext;
@property NSManagedObjectContext *writingContext;

// primary init interface
+ (void)setupSyncEngineWithUIContext:(NSManagedObjectContext *)uContext AndWritingContext:(NSManagedObjectContext *)wContext AutoSync:(BOOL)autoSync;

+ (DPSyncEngine *)sharedInstance;

- (void)startAutoSync;
- (void)performInitialSyncInContext:(NSManagedObjectContext *)context;
- (void)updateLastSyncDate:(NSDate *)syncDate;

@end
