 //
//  DPSyncEngine.m
//  congregate
//
//  Created by Mike Flores on 1/17/13.
//  Copyright (c) 2013 digiputty, llc. All rights reserved.
//

#import "DPSyncEngine.h"
#import "DPSyncEntity.h"
#import "DPBaseEntity.h"

@implementation DPSyncEngine

@synthesize appInfo = _appInfo;

BOOL _syncInProgress = NO;
BOOL _initialSyncInProgress = NO;
BOOL _connected = NO;

#pragma mark - Sync Setup

+ (void)setupSyncEngineWithUIContext:(NSManagedObjectContext *)uContext AndWritingContext:(NSManagedObjectContext *)wContext AutoSync:(BOOL)autoSync
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *appInstances = (NSDictionary *)[defaults objectForKey:kDPAppInstancesKey];
    NSDictionary *appInfo = (NSMutableDictionary *)[appInstances objectForKey:[defaults valueForKey:kDPAppDefaultInstanceKey]];
    
    //BOOL standAlone = (BOOL)[appInfo valueForKey:kDPStandAlone];
    // TEMP *************
    // temporarily disable sync to remote
    BOOL standAlone = YES;
    autoSync = NO;
    // TEMP *************
    if (standAlone)
    {
        _connected = NO;
    } else {   
        // setup PARSE stuff
        //[Parse setApplicationId:@"Sg73v0xXJbNCqRkH1U2ZyChoSDydaxWh0y2hCzaT" clientKey:@"3JwGzXC0p5fk4Ye9q5aV71aFpIBhNlH1ZBuGwm5m"]; //congregate_test
        [Parse setApplicationId:[appInfo valueForKey:kDPAppIdKey] clientKey:[appInfo valueForKey:kDPClientIdKey]]; 
        
        //TODO - Need mechanism to connect if not connected
        NSError *error = nil;
        PFUser *parseUser = [PFUser logInWithUsername:[appInfo valueForKey:kDPAppLoginKey]
                                             password:[appInfo valueForKey:kDPAppPasswordKey]
                                                error:&error];
        //PFUser *parseUser = [PFUser logInWithUsername:[appInfo valueForKey:kDPAppLoginKey]
        //                                     password:@"LOCKED"
        //                                        error:&error];
        if (error)
        {
            NSLog(@" PROBLEM LOGGING IN TO PARSE %@", [error description]);
            _connected = NO;
            
            
        } else {
            
            // make sure the client is the right version
            NSString *appVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
            if ([appVersion isEqualToString:[parseUser valueForKey:@"appVersion"]])
            {
                PFACL *parseACL = [PFACL ACLWithUser:[PFUser currentUser]];
                [parseACL setWriteAccess:YES forUser:[PFUser currentUser]];
                [parseACL setReadAccess:YES forUser:[PFUser currentUser]];
                [PFACL setDefaultACL:parseACL withAccessForCurrentUser:YES];
                
                _connected = YES;
            } else {
                NSLog(@" CLIENT(%@) and DATASTORE(%@) versions do not match", appVersion, [parseUser valueForKey:@"appVersion"]);
                _connected = NO;
            }
        }
    }
    DPSyncEngine *shared = [DPSyncEngine sharedInstance];
    // set the appInfo for the syncEngine
    
    [shared setAppInfo:[NSMutableDictionary dictionaryWithDictionary:appInfo]];
    [shared setUIContext:uContext];
    [shared setWritingContext:wContext];
    
    if (autoSync)
    {
        [shared startAutoSync];
    }
    
}

+ (DPSyncEngine *)sharedInstance
{
    static dispatch_once_t pred;
    static DPSyncEngine *shared = nil;
    
    dispatch_once(&pred, ^{
        shared = [[DPSyncEngine alloc] init];
    });
    
    return shared;
}

#pragma mark - MOC Notifications

- (void)_contextWasSaved:(NSNotification *)notification {
    //[self performSync];
}

- (void)_contextWillSave:(NSNotification *)notification {

    if (_syncInProgress)
    {
        return;
    }
    
    NSManagedObjectContext* context = [notification object];
    if (![context isEqual:_UIContext])
    {
        return;
    }
    
    _syncInProgress = YES;
    // get a temporary child copy of the main context to do the work with
    NSManagedObjectContext *syncContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [syncContext setUndoManager:nil];
    [syncContext setParentContext:_UIContext];
    
    // mark remote copies deleted for locally deleted entities
    // for deleted object we have to get the remote right now because it will be gone in the background processing
    NSMutableArray *remoteObjectsToDelete = [[NSMutableArray alloc] init];
    for (id obj in [_UIContext deletedObjects])
    {
        if ([obj isKindOfClass:[DPSyncEntity class]])
        {
            if ([(DPSyncEntity *)obj updatedFromRemote]) {
                [(DPSyncEntity *)obj setUpdatedFromRemote:NO];
            } else {
                [remoteObjectsToDelete addObject:[(DPSyncEntity *)obj remoteObject]];
            }
        }
    }
    [syncContext performBlock:^{
        for (PFObject *parseObj in remoteObjectsToDelete)
        {
            NSLog(@" XXXXXXXXXX DELETING %@ (%@) FROM SERVER", [parseObj parseClassName], [parseObj objectId]);
            [parseObj setObject:[NSNumber numberWithBool:YES] forKey:@"deleted"];
            NSError *error = nil;
            if ([parseObj save:&error])
            {
                NSLog(@" +++++++++ %@ (%@) DELETED ON SERVER", [parseObj parseClassName], [parseObj valueForKey:@"objectId"]);
            } else {
                [self _reportProgress:[NSString stringWithFormat:@" :: :: :: ERROR SAVING REMOTE/DELETE(%@/%@): %@", [parseObj parseClassName], [parseObj objectId], [error description]]];
            }
        }
    }];
    
    // push new local entities
    NSSet *insertedKeys = [[_UIContext insertedObjects] valueForKey:@"sourceObjectId"];
    [syncContext performBlock:^{
        for (id sourceId in insertedKeys)
        {
            DPSyncEntity *obj = [DPSyncEntity findFirstByAttribute:@"sourceObjectId" WithValue:sourceId InContext:syncContext];
            if (obj && ![obj objectId] && ![obj updatedFromRemote])
            {
                PFObject *parseObj = [obj remoteObject];
                NSError *error = nil;
                //TODO what happens if they are not connected to the network?
                if ([parseObj save:&error])
                {
                    NSLog(@" +++++++++ %@ (%@) SAVED ON SERVER", [[obj class] entityName], [parseObj objectId]);
                    NSString *newObjectId = [parseObj objectId];
                    [obj setValue:newObjectId forKey:@"objectId"];
                    [obj setValue:[NSDate date] forKey:@"updatedAt"]; //BUG - Should have come from parse save
                    [obj setValue:[obj valueForKey:@"sourceCreatedAt"] forKey:@"createdAt"]; //BUG - Should have come from parse save
                } else {
                    [self _reportProgress:[NSString stringWithFormat:@" :: :: :: ERROR SAVING REMOTE(%@/%@): %@", [parseObj parseClassName], [parseObj objectId], [error description]]];
                }                
            }
        }
    }];
    
    // push local updates
    NSSet *updatedKeys = [[_UIContext updatedObjects] valueForKey:@"sourceObjectId"];
    [syncContext performBlock:^{
        for (id sourceId in updatedKeys)
        {
            DPSyncEntity *obj = [DPSyncEntity findFirstByAttribute:@"sourceObjectId" WithValue:sourceId InContext:syncContext];
            if (obj && ![obj updatedFromRemote])
            {
                PFObject *localParseObj = [obj remoteObject];
                // first find out if it was updated by someone else more recently
                PFObject *remoteParseObj = [PFQuery getObjectOfClass:[localParseObj parseClassName] objectId:[localParseObj objectId]];
                if (!remoteParseObj || [[localParseObj valueForKey:@"sourceUpdatedAt"] isGreaterThan:[remoteParseObj updatedAt]])
                {

                    NSError *error = nil;
                    if ([localParseObj save:&error])
                    {
                        NSLog(@" +++++++++ %@ (%@) UPDATED ON SERVER", [obj className], [obj valueForKey:@"objectId"]);
                        [obj setValue:[NSDate date] forKey:@"updatedAt"]; //BUG - Should have come from parse save
                        /*
                         if ([[localParseObj parseClassName] isEqualToString:@"Person"])
                        {
                            NSLog(@" $$$$$$$$ Tags: %@", [localParseObj valueForKey:@"Tags"]);
                        }
                         */
                    } else {
                        [self _reportProgress:[NSString stringWithFormat:@" :: :: :: ERROR SAVING REMOTE(%@/%@): %@", [localParseObj parseClassName], [localParseObj objectId], [error description]]];
                    }
                }
            }
        }
    }];
    
    _syncInProgress = NO;
    
    // pull remote changes
    // TODO ... this should be done in background on a schedule to avoid slow save times
    //NSArray *changesFromRemote = [self pullRemoteChanges];
    //[self updateLocalWithRemoteChanges:changesFromRemote];
    //[[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"lastSyncDate"];
    
}

#pragma mark - KVO for DPSyncEntity Objects

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    /*
    //NSKeyValueChange kind = [[change objectForKey:NSKeyValueChangeKindKey] intValue];
    if (!_initialSyncInProgress && [object isKindOfClass:[DPBaseEntity class]] && ![keyPath isEqualToString:@"sourceUpdatedAt"] && ![keyPath isEqualToString:@"modifiedBy"])
    {
        NSManagedObjectContext *moc = [(NSManagedObject *)object managedObjectContext];
        if ([moc isEqual:_UIContext])
        {
            NSString *currentUsername = [[NSUserDefaults standardUserDefaults] valueForKey:kDPAppUserKey];
            [(DPBaseEntity *)object setModifiedBy:currentUsername];
            [(DPSyncEntity *)object setSourceUpdatedAt:[NSDate date]];
        }
    }
*/
}

#pragma mark - Perform Sycn Logic
-(void)startAutoSync
{
    // only sync and check for remote if connected
    if (_connected)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_contextWasSaved:) name:NSManagedObjectContextDidSaveNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_contextWillSave:) name:NSManagedObjectContextWillSaveNotification object:nil];
        // perform a full sync to start
        [self _syncLocalAndRemote];
        // create a background job that checks for remote updates
        [NSTimer scheduledTimerWithTimeInterval:60*7 target:self selector:@selector(_syncRemoteChanges) userInfo:nil repeats:YES];
    }
}

- (void)_syncLocalAndRemote
{
    if (!_connected)
    {
        return;
    }
    
    if (_syncInProgress) {
        return;
    }
    
    _syncInProgress = YES;
    NSManagedObjectContext *syncContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [syncContext setUndoManager:nil];
    [syncContext setParentContext:_UIContext];
    
    [syncContext performBlock:^{
        @try {
            NSArray *changesFromRemote = [self _pullRemoteChangesForContext:syncContext];
            [self _pushLocalChangesForContext:syncContext];
            [self _updateLocalContext:syncContext WithRemoteChanges:changesFromRemote];
            [self _updateLastSyncDate];
        }
        @catch (NSException *exception) {
            [self _reportProgress:[NSString stringWithFormat:@"ERROR IN SYNC : %@", [exception description]]];
        }
        @finally {
            // save local context
            [self _saveContext:syncContext];
        }
    [self _reportProgress:[NSString stringWithFormat:@"############################### SYNC COMPLETE ###################################"]];
    }];
    
    _syncInProgress = NO;
}

- (void)_syncRemoteChanges
{
    if (!_connected)
    {
        return;
    }

    if (_syncInProgress) {
        return;
    }
    
    //TODO - Should pull down users first so they can login and then get the rest in the background
    NSManagedObjectContext *syncContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [syncContext setUndoManager:nil];
    [syncContext setParentContext:_UIContext];
    
    [syncContext performBlock:^{
        _syncInProgress = YES;
        @try {
            NSArray *changesFromRemote = [self _pullRemoteChangesForContext:syncContext];
            [self _updateLocalContext:syncContext WithRemoteChanges:changesFromRemote];
            [self _updateLastSyncDate];
        }
        @catch (NSException *exception) {
            [self _reportProgress:[NSString stringWithFormat:@"ERROR IN SYNC : %@", [exception description]]];
        }
        @finally {
            // save local context
            [self _saveContext:syncContext];
        }
        [self _reportProgress:[NSString stringWithFormat:@"############################### SYNC COMPLETE ###################################"]];
        _syncInProgress = NO;
    }];
    
    
}

- (void)performInitialSyncInContext:(NSManagedObjectContext *)context
{
    if (!_connected)
    {
        return;
    }
    
    if (_syncInProgress) {
        return;
    }
    
    _syncInProgress = YES;
    _initialSyncInProgress = YES;
    @try {
        NSArray *changesFromRemote = [self _pullRemoteChangesForContext:context];
        [self _updateLocalContext:context WithRemoteChanges:changesFromRemote];
        [self _updateLastSyncDate];
    }
    @catch (NSException *exception) {
        [self _reportProgress:[NSString stringWithFormat:@"ERROR IN SYNC : %@", [exception description]]];
    }
    @finally {
        // save local context
        [self _saveContext:context];
    }
    [self _reportProgress:[NSString stringWithFormat:@"############################### SYNC COMPLETE ###################################"]];
    _syncInProgress = NO;
    _initialSyncInProgress = NO;
    
    
}

#pragma mark - Pushing Local Changes

- (void)_pushLocalChangesForContext:(NSManagedObjectContext *)context
{
    NSDate *lastSyncDate = [_appInfo objectForKey:kDPLastSyncDateKey];
    [self _reportProgress:[NSString stringWithFormat:@"LAST MODIFIED DATE: %@", lastSyncDate]];

    //------------------------------------------------------------------------------------------
    // Find all entities that were created here since the last update
    //------------------------------------------------------------------------------------------
    NSPredicate *newPred = [NSPredicate predicateWithFormat:@"deleted != 1 AND objectId == nil"];
    NSArray *newLocalEntities = [DPSyncEntity findAllWithPredicate:newPred InContext:context];
    
   
    //------------------------------------------------------------------------------------------
    // Now find all entities that were updated here since the last update
    //------------------------------------------------------------------------------------------
    NSPredicate *modPred = [NSPredicate predicateWithFormat:@"objectId != nil AND sourceUpdatedAt > %@", lastSyncDate];
    NSArray *updatedLocalEntities = [DPSyncEntity findAllWithPredicate:modPred InContext:context];
    

    [self _pushLocalChangesWithEntities:newLocalEntities];
    [self _pushLocalChangesWithEntities:updatedLocalEntities];
    
}
    
- (void)_pushLocalChangesWithEntities:(NSArray *)localChanges
{
   
    //------------------------------------------------------------------------------------------
    // create new Parse Objcets and save them
    // this does not create relationships because we need Parse objectIds for that
    //------------------------------------------------------------------------------------------
    NSMutableArray *parseObjectsToUpdate = [[NSMutableArray alloc] initWithCapacity:[localChanges count]];
    [self _reportProgress:[NSString stringWithFormat:@"ENTITY COUNT: %lu", (unsigned long)[localChanges count]]];
    [self _reportProgress:[NSString stringWithFormat:@"CREATING REMOTE OBJECTS"]];
    for (DPSyncEntity *localEntity in localChanges)
    {
        if (![[localEntity entity] isAbstract])
        {
            [self _reportProgress:[NSString stringWithFormat:@"LOCAL ENTITY TO PUSH TO REMOTE: %@/%@", [[localEntity entity] name], [localEntity valueForKey:@"objectId"]]];
            [parseObjectsToUpdate addObject:[localEntity remoteObject]];
        }
    }
    
    // save them one at a time to keep both stores consistent and to update objectId, etc.
    [self _reportProgress:[NSString stringWithFormat:@"             SAVING REMOTE OBJECTS"]];
    for (PFObject *parseObj in parseObjectsToUpdate)
    {
        // first find out if it was updated by someone else more recently
        PFObject *remoteParseObj = [PFQuery getObjectOfClass:[parseObj parseClassName] objectId:[parseObj objectId]];
        if (!remoteParseObj || [[parseObj valueForKey:@"sourceUpdatedAt"] isGreaterThan:[remoteParseObj updatedAt]])
        {
            NSError *error = nil;
            if ([parseObj save:&error])
            {
                NSString *newObjectId = [parseObj objectId];
                NSString *sourceId = [parseObj valueForKey:@"sourceObjectId"];
                for (id localEntity in localChanges)
                {
                    if ([sourceId isEqualToString:[localEntity valueForKey:@"sourceObjectId"]])
                    {
                        [localEntity setValue:newObjectId forKey:@"objectId"];
                        [localEntity setValue:[NSDate date] forKey:@"updatedAt"]; //BUG - Should have come from parse save
                        [localEntity setValue:[localEntity valueForKey:@"sourceCreatedAt"] forKey:@"createdAt"]; //BUG - Should have come from parse save
                    }
                }            
            } else {
                [self _reportProgress:[NSString stringWithFormat:@" :: :: :: ERROR SAVING REMOTE(%@/%@): %@", [parseObj parseClassName], [parseObj objectId], [error description]]];
            }
        }
    }
}

#pragma mark - Pull Remote Changes

- (NSArray *)_pullRemoteChangesForContext:(NSManagedObjectContext *)context
{
    [self _reportProgress:[NSString stringWithFormat:@"CHECKING FOR REMOTE UPDATES"]];
    // get all the syncable entities
    NSArray *syncableEntities = [self _subentitiesOfEntity:@"DPSyncEntity" InContext:context];
    NSMutableArray *remoteUpdates = [[NSMutableArray alloc] init];
    
    // get any remote updates for all syncable entities
    // this only creates basic entity w/o relationships
    for (NSString *key in syncableEntities)
    {
        if (![[NSEntityDescription entityForName:key inManagedObjectContext:context] isAbstract])
        {
            NSArray *entityUpdates = [NSClassFromString(key) getUpdatedRemoteObjects];
            [self _reportProgress:[NSString stringWithFormat:@"%lu REMOTE UPDATES FOR %@", (unsigned long)[entityUpdates count], key]];
            [remoteUpdates addObjectsFromArray:entityUpdates];
        }
    }
    return remoteUpdates;
}

- (void)_updateLocalContext:(NSManagedObjectContext *)context WithRemoteChanges:(NSArray *)remoteUpdates
{
    [self _reportProgress:[NSString stringWithFormat:@"UPDATE ENTITY COUNT: %lu", (unsigned long)[remoteUpdates count]]];
    NSMutableArray *localObjects = [NSMutableArray arrayWithCapacity:[remoteUpdates count]];
    //first create the basic local entities
    for (PFObject *remoteObj in remoteUpdates)
    {
        NSLog(@"ABOUT TO UPDATE OBJECT: %@(%@)", [remoteObj parseClassName], [remoteObj valueForKey:@"sourceObjectId"]);
        id localObj = [NSClassFromString([remoteObj parseClassName]) createOrUpdateFromRemoteObject:remoteObj InContext:context];
        [localObj setUpdatedFromRemote:YES];
        [localObjects addObject:localObj];
    }
    
    // now update the local relationships from the remote data
    [self _reportProgress:[NSString stringWithFormat:@"UPDATE RELATIONSHIPS"]];
    for (id localObj in localObjects)
    {
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"sourceObjectId == %@", [localObj valueForKey:@"sourceObjectId"]];
        NSArray *matchingRemoteObjects = [remoteUpdates filteredArrayUsingPredicate:pred];
        [localObj addRelationshipsFromRemote:[matchingRemoteObjects objectAtIndex:0]];
    }
    
    // now delete any objects marked as deleted
    [self _reportProgress:[NSString stringWithFormat:@"REMOVE DELETED OBJECTS"]];
    for (id localObj in localObjects)
    {
        if (![localObj isDeleted]) {
            if ([(DPSyncEntity *)localObj deletedValue])
            {
                NSLog(@"ABOUT TO DELETE OBJECT: %@(%@)", [[localObj entity] name], [localObj valueForKey:@"sourceObjectId"]);
                [context deleteObject:localObj];
            }
        }
    }
}

- (NSArray *)_subentitiesOfEntity:(NSString *)entityName InContext:(NSManagedObjectContext *)context
{
    NSMutableArray *results = [[NSMutableArray alloc] init];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:entityName inManagedObjectContext:context];
    NSDictionary *subEntities = [entityDescription subentitiesByName];
    
    for (NSString *key in [subEntities keyEnumerator])
    {
        [results addObject:key];
        [results addObjectsFromArray:[self _subentitiesOfEntity:key InContext:context]];
    }
    // get all remote objects for this class that have updates
    return results;

}

#pragma mark - Local Save

- (void)_saveContext:(NSManagedObjectContext *)context
{
    if ([context hasChanges])
    {
        NSError *error = nil;
        if (![context save:&error]) {
            [self _reportProgress:[NSString stringWithFormat:@"Unable to save SYNC context in %@ ... error:%@", NSStringFromClass([self class]), [error description]]];
        }
        
        // this context is a child of the UIContext so we need to save it and the writing context as well
        if (![_UIContext save:&error]) {
            [self _reportProgress:[NSString stringWithFormat:@"Unable to save UI context in %@ ... error:%@", NSStringFromClass([self class]), [error description]]];
        }
        [_writingContext performBlock:^{
            NSError *error = nil;
            if (![_writingContext save:&error]) {
                NSLog(@"Unable to save DISK context ... error:%@", [error description]);
            }
        }];
        
    }
}

#pragma mark - Progress Reporting

- (void)_reportProgress:(NSString *)progress
{
    NSLog(@"%@", progress);
    [[NSNotificationCenter defaultCenter] postNotificationName:kDPProcessNotification object:progress];
}

#pragma mark - Update Sync Date 

- (void)_updateLastSyncDate
{
    if (!_connected)
    {
        return;
    }
    
    [self updateLastSyncDate:[NSDate date]];
}

- (void)updateLastSyncDate:(NSDate *)syncDate
{
    if (!_connected)
    {
        return;
    }
    
    [_appInfo setValue:syncDate forKey:kDPLastSyncDateKey];
    
    //make sure userDefaults are updated
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *appInstances = [NSMutableDictionary dictionaryWithDictionary:[defaults objectForKey:kDPAppInstancesKey]];
    [appInstances removeObjectForKey:[defaults valueForKey:kDPAppDefaultInstanceKey]];
    [appInstances setValue:_appInfo forKey:[defaults valueForKey:kDPAppDefaultInstanceKey]];
    [defaults setValue:appInstances forKey:kDPAppInstancesKey];
}

@end
