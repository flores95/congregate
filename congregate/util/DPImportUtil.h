//
//  DPImportUtil.h
//  congregate
//
//  Created by Mike Flores on 1/24/13.
//  Copyright (c) 2013 digiputty, llc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface DPImportUtil : NSObject

- (void)importFamilyFile:(NSString *)familyFileName AndPersonFile:(NSString *)personFileName InContext:(NSManagedObjectContext *)context;
@end
