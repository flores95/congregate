//
//  DPFillView.h
//  congregate
//
//  Created by Mike Flores on 2/9/13.
//  Copyright (c) 2013 digiputty, llc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface DPFillView : NSView
{
    NSColor *background;
    NSColor *stroke;
    int     strokeWidth;
    bool    gradient;
    bool    borderTop;
    bool    borderBottom;
    bool    borderLeft;
    bool    borderRight;
}

@property (strong) NSColor *background;

@end
