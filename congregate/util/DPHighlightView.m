//
//  DPHighlightView.m
//  congregate
//
//  Created by Mike Flores on 2/9/13.
//  Copyright (c) 2013 digiputty, llc. All rights reserved.
//

#import "DPHighlightView.h"

@implementation DPHighlightView

- (void)mouseEntered:(NSEvent *)theEvent
{
    [[self layer] setBackgroundColor:[[NSColor selectedTextBackgroundColor] CGColor]];
    [super mouseEntered:theEvent];
}

- (void)mouseExited:(NSEvent *)theEvent
{
    [[self layer] setBackgroundColor:[[NSColor controlBackgroundColor] CGColor]];
    [super mouseExited:theEvent];
}

@end
