//
//  DPImportUtil.m
//  congregate
//
//  Created by Mike Flores on 1/24/13.
//  Copyright (c) 2013 digiputty, llc. All rights reserved.
//

#import "DPImportUtil.h"
#import <CoreData/CoreData.h>
#import "CHCSVParser.h"
#import "Person.h"
#import "Family.h"
#import "FamilyMember.h"
#import "EmailAddress.h"
#import "PhoneNumber.h"
#import "MailingAddress.h"
#import "SocialAddress.h"
#import "Note.h"
#import "Tag.h"
#import "ItemCollection.h"
#import "Collectible.h"
#import "DPUser.h"

@implementation DPImportUtil

Tag *noMailingTag, *newsletterTag, *welcomeClassAttendedTag, *welcomeClassInviteTag, *welcomeClassInvitedTag, *widowedTag, *eNewsletterTag;
DPUser *sysDPUser;
NSMutableDictionary *families;

- (void)importFamilyFile:(NSString *)familyFileName AndPersonFile:(NSString *)personFileName InContext:(NSManagedObjectContext *)context
{
    if (!familyFileName && !personFileName) {
        [self reportProgress:[NSString stringWithFormat:@"import files not selected"]];
        return;
    }
    
    // create base data
    // TODO - this should be done elsewhere
    [self createBaseDataInContext:context];
    
    if (familyFileName) {
        // the CSV array
        NSArray *familiesArray = [NSArray arrayWithContentsOfCSVFile:familyFileName];
        // initialize an array to cache CORE DATA Faimilies for linking people to later
        families = [[NSMutableDictionary alloc] initWithCapacity:[familiesArray count]];
        for (NSArray *fam in familiesArray) {
            [self createNewFamilyWithKey:[fam objectAtIndex:0] name:[fam objectAtIndex:1] lastName:[fam objectAtIndex:2] InContext:context];
        }
    }
    if (personFileName) {
        
        NSArray *peopleArray = [NSArray arrayWithContentsOfCSVFile:personFileName];
        [self reportProgress:[NSString stringWithFormat:@"------"]];
        [self reportProgress:[NSString stringWithFormat:@"Number of people to import: %lu", [peopleArray count]]];
        [self reportProgress:[NSString stringWithFormat:@"------"]];
        for (NSArray *person in peopleArray) {
            [self createNewPersonWithArray:person InContext:context];
        }
    }
}

- (void)createBaseDataInContext:(NSManagedObjectContext *)context
{
    sysDPUser = [DPUser findUser:@"_system_" InContext:context];
    if (!sysDPUser)
    {
        //create the system user
        sysDPUser = [DPUser createInContext:context];
        [sysDPUser setUsername:@"_system_"];
        [sysDPUser setPassword:@"$system$"];
    }
    // some tags and collections for later
    noMailingTag                    = [Tag createInContext:context];
    noMailingTag.name               = @"OFFICE:NO MAILING";
    
    newsletterTag                   = [Tag createInContext:context];
    newsletterTag.name              = @"OFFICE:NEWSLETTER";
    
    welcomeClassAttendedTag         = [Tag createInContext:context];
    welcomeClassAttendedTag.name    = @"OFFICE:WELCOME CLASS:ATTENDED";
    
    welcomeClassInviteTag           = [Tag createInContext:context];
    welcomeClassInviteTag.name      = @"OFFICE:WELCOME CLASS:SEND INVITE";
    
    welcomeClassInvitedTag          = [Tag createInContext:context];
    welcomeClassInvitedTag.name     = @"OFFICE:WELCOME CLASS:INVITED";
    
    widowedTag                      = [Tag createInContext:context];
    widowedTag.name                 = @"OFFICE:WIDOWED";
    
    eNewsletterTag                  = [Tag createInContext:context];
    eNewsletterTag.name             = @"OFFICE:E-NEWSLETTER";
    
    // make the _SYSTEM_ user owner of these tags
    [sysDPUser addUserTagsObject:noMailingTag];
    [sysDPUser addUserTagsObject:newsletterTag];
    [sysDPUser addUserTagsObject:welcomeClassAttendedTag];
    [sysDPUser addUserTagsObject:welcomeClassInviteTag];
    [sysDPUser addUserTagsObject:welcomeClassInvitedTag];
    [sysDPUser addUserTagsObject:widowedTag];
    [sysDPUser addUserTagsObject:eNewsletterTag];
}

- (void)createNewPersonWithArray:(NSArray *)personArray InContext:(NSManagedObjectContext *)context
{
    [self reportProgress:[NSString stringWithFormat:@"------"]];
    
    if ([personArray count] != 54) {
        return;
    }
    
    // get the appropriat family
    Family *fam = [families objectForKey:[personArray objectAtIndex:23]];
    
    Person *newPerson = [Person createInContext:context];
    // adult or child?
    if ([[personArray objectAtIndex:0] isEqualToString:@"Adult"]) {
        newPerson.adultValue        = YES;
        newPerson.memberValue        = [[personArray objectAtIndex:3] boolValue];
        newPerson.title              = [personArray objectAtIndex:9];
        //newPerson.membership         = [NSDate dateWithNaturalLanguageString:[personArray objectAtIndex:19]];
        //newPerson.backgroundCheck    = [NSDate dateWithNaturalLanguageString:[personArray objectAtIndex:22]];
        //newPerson.anniversary        = [NSDate dateWithNaturalLanguageString:[personArray objectAtIndex:14]];
        newPerson.marriedValue       = [[personArray objectAtIndex:12] boolValue];
        if (fam) {
            FamilyMember *newMember = [FamilyMember createInContext:context];
            newMember.role = @"Adult";
            newMember.personDetail = newPerson;
            [fam addMembersObject:newMember];
        }
    } else {
        newPerson.adultValue        = NO;
        newPerson.specialNeeds       = [personArray objectAtIndex:46];
        newPerson.school             = [personArray objectAtIndex:42];
        // should we calculate grade here?
        if (fam) {
            [self reportProgress:[NSString stringWithFormat:@"adding child to family %@", [personArray objectAtIndex:23]]];
            FamilyMember *newMember = [FamilyMember createInContext:context];
            newMember.role = @"Child";
            newMember.personDetail = newPerson;
            [fam addMembersObject:newMember];
        }
    }
    //newPerson.objectId           = [personArray objectAtIndex:1];
    newPerson.churchStatus       = [personArray objectAtIndex:2];
    newPerson.activeValue        = [[personArray objectAtIndex:4] boolValue];
    newPerson.firstName          = [personArray objectAtIndex:5];
    newPerson.lastName           = [personArray objectAtIndex:6];
    newPerson.middleName         = [personArray objectAtIndex:7];
    newPerson.name               = [NSString stringWithFormat:@"%@ %@", newPerson.firstName, newPerson.lastName];
    
    // only add if it doesn't match first name
    if (![newPerson.firstName isEqualToString:[personArray objectAtIndex:8]]) {
        newPerson.nickName = [personArray objectAtIndex:8];
    }
    
    newPerson.suffix             = [personArray objectAtIndex:10];
    newPerson.gender             = [personArray objectAtIndex:11];
    newPerson.birthday           = [NSDate dateWithNaturalLanguageString:[personArray objectAtIndex:13]];
    //newPerson.baptism            = [NSDate dateWithNaturalLanguageString:[personArray objectAtIndex:15]];
    //newPerson.bornAgain          = [NSDate dateWithNaturalLanguageString:[personArray objectAtIndex:16]];
    //newPerson.deceased           = [NSDate dateWithNaturalLanguageString:[personArray objectAtIndex:17]];
    //newPerson.dedication         = [NSDate dateWithNaturalLanguageString:[personArray objectAtIndex:18]];
    //newPerson.firstVisit         = [NSDate dateWithNaturalLanguageString:[personArray objectAtIndex:20]];
    newPerson.sourceUpdatedAt    = [NSDate dateWithNaturalLanguageString:[personArray objectAtIndex:21]];
    
    [self reportProgress:[NSString stringWithFormat:@"basic info added for : %@", newPerson.name]];
    
    // if there is an address add it as home address
    if (![[personArray objectAtIndex:24] isEqualToString:@""]) {
        MailingAddress *newAddress = [MailingAddress createInContext:context];
        newAddress.label = @"Home";
        newAddress.addressLine1 = [personArray objectAtIndex:24];
        newAddress.addressLine2 = [personArray objectAtIndex:25];
        newAddress.city         = [personArray objectAtIndex:26];
        newAddress.state        = [personArray objectAtIndex:27];
        newAddress.postalCode   = [personArray objectAtIndex:28];
        newAddress.country      = [personArray objectAtIndex:29];
        [newPerson addMailingAddressesObject:newAddress];
    }
    // if there is a secondary address add it as second home address
    if (![[personArray objectAtIndex:30] isEqualToString:@""]) {
        MailingAddress *newAddress = [MailingAddress createInContext:context];
        newAddress.label = @"Secondary";
        newAddress.addressLine1 = [personArray objectAtIndex:30];
        newAddress.addressLine2 = [personArray objectAtIndex:31];
        newAddress.city         = [personArray objectAtIndex:32];
        newAddress.state        = [personArray objectAtIndex:33];
        newAddress.postalCode   = [personArray objectAtIndex:34];
        newAddress.country      = [personArray objectAtIndex:35];
        [newPerson addMailingAddressesObject:newAddress];
    }
    
    // if there is a phone add it as home
    if (![[personArray objectAtIndex:36] isEqualToString:@""]) {
        PhoneNumber *newPhone = [PhoneNumber createInContext:context];
        newPhone.label = @"Home";
        newPhone.number = [personArray objectAtIndex:36];
        [newPerson addPhoneNumbersObject:newPhone];
    }
    // if there is a phone add it as secondary
    if (![[personArray objectAtIndex:37] isEqualToString:@""]) {
        PhoneNumber *newPhone = [PhoneNumber createInContext:context];
        newPhone.label = @"Home2";
        newPhone.number = [personArray objectAtIndex:37];
        [newPerson addPhoneNumbersObject:newPhone];
    }
    // if there is a phone add it as cell
    if (![[personArray objectAtIndex:38] isEqualToString:@""]) {
        PhoneNumber *newPhone = [PhoneNumber createInContext:context];
        newPhone.label = @"Cell";
        newPhone.number = [personArray objectAtIndex:38];
        [newPerson addPhoneNumbersObject:newPhone];
    }
    // if there is a phone add it as business
    if (![[personArray objectAtIndex:39] isEqualToString:@""]) {
        PhoneNumber *newPhone = [PhoneNumber createInContext:context];
        newPhone.label = @"Work";
        newPhone.number = [personArray objectAtIndex:39];
        [newPerson addPhoneNumbersObject:newPhone];
    }
    
    // if there is an email add it as home
    if (![[personArray objectAtIndex:40] isEqualToString:@""]) {
        EmailAddress *newEmail = [EmailAddress createInContext:context];
        newEmail.label = @"Home";
        newEmail.address = [personArray objectAtIndex:40];
        [newPerson addEmailAddressesObject:newEmail];
    }
    // if there is an emil add it as work
    if (![[personArray objectAtIndex:41] isEqualToString:@""]) {
        EmailAddress *newEmail = [EmailAddress createInContext:context];
        newEmail.label = @"Work";
        newEmail.address = [personArray objectAtIndex:41];
        [newPerson addEmailAddressesObject:newEmail];
    }
    
    [self reportProgress:[NSString stringWithFormat:@"addresses added"]];
    
    // TAGS
    if (![[personArray objectAtIndex:43] isEqualToString:@""]) {
        // Mailing Status
        if ([[personArray objectAtIndex:43] isEqualToString:@"No Mailing"]) {
            [newPerson addTagsObject:noMailingTag];
        }
        if ([[personArray objectAtIndex:43] isEqualToString:@"Newsletter"]) {
            [newPerson addTagsObject:newsletterTag];
        }
    }
    if (![[personArray objectAtIndex:44] isEqualToString:@""]) {
        // Welcome Class Status
        if ([[personArray objectAtIndex:44] isEqualToString:@"ATTENDED"]) {
            [newPerson addTagsObject:welcomeClassAttendedTag];
        }
        if ([[personArray objectAtIndex:44] isEqualToString:@"INVITED"]) {
            [newPerson addTagsObject:welcomeClassInvitedTag];
        }
        if ([[personArray objectAtIndex:44] isEqualToString:@"SEND INVITE"]) {
            [newPerson addTagsObject:welcomeClassInviteTag];
        }
    }
    if (![[personArray objectAtIndex:48] isEqualToString:@""]) {
        // Widowed
        if ([[personArray objectAtIndex:48] isEqualToString:@"Widowed"]) {
            [newPerson addTagsObject:widowedTag];
        }
    }
    if (![[personArray objectAtIndex:51]isEqualToString:@""]) {
        // eNewsletter
        if ([[personArray objectAtIndex:51] isEqualToString:@"Yes"]) {
            [newPerson addTagsObject:eNewsletterTag];
        }
    }
    
    [self reportProgress:[NSString stringWithFormat:@"tags added"]];
    
    // NOTES
    if (![[personArray objectAtIndex:46] isEqualToString:@""]) {
        Note *newNote = [Note createInContext:context];
        newNote.noteText  = [NSString stringWithFormat:@"MEDICAL ALERT: %@", [personArray objectAtIndex:46]];
        [newNote addNoteUsersObject:sysDPUser];
        [newPerson addNotesObject:newNote];
    }
    if (![[personArray objectAtIndex:47] isEqualToString:@""]) {
        Note *newNote = [Note createInContext:context];
        newNote.noteText  = [NSString stringWithFormat:@"BACKGROUND CHECK: %@", [personArray objectAtIndex:47]];
        [newPerson addNotesObject:newNote];
    }
    if (![[personArray objectAtIndex:49] isEqualToString:@""]) {
        Note *newNote = [Note createInContext:context];
        newNote.noteText  = [personArray objectAtIndex:49];
        [newPerson addNotesObject:newNote];
    }
    if (![[personArray objectAtIndex:50] isEqualToString:@""]) {
        Note *newNote = [Note createInContext:context];
        newNote.noteText  = [NSString stringWithFormat:@"FORMER NAME: %@", [personArray objectAtIndex:50]];
        [newPerson addNotesObject:newNote];
    }
    if (![[personArray objectAtIndex:52] isEqualToString:@""]) {
        Note *newNote = [Note createInContext:context];
        newNote.noteText  = [NSString stringWithFormat:@"FOUND CHURCH BY: %@", [personArray objectAtIndex:52]];
        [newPerson addNotesObject:newNote];
    }
    
    [self reportProgress:[NSString stringWithFormat:@"PERSON ADDED: %@", newPerson.name]];
    [self reportProgress:[NSString stringWithFormat:@"------"]];
    
}

- (void)createNewFamilyWithKey:(NSString *)key name:(NSString *)name lastName:(NSString *)lastName InContext:(NSManagedObjectContext *)context
{
    
    // Create a new Family in the current thread context
    Family *fam   = [Family createInContext:context];
    fam.name      = name;
    fam.lastName  = lastName;
    
    // Add family to cache
    [families setObject:fam forKey:key];
    
}

- (void) reportProgress:(NSString *)progress
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DP_PROCESSING_NOTIFICATION" object:progress];    
}

@end
